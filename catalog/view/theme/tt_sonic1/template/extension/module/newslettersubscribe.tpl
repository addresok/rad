<div class="newletter-subscribe">
	<div id="boxes-newsletter" class="newletter-container">
		<div style="" id="dialog_normal" class="window">
			<div class="row">
				<div class="col-md-7 col-sm-8 col-xs-12 box">
					<div class="newletter-title">
						<h2><i class="fa fa-envelope-o"></i><span><?php echo $heading_title; ?></span></h2>
					</div>
					<div class="box-content newleter-content">
						<div id="frm_subscribe_normal">
							<form name="subscribe" id="subscribe">
				   				<?php if(!$thickbox) { ?> style="display:none" <?php } ?>
								<input type="text" value="" name="subscribe_email" id="subscribe_email">
								<input type="hidden" value="" name="subscribe_name" id="subscribe_name" />
								<a class="button" onclick="email_subscribe()">
									<span><?php echo $entry_button; ?></span>
								</a>
								<?php if($option_unsubscribe) { ?>
								<a class="button" onclick="email_unsubscribe()">
									<span><?php echo $entry_unbutton; ?></span>
								</a>
								<?php } ?>
			  				</form>
						</div><!-- /#frm_subscribe -->
						<div id="notification_normal"></div>
					</div><!-- /.box-content -->
				</div>
				<div class="col-md-5 col-sm-4 col-xs-12">
					<ul class="link-follow">
						<li class="first">
							<a class="rss" href="#">
								<i class="fa fa-rss"></i>
								<span class="tooltips">RSS feed</span>
							</a>
						</li>
						<li>
							<a class="google" href="https://plus.google.com/+PlazaThemesMagento/posts">
								<i class="fa fa-google"></i>
								<span class="tooltips">google+</span>
							</a>
						</li>
						<li>
							<a class="face" href="https://www.facebook.com/PlazaThemes1/">
								<i class="fa fa-facebook"></i>
								<span class="tooltips">facebook</span>
							</a>
						</li>
						<li>
							<a class="twitter" href="https://twitter.com/plazathemes">
								<i class="fa fa-twitter"></i>
								<span class="tooltips">twitter</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>	

<script type="text/javascript">
function email_subscribe(){
			$.ajax({
					type: 'post',
					url: 'index.php?route=extension/module/newslettersubscribe/subscribe',
		            data:$("#subscribe").serialize(),
					success: function (json) {
						if(json['status'] == true) {
							var html = 	'<div class="success">';
							html +=		json['html'];
							html +=		'</div>'
							$('#notification_normal').html(html);
						} else {
							var html = 	'<div class="warning">';
							html +=		json['html'];
							html +=		'</div>'
							$('#notification_normal').html(html);
						}

						if(json['reset'] == true) {
							$("#subscribe")[0].reset();
						}
						
					}}); 
			
			
		}
		function email_unsubscribe(){
			$.ajax({
					type: 'post',
					url: 'index.php?route=extension/module/newslettersubscribe/unsubscribe',
					dataType: 'html',
		            data:$("#subscribe").serialize(),
					success: function (html) {
						eval(html);
					}}); 
			$('html, body').delay( 1500 ).animate({ scrollTop: 0 }, 'slow'); 
			
		}
</script>
<script type="text/javascript">
    $(document).ready(function() {
		$('#subscribe_email').keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
                email_subscribe();
            }
			var name= $(this).val();
		  	$('#subscribe_name').val(name);
        });
		$('#subscribe_email').change(function() {
		 var name= $(this).val();
		  		$('#subscribe_name').val(name);
		});
	
    });
</script>


	</div><!-- /.box -->
</div>
