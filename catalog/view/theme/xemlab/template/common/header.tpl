<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 ,maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<link href="catalog/view/javascript/jquery/css/jquery-ui.css" rel="stylesheet" media="screen" />
<link href="catalog/view/theme/xemlab/stylesheet/opentheme/oclayerednavigation/css/oclayerednavigation.css" rel="stylesheet">
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<link href="catalog/view/theme/xemlab/stylesheet/opentheme/ocsearchcategory/css/ocsearchcategory.css" rel="stylesheet">
<link href="catalog/view/theme/xemlab/stylesheet/opentheme/ocslideshow/ocslideshow.css" rel="stylesheet" />
<link href="catalog/view/theme/xemlab/stylesheet/opentheme/hozmegamenu/css/custommenu.css" rel="stylesheet" />
<link href="catalog/view/theme/xemlab/stylesheet/opentheme/vermegamenu/css/ocvermegamenu.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/xemlab/stylesheet/opentheme/ocquickview/css/ocquickview.css">
<link href="catalog/view/theme/xemlab/stylesheet/opentheme/css/owl.carousel.css" rel="stylesheet" />
<link href="catalog/view/theme/xemlab//stylesheet/opentheme/css/animate.css" rel="stylesheet" />

  <link href="catalog/view/theme/xemlab/stylesheet/stylesheet.css" rel="stylesheet">
  <link href="catalog/view/theme/xemlab/stylesheet/style.css" rel="stylesheet">




  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/jquery/jquery-ui.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/opentheme/oclayerednavigation/oclayerednavigation.js" type="text/javascript"></script>

  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

  <script src="catalog/view/javascript/opentheme/ocajaxlogin/ocajaxlogin.js" type="text/javascript"></script>

  <script src="catalog/view/javascript/opentheme/ocslideshow/jquery.nivo.slider.js" type="text/javascript"></script>

  <script src="catalog/view/javascript/opentheme/hozmegamenu/mobile_menu.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/opentheme/hozmegamenu/custommenu.js" type="text/javascript"></script>

  <script src="catalog/view/javascript/opentheme/vermegamenu/ver_menu.js" type="text/javascript"></script>

  <script src="catalog/view/javascript/opentheme/ocquickview/ocquickview.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/opentheme/owl-carousel/owl.carousel.js" type="text/javascript"></script>


  <script src="catalog/view/javascript/jquery/elevatezoom/jquery.elevatezoom.js" type="text/javascript"></script>

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9w2x4aVfH1KlP6vumkMLt99c_JaRHvVA"
  type="text/javascript"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
    <div class="pull-left left-top">
      <ul class="list-inline">
        <li>
          <a href="<?php echo $contact; ?>">
            <i class="fa fa-phone"></i> 
            <?php echo $text_telephone; ?>
            <span class=""> <?php echo $telephone; ?></span>
          </a>
        </li>
        <li>
          <?php if ($logged) { ?>
          <a id="a-logout-link" href="<?php echo $logout; ?>"><i class="fa fa-lock"></i><?php echo $text_logout; ?></a>
          <?php } else { ?>
          <a id="a-login-link" href="<?php echo $login; ?>"><i class="fa fa-unlock-alt"></i><?php echo $text_login; ?></a>
          <?php } ?>
        </li>
      </ul>
    </div>
    <div id="top-links" class="nav pull-right right-top">
      <ul class="list-inline">
        <li><?php echo $currency; ?></li>
        <li><?php echo $language; ?></li>
        <li class="top-account">
          <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle"><i class="fa fa-user"></i><span class="hidden-xs"><?php echo $text_account; ?></span><i class="fa fa-angle-down" aria-hidden="true"></i></a>
          <ul class="dropdown-menu menu-top-account ul-account">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <?php } else { ?>
            <li><a id="a-register-link" href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <?php } ?>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-12">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="quick-access">
          <div class="top-search-content"><?php echo $search; ?></div>
        </div>
      </div>
      <div class="col-md-2 col-sm-12 top-btn-act">
        <div class="row quick-access">
          <div class="col-md-4 col-sm-4">
              <div class="top-compare-content pull-right">
                <button type="button"  data-loading-text="Загрузка..." class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                  <i class="fa fa-refresh"></i>
                  <span id="compare-total"><?=$compare_total?></span>
                </button>
              </div>
          </div>
          <div class="col-md-4 col-sm-4">
              <div class="top-wishlist-content pull-right">
                <button type="button"  data-loading-text="Загрузка..." class="btn btn-inverse btn-block btn-lg dropdown-toggle">
                  <i class="fa fa-heart"></i>
                  <span id="wishlist-total">0</span>
                </button>
              </div>
          </div>
          <div class="col-md-4 col-sm-4">
              <div class="top-cart-content"><?php echo $cart; ?></div>
          </div>
        </div>


      </div>
    </div>
  </div>
  
</header>
<div class="top-menu">
  <div class="container">
      <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-12"><?php echo $ver_menu; ?></div>
          <div class="col-md-9 col-sm-9 col-xs-12"><?php echo $hoz_menu; ?></div>
      </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() { 
  //top-cart show subnav on hover
    $('#cart').mouseenter(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideDown();
    });
    //hide submenus on exit
    $('#cart').mouseleave(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideUp();
    });

  //top-currency show subnav on hover
    $('#form-currency').mouseenter(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideDown();
    });
    //hide submenus on exit
    $('#form-currency').mouseleave(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideUp();
    });

  //top-languge show subnav on hover
    $('#form-language').mouseenter(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideDown();
    });
    //hide submenus on exit
    $('#form-language').mouseleave(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideUp();
    });

  //top-account show subnav on hover
    $('.top-account').mouseenter(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideDown();
    });
    //hide submenus on exit
    $('.top-account').mouseleave(function() {
      $(this).find(".dropdown-menu").stop(true, true).slideUp();
    });
    
 
});
</script>
