<div class="block-content-bottom">
<div class="container">
  <div class="row">
    <div class="static-block-bottom col-lg-4 col-md-4  col-sm-4 col-xs-12">
      <?php echo $footer1; ?>
    </div>
    <div class="logo-container  listProducts col-lg-8 col-md-8  col-sm-8 col-xs-12">
      <?php echo $footer2; ?>
    </div>
  </div>
</div>
</div>
<div class="top-footer">
  <div class="container">
      <?php echo $newsletter; ?>
  </div>
</div>

<footer>
  <div class="container">
    <div class="row">
       <div class="col-md-2 col-sm-4 col-xs-12">
        <h5 class="heading-title-footer"><?php echo $text_category_footer; ?></h5>
        <ul class="list-unstyled">
          <li><a href="#"><?php echo $text_category_footer_laptop; ?></a></li>
          <li><a href="#"><?php echo $text_category_footer_mobile; ?></a></li>
          <li><a href="#"><?php echo $text_category_footer_tablet; ?></a></li>
          <li><a href="#"><?php echo $text_category_footer_headphone; ?></a></li>
          <li><a href="#"><?php echo $text_category_footer_camera; ?></a></li>
          <li><a href="#"><?php echo $text_category_footer_accessories; ?></a></li>
        </ul>
      </div>
      <?php if ($informations) { ?>
      <div class="col-md-2 col-sm-4 col-xs-12">
        <h5 class="heading-title-footer"><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
        </ul>
      </div>
      <?php } ?>
      <div class="col-md-2 col-sm-4 col-xs-12">
        <!-- <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled"> 
        </ul> -->
        <h5 class="heading-title-footer"><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $viewcart; ?>"><?php echo $text_viewcart; ?></a></li>
          <li><a href="<?php echo $blog; ?>"><?php echo $text_blog; ?></a></li>
        </ul>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-12">
        <h5 class="heading-title-footer"><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $search; ?>"><?php echo $text_search; ?></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-8 col-xs-12">
        <h5 class="heading-title-footer"><?php echo $text_store_information; ?></h5>
        <ul class="list-unstyled">
          <li>
           <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p class="footer-information"> 
              <span class="color-title"> <?php echo $title_address; ?> : </span> <span class="ft-left"><?php echo $address; ?></span> 
            </p>
          </li>
          <li>
            <i class="fa fa-mobile" aria-hidden="true"></i>
            <p class="footer-information">
              <span class="color-title"><?php echo $title_telephone; ?> : </span><span class="ft-left"><?php echo $telephone; ?></span>
            </p>
          </li>
          <li>
            <i class="fa fa-fax" aria-hidden="true"></i>
            <p class="footer-information">
              <span class="color-title"><?php echo $title_fax; ?> : </span><span class="ft-left"><?php echo $fax; ?></span>
            </p>
          </li>
          <li>
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <p class="footer-information"> <span class="color-title"><?php echo $title_email; ?> : </span>  <a href="#" class="ft-left ft-email"><?php echo $email; ?></a></p>
          </li>
          
        </ul>
      </div>
      <!-- <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          
        </ul>
      </div> -->
    </div>
    
  </div>
  <div class="footer-powered"> 
    <div class="container">
      <p><?php echo $powered; ?></p>
      <span class="footer-dev">
        линк на розраба
      </span>
    </div>    
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<div id="back-top" class="hidden-xs"></div>
<script type="text/javascript">
    $(document).ready(function(){
     // hide #back-top first
     $("#back-top").hide();
     // fade in #back-top
     $(function () {
      $(window).scroll(function () {
       if ($(this).scrollTop() > 300) {
        $('#back-top').fadeIn();
         $('#back-top').addClass("show");
       } else {
        $('#back-top').fadeOut();
        $('#back-top').removeClass("show");
       }
      });
      // scroll body to 0px on click
      $('#back-top').click(function () {
       $('body,html').animate({
        scrollTop: 0
       }, 800);
       return false;
      });
     });
    });
</script>
</body></html>