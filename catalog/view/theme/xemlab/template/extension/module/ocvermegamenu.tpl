<div class="vermagemenu visible-lg visible-md visible-sm">
    <div class="content-vermagemenu"> 
        <h2 class="title-ver-menu">
        <?php echo $heading_title;?>
            <span class="btn-ver-menu">
                <em class="line-1"></em>
                <em class="line-2"></em>
                <em class="line-3"></em>
            </span>
        </h2>
        <div class="navleft-container">
            <div id="pt_vmegamenu" class="pt_vmegamenu">
                <?php echo $vermegamenu ?> 
            </div>	
        </div>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
var CUSTOMMENU_POPUP_EFFECTS = <?php echo $effects; ?>;
var CUSTOMMENU_POPUP_TOP_OFFSET = <?php echo $top_offset ; ?>
//]]>

$('.vermagemenu h2').click(function () {
    $( ".navleft-container" ).slideToggle("slow");
});

var btn_ver_menu = document.querySelector('.title-ver-menu');
function toggleMenu () {
  btn_ver_menu.classList.toggle('open');
}
btn_ver_menu.addEventListener('click', toggleMenu);
</script>