<div class="most-products-container">
	<div class="module-title">
		<h3 class="title title-group"><?php echo $title; ?></h3>
	</div> 
    <?php 
    $count = 0;
    $rows = $config_slide['f_rows']; 
    if(!$rows) { $rows=1; }
   ?>
   <div class="row">
		<div class="col-lg-3 col-md-4 col-sm-5 hidden-xs">
			<img src="<?php echo $config_slide['image'] ?>" alt="#" >
		</div>
		<div class="col-lg-9 col-md-8 col-sm-7 col-xs-12">
			<div class="row">
			<div class="most-viewed-products-slider">
				<?php foreach ($products as $product) { ?>
				<?php  if($count % $rows == 0 ) { echo '<div class="row_items">'; } $count++; ?>
				<div class="item_product">
					<div class="product-thumb transition">
						<div class="image">
							<a class="product-image" href="<?php echo $product['href']; ?>">
								<?php if($product['rotator_image']): ?>
								<img class="img2 image2" src="<?php echo $product['rotator_image']; ?>" alt="<?php echo $product['name']; ?>" />
								<?php endif; ?>
								<img class="image1" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
							</a>
						</div>
						<?php if($config_slide['f_show_label']): ?>
							<?php if ($product['is_new']):
								if($product['special']): ?>
									<div class="label-pro-sale"><?php echo $text_sale; ?></div>
								<?php else: ?>
									<div class="label-pro-new"><?php echo $text_new; ?></div>
								<?php endif; ?>
							<?php else: ?>
								<?php if($product['special']): ?>
									<div class="label-pro-sale"><?php echo $text_sale; ?></div>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
						<div class="caption">
							<div class="name">
								<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
							</div>
							<div class="stock-status">
                              <span style="font-weight:bold;color:<?php echo $product['stock_color']; ?>"><?php echo $product['stock']; ?></span>
                            </div>
							<?php if (isset($product['rating'])) { ?>
							<div class="rating">
								<span>
									<img src="catalog/view/theme/tt_bestmarket1/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
								</span>
							</div>
							<?php } ?>

							<?php if($config_slide['f_show_price']) { ?>
							<?php if ($product['price']) { ?>
							<div class="price">
								<?php if (!$product['special']) { ?>
								<?php echo $product['price']; ?>
								<?php } else { ?>
								<span class="price-new"><?php echo $product['special']; ?></span>
								<span class="price-old"><?php echo $product['price']; ?></span>
								<?php } ?>
							</div>
							<?php } ?>
							<?php } ?>

							<?php if($config_slide['f_show_des']) { ?>
								<div class="des"><?php echo $product['description']; ?></div>
							<?php } ?>
							<div class="box-button">
								<?php if($config_slide['f_show_addtocart']) { ?>
								<button class="btn-addtocart" type="button"  title="<?php echo $button_cart; ?>"   onclick="cart.add('<?php echo $product['product_id']; ?>');"><span class="icon_bag_alt"></span><em class="tooltips"><?php echo $tooltip_cart; ?></em></button>
								<?php } ?>
								<div class="button-group hidden">
									<button class="btn-wishlist" type="button"  title="<?php echo $button_wishlist; ?>"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="icon_star_alt"></span><em class="tooltips"><?php echo $tooltip_wishlist; ?></em></button>
									<button class="btn-compare" type="button"  title="<?php echo $button_compare; ?>"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><span class="arrow_left-right_alt"></span><em class="tooltips"><?php echo $tooltip_compare; ?></em></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if($count % $rows == 0 || $count == count($products)): ?>
				</div>
				<?php endif; ?>
				<?php } ?>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() { 
  	$(".most-viewed-products-slider").owlCarousel({
	  autoPlay: <?php if($config_slide['autoplay']) { echo 'true' ;} else { echo 'false'; } ?>,
		items : <?php if($config_slide['items']) { echo $config_slide['items'] ;} else { echo 3; } ?>,
		slideSpeed : <?php if($config_slide['f_speed']) { echo $config_slide['f_speed']; } else { echo 200;} ?>,
		navigation : <?php if($config_slide['f_show_nextback']) { echo 'true' ;} else { echo 'false'; } ?>,
		paginationNumbers : true,
		pagination : <?php if($config_slide['f_show_ctr']) { echo 'true' ;} else { echo 'false';} ?>,
		stopOnHover : false,
		itemsDesktop : [1199,2],
		itemsDesktopSmall : [991,1],
		itemsTablet: [767,2],
		itemsMobile : [400,1],
  	});
 
});
</script>