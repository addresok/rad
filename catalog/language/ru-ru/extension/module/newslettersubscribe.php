<?php
// Heading 
$_['heading_title'] 	 = 'Подписка';

$_['newletter_lable'] 	 = 'Подписка на наши новости.';
$_['newletter_des'] 	 = 'текст 1';
$_['sub_newletter'] 	 = 'текст 2';

//Fields
$_['entry_email'] 		 = 'Введите Email';
$_['entry_name'] 		 = 'Введите имя';

//Buttons
$_['entry_button'] 		 = 'Подписаться';
$_['entry_unbutton'] 	 = 'Отписаться';

//text
$_['text_subscribe'] 	 = 'Подписка';

$_['mail_subject']   	 = 'Подписка на новости';

//Messages
$_['error_invalid'] 	 = 'Ошибка: Введите корректный адрес электронной почты.';
$_['subscribe']	    	 = 'Вы подписаны.';
$_['unsubscribe'] 	     = 'Вы отписаны.';
$_['alreadyexist'] 	     = 'Ошибка: Электронная почта уже существует.';
$_['notexist'] 	    	 = 'Ошибка: электронная почта не существует.';
$_['entry_show_again']   =  "Не показывать это  окно снова";