<?php
// Heading
$_['heading_title'] = 'Product Tabs Slider';

// Text
$_['text_tax']      = 'Ex Tax:';
$_['text_bestseller']   = 'Лидер продаж';
$_['text_mostviewed']      = 'Популярное';
$_['text_random']      = 'Случайное';
$_['text_special']      = 'Нет в наличии';
$_['text_latest']      = 'Новинки';
$_['text_sale']      = 'Нет в наличии';
$_['text_new']      = 'Новинки';
$_['text_empty_mostviewed']    = 'There is no Most Viewed Products!';
$_['text_empty_random']    = 'There is no Random Products!';
$_['text_empty_special']    = 'There is no Special Products!';
$_['text_empty_latest']    = 'There is no New Products!';
$_['text_empty_bestseller']    = 'There is no Bestseller Products!';