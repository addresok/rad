<?php
// Heading
$_['heading_title'] = 'Предложение месяца';

// Text
$_['text_tax']      = 'Ex Tax:';
$_['text_sale']      = 'Нет в наличии';
$_['text_new']      = 'Новинка';
$_['text_empty']    = 'There is no products match this category';
$_['tooltip_wishlist']    = 'В закладки';
$_['tooltip_compare']    = 'В сравнение';
