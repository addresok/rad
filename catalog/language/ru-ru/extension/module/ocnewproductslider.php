<?php
// Heading
$_['heading_title'] = 'Новинки';

// Text
$_['text_tax']      = 'Ex Tax:';
$_['text_reviews']             = '%s просмотры';
$_['text_sale']      = 'Нет в наличии';
$_['text_new']      = 'Новинки';
$_['text_empty']    = 'Здесь ничего нет.';