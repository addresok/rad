<?php
$_['heading_title']     = 'Акции';

//Text
$_['text_reviews']      = '%s просмотры';
$_['text_years']        = "Лет";
$_['text_months']       = "Месяцев";
$_['text_weeks']        = "Недель";
$_['text_days']         = "Дней";
$_['text_hours']        = "Часов";
$_['text_minutes']      = "минут";
$_['text_seconds']      = "Секунд";
$_['text_sale']      = 'Нет в наличии';
$_['text_new']      = 'Новинка';
$_['text_empty']    = 'Здесь пусто';
?>
