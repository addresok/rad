<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_store_information']   = 'Контакты';
$_['text_powered']      = 'Босс инк.';
$_['text_category_footer']      = 'Ссылки';
$_['text_category_footer_laptop']      = 'Ссылка 1';
$_['text_category_footer_mobile']      = 'Ссылка 2';
$_['text_category_footer_tablet']      = 'Ссылка 3';
$_['text_category_footer_headphone']      = 'Ссылка 4';
$_['text_category_footer_camera']      = 'Ссылка 5';
$_['text_category_footer_accessories']      = 'Ссылка 6';
$_['text_login']      = 'Авторизация';
$_['text_viewcart']      = 'Ваша корзина';
$_['text_blog']      = 'Статьи';
$_['text_search']      = 'Поиск';
$_['title_address']      = 'Адрес';
$_['title_fax']      = 'Факс';
$_['title_email']      = 'Електронный адрес';
$_['title_telephone']      = 'Контактный телефон';
$_['text_voucher']      = 'Сертификаты';

