-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Гру 02 2017 р., 18:34
-- Версія сервера: 5.6.34
-- Версія PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `rad`
--

-- --------------------------------------------------------

--
-- Структура таблиці `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(1, 1, 'mr', 'mid', '', '15-12 t05 timecity', '', 'hai ba trung', '0123456', 220, 3481, '[]');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_affiliate`
--

CREATE TABLE `oc_affiliate` (
  `affiliate_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_affiliate_activity`
--

CREATE TABLE `oc_affiliate_activity` (
  `affiliate_activity_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_affiliate_login`
--

CREATE TABLE `oc_affiliate_login` (
  `affiliate_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_affiliate_transaction`
--

CREATE TABLE `oc_affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `name`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'RSqb2oQeCyMSzxeSu4ly1nleojRTXDuMTBYO5D3Kp7qLP8xQeNmobMLZAY01wawjrY7xSarrFXTyD8wcpCxDMniAYlj7VlTX3HKBEhYq5YE5jJsrjV6LESFY8g4uF979seVuW69Yj77XWLyC1wzfCmuESxzxcOjadLb66TnXh2CsAUmpvlpwsVpSnEc5SUCzZLkvgMQsiyTamPMOEQlsraab4elYKWHEaZsUCU6npCD89odpdw6XWWqHoTGNaZxK', 1, '2016-10-03 15:06:21', '2016-10-03 15:06:21'),
(2, 'Default', 'HVVwbmQF6RKI9dE3iFJGnPANufJmGTdGpY4zkDd2Qv9dXsBSuOB9tcCbxFUCzPl4WwvxQqnPJ2uc1OK7ufSKmEjJ4rqmgobHixPOSp0ypJQnYJNnY6NuEYXuOXucBmfFPv8EZqaFPahXhGnQ89lpQXit9rWILAuWW8JDk8DehQspIAa7UlD4wmyXRO1Y1BlBXs8NJcyKkkneepUheUaxia8GKhYeL47tB4iRxpO1p14fSd0yJqEx8JgxL2MEmSq7', 1, '2017-11-01 01:39:49', '2017-11-01 01:39:49');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 2, '127.0.0.1');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `session_name` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `token`, `session_id`, `session_name`, `ip`, `date_added`, `date_modified`) VALUES
(1, 2, 'Rvb6UuCrWybRl4CF2sKdiiw91hgTKaR2', 'cvovo24vbigdt3il1ilesn9v83', '', '127.0.0.1', '2017-11-02 20:02:41', '2017-11-02 20:02:41'),
(2, 2, 'qGrMCojhQtf5nmCfTkLXE3V8IjsfNfWg', 'auv5cgmuk6he622ju9bbm29ld6', '', '127.0.0.1', '2017-11-09 14:56:29', '2017-11-09 14:56:29');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_article`
--

CREATE TABLE `oc_article` (
  `article_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_article_description`
--

CREATE TABLE `oc_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `intro_text` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_article_list`
--

CREATE TABLE `oc_article_list` (
  `article_list_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_article_to_list`
--

CREATE TABLE `oc_article_to_list` (
  `article_list_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_article_to_store`
--

CREATE TABLE `oc_article_to_store` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `mf_tooltip` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`, `mf_tooltip`) VALUES
(1, 1, 'Description', NULL),
(2, 1, 'No. of Cores', NULL),
(4, 1, 'test 1', NULL),
(5, 1, 'test 2', NULL),
(6, 1, 'test 3', NULL),
(7, 1, 'test 4', NULL),
(8, 1, 'test 5', NULL),
(9, 1, 'test 6', NULL),
(10, 1, 'test 7', NULL),
(11, 1, 'test 8', NULL),
(3, 1, 'Clockspeed', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'HP Products', 1),
(7, 'Home Page Slideshow', 1),
(8, 'Manufacturers', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(79, 7, 1, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/demo/banners/iPhone6.jpg', 0),
(87, 6, 1, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(80, 7, 1, 'MacBookAir', '', 'catalog/demo/banners/MacBookAir.jpg', 0),
(141, 8, 1, 'HTC', '#', 'catalog/brand/logo_htc-316x316_0.png', 1),
(106, 7, 2, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/demo/banners/iPhone6.jpg', 0),
(107, 6, 2, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(142, 8, 1, 'Apple', '#', 'catalog/brand/open_graph_logo.png', 3),
(140, 8, 1, 'Samsung', '#', 'catalog/brand/58482500cef1014c0b5e49cf.png', 4),
(112, 7, 2, 'MacBookAir', '', 'catalog/demo/banners/MacBookAir.jpg', 0),
(138, 8, 1, 'Lenovo', '#', 'catalog/brand/lenovo.png', 5),
(139, 8, 1, 'Sony', '#', 'catalog/brand/image.png', 2);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_cart`
--

INSERT INTO `oc_cart` (`cart_id`, `api_id`, `customer_id`, `session_id`, `product_id`, `recurring_id`, `option`, `quantity`, `date_added`) VALUES
(4, 0, 1, 'bdappn595o4b9tau1ft578ggo3', 34, 0, '[]', 1, '2016-10-07 16:17:26');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `thumbnail_image` varchar(255) DEFAULT NULL,
  `homethumb_image` varchar(255) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`, `thumbnail_image`, `homethumb_image`, `featured`) VALUES
(20, 'catalog/demo/image-block/cate.jpg', 0, 1, 1, 1, 1, '2009-01-05 21:49:43', '2017-11-15 14:44:52', 'catalog/demo/icon/17-0_thumb.png', '', 0),
(24, '', 0, 1, 1, 5, 1, '2009-01-20 02:36:26', '2017-11-15 13:17:43', '', '', 0),
(18, 'catalog/demo/hp_2.jpg', 0, 1, 0, 2, 1, '2009-01-05 21:49:15', '2017-11-15 13:20:04', 'catalog/demo/icon/17-0_thumb.png', '', 0),
(33, '', 0, 1, 1, 6, 1, '2009-02-03 14:17:55', '2017-11-15 15:28:47', 'catalog/demo/icon/17-0_thumb.png', '', 0),
(34, 'catalog/demo/ipod_touch_4.jpg', 0, 1, 4, 7, 1, '2009-02-03 14:18:11', '2017-11-15 13:23:17', 'catalog/demo/icon/17-0_thumb.png', '', 0),
(57, '', 0, 1, 1, 3, 1, '2011-04-26 08:53:16', '2017-11-15 15:23:44', 'catalog/demo/icon/17-0_thumb.png', '', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(33, 1, 'Планшеты и Аксессуары', '', 'Tablets and Accessories', '', ''),
(24, 1, 'Фото/видео техника', '', 'Photo-video-equipment', '', ''),
(20, 1, 'Ноутбуки и аксессуары', '&lt;p&gt;\r\n	Example of category description text&lt;/p&gt;\r\n', 'Laptops-Accessories', 'Example of category description', ''),
(34, 1, 'Аудиотехника', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', 'Audiotekhnika', '', ''),
(18, 1, 'Игровые приставки', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', 'consoles', '', ''),
(57, 1, 'Телефоны и Аксессуары', '', 'phone-accessories', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_category_filter`
--

INSERT INTO `oc_category_filter` (`category_id`, `filter_id`) VALUES
(20, 1),
(20, 2),
(20, 4),
(20, 5),
(20, 18),
(20, 19),
(20, 20),
(20, 21),
(20, 22),
(24, 17),
(24, 21),
(33, 1),
(33, 4),
(33, 7),
(33, 21),
(33, 22),
(34, 1),
(34, 21),
(34, 22);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(20, 20, 0),
(24, 24, 0),
(18, 18, 0),
(33, 33, 0),
(34, 34, 0),
(57, 57, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(20, 0, 0),
(33, 0, 0),
(18, 0, 0),
(34, 0, 0),
(57, 0, 0),
(24, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(18, 0),
(20, 0),
(24, 0),
(33, 0),
(34, 0),
(57, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_cmsblock`
--

CREATE TABLE `oc_cmsblock` (
  `cmsblock_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` tinyint(1) DEFAULT NULL,
  `identify` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `banner_store` varchar(255) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_cmsblock`
--

INSERT INTO `oc_cmsblock` (`cmsblock_id`, `status`, `sort_order`, `identify`, `link`, `type`, `banner_store`) VALUES
(19, 1, 0, '', '', 1, '0'),
(20, 1, 0, 'link_menu', 'index.php?route=blog/blog', 0, '0'),
(26, 1, 0, 'link_menu', 'http://towerthemes.com/', 0, '4,1,2,3,0'),
(22, 1, 0, '', '', 1, '0'),
(23, 1, 0, '', '', 1, '0'),
(24, 1, 0, '', '', 1, '0'),
(25, 1, 0, '', '', 1, '0'),
(27, 1, 0, 'link_menu', 'index.php?route=product/ocbestseller', 0, '0'),
(28, 1, 0, 'link_menu', 'index.php?route=product/ocnewproduct', 0, '0'),
(29, 1, 0, '', '', 1, '0'),
(30, 1, 0, '', '', 1, '0'),
(31, 1, 0, '', '', 1, '0'),
(32, 1, 0, '', '', 1, '0'),
(33, 1, 0, '', '', 1, '0');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_cmsblock_description`
--

CREATE TABLE `oc_cmsblock_description` (
  `cmsblock_des_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `cmsblock_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `sub_title` varchar(64) DEFAULT NULL,
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_cmsblock_description`
--

INSERT INTO `oc_cmsblock_description` (`cmsblock_des_id`, `language_id`, `cmsblock_id`, `title`, `sub_title`, `description`) VALUES
(177, 2, 26, 'View All Themes', '', '																			'),
(258, 1, 23, 'Block content bottom', NULL, '																																																																																&lt;div class=&quot;img&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/banners/360x139-csmblock23.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;																																																																								'),
(263, 1, 24, 'Block content top category', NULL, '																																																																														&lt;div class=&quot;category-image&quot;&gt;\r\n									&lt;p class=&quot;category-image&quot;&gt;&lt;img src=&quot;image/catalog/demo/image-block/banner-cate.jpg&quot; alt=&quot;Smartphones &amp;amp; Accessories&quot; title=&quot;Smartphones &amp;amp; Accessories&quot;&gt;&lt;/p&gt;								&lt;/div&gt;																																																																										'),
(253, 1, 20, 'Блог', NULL, '																																																																																															'),
(251, 1, 22, 'Block ourservice', NULL, '																																																																																&lt;div class=&quot;our-service&quot;&gt;\r\n&lt;h4 class=&quot;our-service-title&quot;&gt;&lt;span&gt;Наш сервис&lt;/span&gt;&lt;/h4&gt;\r\n&lt;ul&gt;\r\n&lt;li&gt;&lt;i class=&quot;fa fa-truck&quot;&gt;&lt;/i&gt;\r\n&lt;h2&gt;Бесплатная доставка,&lt;/h2&gt;\r\n&lt;p&gt;по Украине.&lt;/p&gt;\r\n&lt;/li&gt;\r\n&lt;li&gt;&lt;i class=&quot;fa fa-tag&quot;&gt;&lt;/i&gt;\r\n&lt;h2&gt;Скидки,&lt;/h2&gt;\r\n&lt;p&gt;новые скидки каждую неделю.&lt;/p&gt;\r\n&lt;/li&gt;\r\n&lt;li&gt;&lt;i class=&quot;fa fa-thumbs-up&quot;&gt;&lt;/i&gt;\r\n&lt;h2&gt;Покупай онлайн,&lt;/h2&gt;\r\n&lt;p&gt;оплати покупку не выходя из дома.&lt;/p&gt;\r\n&lt;/li&gt;\r\n&lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;\r\n&lt;h2&gt;Гарантия качества,&lt;/h2&gt;\r\n&lt;p&gt;наши продукты сертифицированы.&lt;/p&gt;\r\n&lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;/div&gt;																																																																								'),
(176, 1, 26, 'View All Themes', '', '																			'),
(246, 1, 27, 'Лучшие товары', NULL, '																																																									'),
(244, 1, 28, 'Новые товары', NULL, '																																						'),
(260, 1, 29, 'Block content top 2', NULL, '																				&lt;div class=&quot;static-block-top-home2&quot;&gt;\r\n	&lt;div class=&quot;row row-1 clearfix&quot;&gt;\r\n		&lt;div class=&quot;  col-lg-3 col-md-3  col-sm-3 col-xs-12&quot;&gt;\r\n			&lt;div class=&quot;img img-transiton1&quot;&gt;&lt;a href=&quot;index.php?route=product/category&amp;amp;path=20&quot; title=&quot;&quot;&gt; &lt;img alt=&quot;saw&quot; src=&quot;image/catalog/demo/image-block/16.jpg&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n	&lt;div class=&quot;row row-2&quot;&gt;\r\n		&lt;div class=&quot;col-lg-5 col-lg-offset-3 col-md-5 col-md-offset-3 col-sm-5 col-sm-offset-3 col-xs-12&quot;&gt;\r\n			&lt;div class=&quot;img img-transiton1&quot;&gt;&lt;a href=&quot;index.php?route=product/category&amp;amp;path=20&quot; title=&quot;&quot;&gt; &lt;img alt=&quot;drill&quot; src=&quot;image/catalog/demo/image-block/7.jpg&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n		&lt;/div&gt;\r\n		&lt;div class=&quot;col-lg-4 col-md-4 col-sm-4 col-xs-12&quot;&gt;\r\n			&lt;div class=&quot;img img-transiton1&quot;&gt;&lt;a href=&quot;index.php?route=product/category&amp;amp;path=20&quot; title=&quot;&quot;&gt; &lt;img alt=&quot;drill&quot; src=&quot;image/catalog/demo/image-block/12.jpg&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n	&lt;div class=&quot;row row-3&quot;&gt;\r\n		&lt;div class=&quot;col-lg-4 col-lg-offset-3 col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3 col-xs-12&quot;&gt;\r\n			&lt;div class=&quot;img img-transiton1&quot;&gt;&lt;a href=&quot;index.php?route=product/category&amp;amp;path=20&quot; title=&quot;&quot;&gt; &lt;img alt=&quot;drill&quot; src=&quot;image/catalog/demo/image-block/8.jpg&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n		&lt;/div&gt;\r\n		&lt;div class=&quot;col-lg-5 col-md-5 col-sm-5 col-xs-12&quot;&gt;\r\n			&lt;div class=&quot;img img-transiton1&quot;&gt;&lt;a href=&quot;index.php?route=product/category&amp;amp;path=20&quot; title=&quot;&quot;&gt; &lt;img alt=&quot;drill&quot; src=&quot;image/catalog/demo/image-block/9.jpg&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;																																																																								'),
(261, 1, 30, 'Block content top 3', NULL, '										&lt;div class=&quot;static-block&quot;&gt;\r\n	&lt;div class=&quot;container-static-block&quot;&gt;\r\n		&lt;div class=&quot;row&quot;&gt;\r\n			&lt;div class=&quot;img img-transiton1 col-lg-6 col-md-6 col-sm-6 col-xs-12&quot;&gt;&lt;a title=&quot;&quot; href=&quot;index.php?route=product/category&amp;amp;path=20&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/9.jpg&quot; alt=&quot;drill&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n			&lt;div class=&quot;img img-transiton1 col-lg-6 col-md-6 col-sm-6 col-xs-12&quot;&gt;&lt;a title=&quot;&quot; href=&quot;index.php?route=product/category&amp;amp;path=20&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/7.jpg&quot; alt=&quot;drill&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n			&lt;/div&gt;\r\n		&lt;/div&gt;\r\n	&lt;/div&gt;\r\n										'),
(257, 1, 32, 'Banner right home 4', NULL, '																																																		&lt;div class=&quot;static-block &quot;&gt;\r\n&lt;div class=&quot;img img-block1&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/banner7-right41.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;div class=&quot;img img-block1&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/banner7-right42.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;div class=&quot;img img-block1&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/banner7-right43.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;/div&gt;																																													'),
(264, 1, 31, 'Banner left home 4', NULL, '																																																		&lt;div class=&quot;static-block &quot;&gt;\r\n&lt;div class=&quot;img img-block1&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/8.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;div class=&quot;img img-block1&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/7-4.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;div class=&quot;img img-block1&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/12.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;/div&gt;																																													'),
(262, 1, 33, 'Block content top 4', NULL, '																																							&lt;div class=&quot;static-block-home4 &quot;&gt;\r\n&lt;div class=&quot;col-lg-8 col-md-8 col-sm-12 col-xs-12&quot;&gt;\r\n&lt;div class=&quot;textstaticblock &quot;&gt;\r\n&lt;div class=&quot;img pull-left&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/img_4_8.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;div class=&quot;media-body&quot;&gt;\r\n&lt;h4&gt;GET THE THEME INSTALLED AND CONFIGURED FOR FREE!&lt;/h4&gt;\r\n&lt;p&gt;Installation of our theme is pretty easy, anyway if youve got into any problems we can install it on your server absolutelly for free.&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;img col-lg-4 col-md-4 col-sm-12 col-xs-12&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/demo/image-block/block-top-home4.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;\r\n&lt;/div&gt;																		'),
(240, 1, 19, 'Block content top 1', NULL, '																																																																						&lt;div class=&quot;static-block&quot;&gt;&lt;div class=&quot;container&quot;&gt;&lt;div class=&quot;row&quot;&gt;&lt;div class=&quot;img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/banners/370x240.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/banners/360x234.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;img img-transiton1 col-lg-4 col-md-4  col-sm-4 col-xs-12&quot;&gt;&lt;a title=&quot;&quot; href=&quot;#&quot;&gt; &lt;img src=&quot;image/catalog/banners/370x240.jpg&quot; alt=&quot;&quot;&gt; &lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;																																																															'),
(259, 1, 25, 'Block content left 1', NULL, '																																																		&lt;div class=&quot;static-block&quot;&gt;\r\n&lt;div class=&quot;img img-transiton1&quot;&gt;&lt;a href=&quot;#&quot; title=&quot;&quot;&gt;&lt;img src=&quot;image/catalog/banners/263x296-csmblk25.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;&lt;/div&gt;\r\n&lt;/div&gt;																																													');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_cmsblock_to_store`
--

CREATE TABLE `oc_cmsblock_to_store` (
  `cmsblock_id` int(11) DEFAULT NULL,
  `store_id` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, 0, '2017-11-02 18:10:36'),
(4, 'Гривна', 'UAN', '', ' грн.', '0', 1.00000000, 1, '2017-11-19 21:35:18');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `approved`, `safe`, `token`, `code`, `date_added`) VALUES
(1, 1, 0, 1, 'mr', 'mid', 'demo@towerthemes.com', '0974221186', '', '4f9d30e1d78048ed25110838e360140f6d75c406', '5Nbc8j4ic', NULL, NULL, 1, 1, '[]', '127.0.0.1', 1, 1, 0, '', '', '2016-10-05 15:30:13');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test'),
(1, 2, 'Default', 'test');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(1, 1, '127.0.0.1', '2016-10-05 15:30:15');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(2, 'demo@plazathemes.com', '127.0.0.1', 2, '2016-10-05 10:29:34', '2016-10-05 10:29:40'),
(4, 'admin', '127.0.0.1', 8, '2017-11-19 01:33:08', '2017-12-02 16:23:47'),
(5, '', '127.0.0.1', 21, '2017-11-19 14:48:40', '2017-12-02 16:27:12'),
(6, 'аdmin', '127.0.0.1', 5, '2017-11-21 01:35:17', '2017-11-21 01:35:22'),
(7, 'vladyslav9417@gmail.com', '127.0.0.1', 5, '2017-11-23 14:26:00', '2017-11-23 14:26:05'),
(8, 'фффффффффффф', '127.0.0.1', 1, '2017-11-24 21:39:45', '2017-11-24 21:39:45');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_customer_wishlist`
--

INSERT INTO `oc_customer_wishlist` (`customer_id`, `product_id`, `date_added`) VALUES
(1, 32, '2016-10-07 09:52:28'),
(1, 46, '2016-10-07 09:52:28');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(7) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_dqc_setting`
--

CREATE TABLE `oc_dqc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_dqc_setting`
--

INSERT INTO `oc_dqc_setting` (`setting_id`, `store_id`, `name`, `value`) VALUES
(1, 0, '11/02/2017 07:39:25 pm', '{\"name\":\"11\\/02\\/2017 07:39:25 pm\",\"general\":{\"clear_session\":\"0\",\"login_refresh\":\"0\",\"analytics_event\":\"0\",\"update_mini_cart\":\"1\",\"compress\":\"1\",\"min_order\":{\"value\":\"0\",\"text\":{\"1\":\"You must have a sum more then %s to make an order \",\"2\":\"The minimum order is %s\"}},\"min_quantity\":{\"value\":\"0\",\"text\":{\"1\":\"You must have a quantity more then %s to make an order \",\"2\":\"The minimum quantity is %s\"}},\"config\":\"d_quickcheckout\"},\"step\":{\"login\":{\"default_option\":\"guest\",\"option\":{\"login\":{\"display\":\"1\",\"title\":{\"1\":\"Login\",\"2\":\"Login\"}},\"register\":{\"display\":\"1\",\"title\":{\"1\":\"Register\",\"2\":\"Register\"}},\"guest\":{\"display\":\"1\",\"title\":{\"1\":\"Guest\",\"2\":\"Guest\"}}},\"icon\":\"fa fa-user\",\"description\":{\"1\":\"\",\"2\":\"\"},\"column\":\"1\",\"row\":\"1\"},\"payment_address\":{\"fields\":{\"firstname\":{\"sort_order\":\"0\",\"value\":\"\",\"mask\":\"\"},\"lastname\":{\"sort_order\":\"1\",\"value\":\"\",\"mask\":\"\"},\"email\":{\"sort_order\":\"2\",\"value\":\"\",\"mask\":\"\"},\"email_confirm\":{\"sort_order\":\"3\",\"value\":\"\",\"mask\":\"\"},\"telephone\":{\"sort_order\":\"4\",\"value\":\"\",\"mask\":\"\"},\"fax\":{\"sort_order\":\"5\",\"value\":\"\",\"mask\":\"\"},\"password\":{\"sort_order\":\"6\"},\"confirm\":{\"sort_order\":\"7\"},\"heading\":{\"sort_order\":\"8\"},\"company\":{\"sort_order\":\"9\",\"value\":\"\",\"mask\":\"\"},\"customer_group_id\":{\"sort_order\":\"10\"},\"address_1\":{\"sort_order\":\"11\",\"value\":\"\",\"mask\":\"\"},\"address_2\":{\"sort_order\":\"12\",\"value\":\"\",\"mask\":\"\"},\"city\":{\"sort_order\":\"13\",\"value\":\"\",\"mask\":\"\"},\"postcode\":{\"sort_order\":\"14\",\"value\":\"\",\"mask\":\"\"},\"country_id\":{\"sort_order\":\"15\",\"value\":\"\"},\"zone_id\":{\"sort_order\":\"16\",\"value\":\"\"},\"newsletter\":{\"sort_order\":\"17\",\"value\":\"1\"},\"shipping_address\":{\"sort_order\":\"18\",\"value\":\"0\"},\"agree\":{\"sort_order\":\"19\",\"value\":\"0\"}},\"icon\":\"fa fa-book\",\"title\":{\"1\":\"Payment Address\",\"2\":\"Payment Address\"},\"description\":{\"1\":\"\",\"2\":\"\"},\"column\":\"1\",\"row\":\"2\"},\"shipping_address\":{\"fields\":{\"firstname\":{\"sort_order\":\"1\",\"value\":\"\",\"mask\":\"\"},\"lastname\":{\"sort_order\":\"2\",\"value\":\"\",\"mask\":\"\"},\"company\":{\"sort_order\":\"3\",\"value\":\"\",\"mask\":\"\"},\"address_1\":{\"sort_order\":\"4\",\"value\":\"\",\"mask\":\"\"},\"address_2\":{\"sort_order\":\"5\",\"value\":\"\",\"mask\":\"\"},\"city\":{\"sort_order\":\"6\",\"value\":\"\",\"mask\":\"\"},\"postcode\":{\"sort_order\":\"7\",\"value\":\"\",\"mask\":\"\"},\"country_id\":{\"sort_order\":\"8\",\"value\":\"\"},\"zone_id\":{\"sort_order\":\"9\",\"value\":\"\"}},\"icon\":\"fa fa-book\",\"title\":{\"1\":\"Shipping Address\",\"2\":\"Shipping Address\"},\"description\":{\"1\":\"\",\"2\":\"\"},\"column\":\"1\",\"row\":\"3\"},\"shipping_method\":{\"display\":\"1\",\"display_options\":\"1\",\"display_title\":\"1\",\"input_style\":\"radio\",\"default_option\":\"flat\",\"icon\":\"fa fa-truck\",\"title\":{\"1\":\"Shipping method\",\"2\":\"Shipping method\"},\"description\":{\"1\":\"Please select the preferred shipping method to use on this order.\",\"2\":\"Please select the preferred shipping method to use on this order.\"},\"column\":\"2\",\"row\":\"1\"},\"payment_method\":{\"display\":\"1\",\"display_options\":\"1\",\"display_images\":\"1\",\"input_style\":\"radio\",\"default_option\":\"cod\",\"icon\":\"fa fa-credit-card\",\"title\":{\"1\":\"Payment method\",\"2\":\"Payment method\"},\"description\":{\"1\":\"Please select the preferred payment method to use on this order.\",\"2\":\"Please select the preferred payment method to use on this order.\"},\"column\":\"3\",\"row\":\"1\"},\"payment\":{\"default_payment_popup\":\"0\",\"payment_popups\":{\"cod\":\"0\",\"free_checkout\":\"0\"},\"column\":\"4\",\"row\":\"2\"},\"cart\":{\"icon\":\"fa fa-shopping-cart\",\"title\":{\"1\":\"Shopping cart\",\"2\":\"Shopping cart\"},\"description\":{\"1\":\"\",\"2\":\"\"},\"column\":\"4\",\"row\":\"2\"},\"confirm\":{\"fields\":{\"comment\":{\"sort_order\":\"0\"},\"agree\":{\"sort_order\":\"1\",\"value\":\"0\"}},\"column\":\"4\",\"row\":\"2\"}},\"design\":{\"login_style\":\"popup\",\"theme\":\"default\",\"block_style\":\"row\",\"placeholder\":\"1\",\"breadcrumb\":\"1\",\"address_style\":\"radio\",\"cart_image_size\":{\"width\":\"80\",\"height\":\"80\"},\"max_width\":\"\",\"bootstrap\":\"1\",\"autocomplete\":\"1\",\"only_quickcheckout\":\"0\",\"column_width\":{\"1\":\"4\",\"2\":\"4\",\"3\":\"4\",\"4\":\"8\"},\"custom_style\":\"\"},\"account\":{\"guest\":{\"payment_address\":{\"display\":\"1\",\"fields\":{\"firstname\":{\"display\":\"1\",\"require\":\"1\"},\"lastname\":{\"display\":\"1\",\"require\":\"1\"},\"email\":{\"display\":\"1\",\"require\":\"1\"},\"email_confirm\":{\"display\":\"1\",\"require\":\"1\"},\"telephone\":{\"display\":\"1\",\"require\":\"0\"},\"fax\":{\"display\":\"1\",\"require\":\"0\"},\"heading\":{\"display\":\"1\"},\"company\":{\"display\":\"1\",\"require\":\"0\"},\"customer_group_id\":{\"display\":\"1\",\"require\":\"0\"},\"address_1\":{\"display\":\"1\",\"require\":\"1\"},\"address_2\":{\"display\":\"0\",\"require\":\"0\"},\"city\":{\"display\":\"1\",\"require\":\"1\"},\"postcode\":{\"display\":\"1\",\"require\":\"1\"},\"country_id\":{\"display\":\"1\",\"require\":\"1\"},\"zone_id\":{\"display\":\"1\",\"require\":\"1\"},\"shipping_address\":{\"display\":\"1\"},\"agree\":{\"display\":\"1\",\"require\":\"1\"}}},\"shipping_address\":{\"display\":\"1\",\"require\":\"0\",\"fields\":{\"firstname\":{\"display\":\"1\",\"require\":\"1\"},\"lastname\":{\"display\":\"0\",\"require\":\"0\"},\"company\":{\"display\":\"1\",\"require\":\"0\"},\"address_1\":{\"display\":\"1\",\"require\":\"1\"},\"address_2\":{\"display\":\"0\",\"require\":\"0\"},\"city\":{\"display\":\"1\",\"require\":\"0\"},\"postcode\":{\"display\":\"1\",\"require\":\"1\"},\"country_id\":{\"display\":\"1\",\"require\":\"1\"},\"zone_id\":{\"display\":\"1\",\"require\":\"1\"}}},\"cart\":{\"display\":\"1\",\"columns\":{\"image\":\"1\",\"name\":\"1\",\"model\":\"0\",\"quantity\":\"1\",\"price\":\"1\",\"total\":\"1\"},\"option\":{\"coupon\":{\"display\":\"1\"},\"voucher\":{\"display\":\"1\"},\"reward\":{\"display\":\"1\"}}},\"confirm\":{\"fields\":{\"comment\":{\"display\":\"1\",\"require\":\"0\"},\"agree\":{\"display\":\"1\",\"require\":\"1\"}}}},\"register\":{\"payment_address\":{\"display\":\"1\",\"fields\":{\"firstname\":{\"display\":\"1\",\"require\":\"1\"},\"lastname\":{\"display\":\"1\",\"require\":\"1\"},\"email\":{\"display\":\"1\",\"require\":\"1\"},\"email_confirm\":{\"display\":\"1\",\"require\":\"1\"},\"telephone\":{\"display\":\"1\",\"require\":\"0\"},\"fax\":{\"display\":\"1\",\"require\":\"0\"},\"password\":{\"display\":\"1\",\"require\":\"1\"},\"confirm\":{\"display\":\"1\",\"require\":\"1\"},\"heading\":{\"display\":\"1\"},\"company\":{\"display\":\"1\",\"require\":\"0\"},\"customer_group_id\":{\"display\":\"1\",\"require\":\"0\"},\"address_1\":{\"display\":\"1\",\"require\":\"1\"},\"address_2\":{\"display\":\"0\",\"require\":\"0\"},\"city\":{\"display\":\"1\",\"require\":\"1\"},\"postcode\":{\"display\":\"1\",\"require\":\"1\"},\"country_id\":{\"display\":\"1\",\"require\":\"1\"},\"zone_id\":{\"display\":\"1\",\"require\":\"1\"},\"newsletter\":{\"display\":\"1\",\"require\":\"0\"},\"shipping_address\":{\"display\":\"1\"},\"agree\":{\"display\":\"1\",\"require\":\"1\"}}},\"shipping_address\":{\"display\":\"1\",\"require\":\"0\",\"fields\":{\"firstname\":{\"display\":\"1\",\"require\":\"1\"},\"lastname\":{\"display\":\"0\",\"require\":\"0\"},\"company\":{\"display\":\"1\",\"require\":\"0\"},\"address_1\":{\"display\":\"1\",\"require\":\"1\"},\"address_2\":{\"display\":\"0\",\"require\":\"0\"},\"city\":{\"display\":\"1\",\"require\":\"0\"},\"postcode\":{\"display\":\"1\",\"require\":\"1\"},\"country_id\":{\"display\":\"1\",\"require\":\"1\"},\"zone_id\":{\"display\":\"1\",\"require\":\"1\"}}},\"cart\":{\"display\":\"1\",\"columns\":{\"image\":\"1\",\"name\":\"1\",\"model\":\"0\",\"quantity\":\"1\",\"price\":\"1\",\"total\":\"1\"},\"option\":{\"coupon\":{\"display\":\"1\"},\"voucher\":{\"display\":\"1\"},\"reward\":{\"display\":\"1\"}}},\"confirm\":{\"fields\":{\"comment\":{\"display\":\"1\",\"require\":\"0\"},\"agree\":{\"display\":\"1\",\"require\":\"1\"}}}},\"logged\":{\"payment_address\":{\"display\":\"1\",\"fields\":{\"firstname\":{\"display\":\"1\",\"require\":\"1\"},\"lastname\":{\"display\":\"1\",\"require\":\"1\"},\"company\":{\"display\":\"1\",\"require\":\"0\"},\"address_1\":{\"display\":\"1\",\"require\":\"1\"},\"address_2\":{\"display\":\"0\",\"require\":\"0\"},\"city\":{\"display\":\"1\",\"require\":\"0\"},\"postcode\":{\"display\":\"1\",\"require\":\"1\"},\"country_id\":{\"display\":\"1\",\"require\":\"1\"},\"zone_id\":{\"display\":\"1\",\"require\":\"1\"},\"shipping_address\":{\"display\":\"1\"}}},\"shipping_address\":{\"display\":\"1\",\"require\":\"0\",\"fields\":{\"firstname\":{\"display\":\"1\",\"require\":\"1\"},\"lastname\":{\"display\":\"0\",\"require\":\"0\"},\"company\":{\"display\":\"1\",\"require\":\"0\"},\"address_1\":{\"display\":\"1\",\"require\":\"1\"},\"address_2\":{\"display\":\"0\",\"require\":\"0\"},\"city\":{\"display\":\"1\",\"require\":\"1\"},\"postcode\":{\"display\":\"1\",\"require\":\"1\"},\"country_id\":{\"display\":\"1\",\"require\":\"1\"},\"zone_id\":{\"display\":\"1\",\"require\":\"1\"}}},\"cart\":{\"display\":\"1\",\"columns\":{\"image\":\"1\",\"name\":\"1\",\"model\":\"0\",\"quantity\":\"1\",\"price\":\"1\",\"total\":\"1\"},\"option\":{\"coupon\":{\"display\":\"1\"},\"voucher\":{\"display\":\"1\"},\"reward\":{\"display\":\"1\"}}},\"confirm\":{\"fields\":{\"comment\":{\"display\":\"1\",\"require\":\"0\"},\"agree\":{\"display\":\"1\",\"require\":\"1\"}}}}}}');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_dqc_statistic`
--

CREATE TABLE `oc_dqc_statistic` (
  `statistic_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `data` text NOT NULL,
  `rating` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `date_added`) VALUES
(1, 'voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'theme_default'),
(21, 'module', 'installtemp'),
(22, 'module', 'occmsblock'),
(23, 'module', 'ochozmegamenu'),
(24, 'module', 'occountdown'),
(25, 'module', 'ocslideshow'),
(26, 'module', 'ocvermegamenu'),
(27, 'module', 'ocproductrotator'),
(29, 'module', 'octabproductslider'),
(44, 'module', 'octabcategoryslider'),
(31, 'module', 'ocnewproductslider'),
(32, 'module', 'newslettersubscribe'),
(54, 'module', 'ocblog'),
(34, 'module', 'oclayerednavigation'),
(47, 'module', 'ocnewproductpage'),
(36, 'module', 'ocquickview'),
(37, 'module', 'octestimonial'),
(38, 'module', 'ocsearchcategory'),
(46, 'module', 'ocbestsellerpage'),
(45, 'module', 'occategorythumbnail'),
(50, 'module', 'mega_filter');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 0),
(2, 1, 0),
(3, 1, 0),
(4, 1, 0),
(5, 1, 0),
(6, 1, 0),
(7, 1, 0),
(8, 1, 0),
(9, 2, 0),
(10, 2, 0),
(11, 2, 0),
(12, 2, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(16, 2, 0),
(17, 2, 0),
(22, 3, 0),
(21, 3, 0),
(20, 3, 0),
(19, 3, 0),
(18, 3, 0),
(25, 4, 0),
(24, 4, 0),
(23, 4, 0),
(28, 5, 0),
(27, 5, 0),
(26, 5, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 1, 1, 'red'),
(2, 1, 1, 'blue'),
(3, 1, 1, 'green'),
(4, 1, 1, 'black'),
(5, 1, 1, 'brown'),
(6, 1, 1, 'yellow'),
(7, 1, 1, 'pink'),
(9, 1, 2, 'hermes'),
(10, 1, 2, 'chanel'),
(11, 1, 2, 'louis vuitton'),
(12, 1, 2, 'christian dior'),
(13, 1, 2, 'ferragamo'),
(14, 1, 2, 'versace'),
(15, 1, 2, 'prada'),
(22, 1, 3, 'xxl'),
(21, 1, 3, 'xl'),
(20, 1, 3, 'l'),
(19, 1, 3, 'm'),
(18, 1, 3, 's'),
(25, 1, 4, 'girly'),
(24, 1, 4, 'dressy'),
(23, 1, 4, 'casual'),
(28, 1, 5, 'viscose '),
(27, 1, 5, 'polyester '),
(26, 1, 5, 'cotton '),
(16, 1, 2, 'giorgio armani'),
(17, 1, 2, 'ermenegildo zegna'),
(8, 1, 1, 'orange'),
(1, 2, 1, 'red'),
(2, 2, 1, 'blue'),
(3, 2, 1, 'green'),
(4, 2, 1, 'black'),
(5, 2, 1, 'brown'),
(6, 2, 1, 'yellow'),
(7, 2, 1, 'pink'),
(9, 2, 2, 'hermes'),
(10, 2, 2, 'chanel'),
(11, 2, 2, 'louis vuitton'),
(12, 2, 2, 'christian dior'),
(13, 2, 2, 'ferragamo'),
(14, 2, 2, 'versace'),
(15, 2, 2, 'prada'),
(22, 2, 3, 'xxl'),
(21, 2, 3, 'xl'),
(20, 2, 3, 'l'),
(19, 2, 3, 'm'),
(18, 2, 3, 's'),
(25, 2, 4, 'girly'),
(24, 2, 4, 'dressy'),
(23, 2, 4, 'casual'),
(28, 2, 5, 'viscose '),
(27, 2, 5, 'polyester '),
(26, 2, 5, 'cotton '),
(16, 2, 2, 'giorgio armani'),
(17, 2, 2, 'ermenegildo zegna'),
(8, 2, 1, 'orange');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `mf_tooltip` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`, `mf_tooltip`) VALUES
(1, 1, 'color', NULL),
(2, 1, 'manufacture', NULL),
(3, 1, 'size', NULL),
(4, 1, 'style', NULL),
(5, 1, 'compositions', NULL),
(1, 2, 'color', NULL),
(2, 2, 'manufacture', NULL),
(3, 2, 'size', NULL),
(4, 2, 'style', NULL),
(5, 2, 'compositions', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 'О нас', '&lt;p&gt;\r\n	About Selphy&amp;nbsp;lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt;\r\n', 'About Us', '', ''),
(6, 1, 'Доставка ', '&lt;p&gt;\r\n	Delivery Information&amp;nbsp;&lt;span style=&quot;line-height: 17.1428px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 17.1428px;&quot;&gt;Selphy&amp;nbsp;lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/span&gt;&lt;/p&gt;\r\n', 'Delivery Information', '', ''),
(3, 1, 'Конфиденциальность', '&lt;p&gt;\r\n	Privacy Policy&amp;nbsp;&lt;span style=&quot;line-height: 17.1428px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 17.1428px;&quot;&gt;Selphy&amp;nbsp;lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/span&gt;&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(5, 1, 'Условия соглашения', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&amp;nbsp;&lt;span style=&quot;line-height: 17.1428px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 17.1428px;&quot;&gt;Selphy&amp;nbsp;lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/span&gt;&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(4, 0, 0),
(6, 0, 0),
(3, 0, 0),
(5, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'Русский', 'ru-ru', 'ru,ru_RU,ru_RU.UTF-8', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home 1'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search'),
(14, 'Home 2'),
(15, 'Home 3'),
(16, 'Home 4'),
(17, 'Lates Testimonial'),
(18, 'Blog'),
(21, 'Account 3,4'),
(20, 'Best Seller'),
(22, 'Affiliate 3,4'),
(23, 'Best Seller 3,4'),
(24, 'Blog 3,4'),
(25, 'Category 3,4'),
(26, 'Checkout 3,4'),
(27, 'Compare 3,4'),
(28, 'Contact 3,4'),
(29, 'Information 3,4'),
(30, 'Lates Testimonial 3,4'),
(31, 'Manufacturer 3,4'),
(32, 'Product 3,4'),
(33, 'Search 3,4'),
(34, 'Sitemap 3,4'),
(35, 'Mega Filter PRO'),
(36, 'Manufacturer info');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(1048, 10, 'ocvermegamenu.34', 'ver_menu', 0),
(1036, 6, 'newslettersubscribe.40', 'newsletter', 0),
(1853, 1, 'octestimonial.42', 'tesmonial', 0),
(1851, 1, 'occmsblock.43', 'block_service', 0),
(1849, 1, 'carousel.45', 'footer2', 0),
(1897, 3, 'newslettersubscribe.40', 'newsletter', 0),
(1896, 3, 'carousel.45', 'footer2', 0),
(1850, 1, 'newslettersubscribe.40', 'newsletter', 0),
(1171, 2, 'newslettersubscribe.40', 'newsletter', 0),
(1170, 2, 'ochozmegamenu.33', 'hoz_menu', 0),
(1169, 2, 'carousel.45', 'footer2', 0),
(1848, 1, 'occmsblock.46', 'footer1', 0),
(1035, 6, 'ochozmegamenu.33', 'hoz_menu', 0),
(1034, 6, 'carousel.45', 'footer2', 0),
(1047, 10, 'newslettersubscribe.40', 'newsletter', 0),
(1046, 10, 'ochozmegamenu.33', 'hoz_menu', 0),
(1045, 10, 'carousel.45', 'footer2', 1),
(1044, 10, 'occmsblock.46', 'footer1', 0),
(1033, 6, 'occmsblock.46', 'footer1', 0),
(1115, 12, 'ocvermegamenu.34', 'ver_menu', 0),
(1114, 12, 'newslettersubscribe.40', 'newsletter', 0),
(1113, 12, 'ochozmegamenu.33', 'hoz_menu', 0),
(1112, 12, 'carousel.45', 'footer2', 0),
(1125, 8, 'newslettersubscribe.40', 'newsletter', 0),
(1124, 8, 'ochozmegamenu.33', 'hoz_menu', 0),
(1123, 8, 'carousel.45', 'footer2', 0),
(1136, 11, 'newslettersubscribe.40', 'newsletter', 0),
(1135, 11, 'ochozmegamenu.33', 'hoz_menu', 0),
(1134, 11, 'carousel.45', 'footer2', 0),
(1162, 32, 'carousel.45', 'footer2', 0),
(1903, 5, 'carousel.45', 'footer2', 0),
(1902, 5, 'occmsblock.46', 'footer1', 0),
(1901, 5, 'ocvermegamenu.34', 'ver_menu', 0),
(1197, 13, 'carousel.45', 'footer2', 0),
(1198, 13, 'ochozmegamenu.33', 'hoz_menu', 0),
(1199, 13, 'newslettersubscribe.40', 'newsletter', 0),
(1200, 13, 'ocvermegamenu.34', 'ver_menu', 0),
(1194, 9, 'ocvermegamenu.34', 'ver_menu', 0),
(1193, 9, 'newslettersubscribe.40', 'newsletter', 0),
(1192, 9, 'ochozmegamenu.33', 'hoz_menu', 0),
(1191, 9, 'carousel.45', 'footer2', 0),
(1147, 17, 'newslettersubscribe.40', 'newsletter', 0),
(1146, 17, 'ochozmegamenu.33', 'hoz_menu', 0),
(1145, 17, 'carousel.45', 'footer2', 0),
(1073, 18, 'newslettersubscribe.40', 'newsletter', 0),
(1072, 18, 'ochozmegamenu.33', 'hoz_menu', 0),
(1071, 18, 'carousel.45', 'footer2', 0),
(1070, 18, 'occmsblock.46', 'footer1', 0),
(1677, 14, 'octestimonial.42', 'tesmonial', 0),
(1256, 23, 'newslettersubscribe.40', 'newsletter', 0),
(1248, 20, 'newslettersubscribe.40', 'newsletter', 0),
(1098, 7, 'newslettersubscribe.40', 'newsletter', 0),
(1097, 7, 'ochozmegamenu.33', 'hoz_menu', 0),
(1096, 7, 'carousel.45', 'footer2', 0),
(1074, 18, 'ocvermegamenu.34', 'ver_menu', 0),
(1249, 20, 'ocvermegamenu.34', 'ver_menu', 0),
(1247, 20, 'ochozmegamenu.33', 'hoz_menu', 0),
(1245, 20, 'occmsblock.46', 'footer1', 0),
(1246, 20, 'carousel.45', 'footer2', 0),
(1243, 20, 'occmsblock.49', 'column_left', 1),
(1675, 14, 'occmsblock.43', 'block_service', 0),
(1674, 14, 'newslettersubscribe.40', 'newsletter', 0),
(1673, 14, 'carousel.45', 'footer2', 0),
(1847, 1, 'octabproductslider.36', 'block_top', 1),
(1672, 14, 'occmsblock.46', 'footer1', 0),
(1671, 14, 'octabcategoryslider.71', 'content_top', 5),
(1846, 1, 'occmsblock.35', 'block_top', 0),
(1724, 15, 'octestimonial.42', 'column_right', 2),
(1722, 15, 'occountdown.62', 'column_right', 0),
(1723, 15, 'ocnewproductslider.39', 'column_right', 1),
(1720, 15, 'carousel.45', 'footer2', 0),
(1866, 16, 'occmsblock.43', 'block_service', 0),
(1845, 1, 'octabcategoryslider.68', 'content_top', 3),
(1865, 16, 'newslettersubscribe.40', 'newsletter', 0),
(1864, 16, 'carousel.45', 'footer2', 0),
(1863, 16, 'occmsblock.46', 'footer1', 0),
(1862, 16, 'octabproductslider.36', 'block_top', 1),
(1861, 16, 'occmsblock.61', 'block_top', 0),
(1037, 6, 'ocvermegamenu.34', 'ver_menu', 1),
(1895, 3, 'occmsblock.46', 'footer1', 0),
(1894, 3, 'ocvermegamenu.34', 'ver_menu', 0),
(1099, 7, 'ocvermegamenu.34', 'ver_menu', 0),
(1095, 7, 'occmsblock.46', 'footer1', 0),
(1111, 12, 'occmsblock.46', 'footer1', 0),
(1126, 8, 'ocvermegamenu.34', 'ver_menu', 0),
(1122, 8, 'occmsblock.46', 'footer1', 0),
(1670, 14, 'octabcategoryslider.70', 'content_top', 4),
(1137, 11, 'ocvermegamenu.34', 'ver_menu', 0),
(1133, 11, 'occmsblock.46', 'footer1', 0),
(1148, 17, 'ocvermegamenu.34', 'ver_menu', 0),
(1144, 17, 'occmsblock.46', 'footer1', 0),
(1900, 5, 'ochozmegamenu.33', 'hoz_menu', 0),
(1899, 5, 'account', 'column_left', 2),
(1168, 2, 'occmsblock.46', 'footer1', 0),
(1196, 13, 'occmsblock.46', 'footer1', 0),
(1190, 9, 'occmsblock.46', 'footer1', 0),
(1028, 21, 'carousel.45', 'footer2', 0),
(1029, 21, 'occmsblock.46', 'footer1', 0),
(1030, 21, 'ochozmegamenu.54', 'hoz_menu', 0),
(1031, 21, 'newslettersubscribe.40', 'newsletter', 0),
(1039, 22, 'carousel.45', 'footer2', 0),
(1040, 22, 'newslettersubscribe.40', 'newsletter', 0),
(1041, 22, 'occmsblock.46', 'footer1', 0),
(1042, 22, 'ochozmegamenu.54', 'hoz_menu', 0),
(1255, 23, 'ochozmegamenu.54', 'hoz_menu', 0),
(1254, 23, 'carousel.45', 'footer2', 0),
(1253, 23, 'occmsblock.46', 'footer1', 0),
(1251, 23, 'occmsblock.49', 'column_left', 1),
(1250, 23, 'occountdown.62', 'column_left', 0),
(1242, 20, 'occountdown.62', 'column_left', 0),
(1065, 24, 'occmsblock.46', 'footer1', 0),
(1066, 24, 'carousel.45', 'footer2', 0),
(1067, 24, 'ochozmegamenu.54', 'hoz_menu', 0),
(1068, 24, 'newslettersubscribe.40', 'newsletter', 0),
(1882, 25, 'ochozmegamenu.54', 'hoz_menu', 0),
(1881, 25, 'occmsblock.49', 'column_left', 1),
(1880, 25, 'oclayerednavigation', 'column_left', 0),
(1103, 26, 'ochozmegamenu.54', 'hoz_menu', 0),
(1102, 26, 'carousel.45', 'footer2', 0),
(1101, 26, 'occmsblock.46', 'footer1', 0),
(1104, 26, 'newslettersubscribe.40', 'newsletter', 0),
(1106, 27, 'newslettersubscribe.40', 'newsletter', 0),
(1107, 27, 'carousel.45', 'footer2', 0),
(1108, 27, 'occmsblock.46', 'footer1', 0),
(1109, 27, 'ochozmegamenu.54', 'hoz_menu', 0),
(1117, 28, 'carousel.45', 'footer2', 0),
(1118, 28, 'newslettersubscribe.40', 'newsletter', 0),
(1119, 28, 'occmsblock.46', 'footer1', 0),
(1120, 28, 'ochozmegamenu.54', 'hoz_menu', 0),
(1128, 29, 'carousel.45', 'footer2', 0),
(1129, 29, 'newslettersubscribe.40', 'newsletter', 0),
(1130, 29, 'occmsblock.46', 'footer1', 0),
(1131, 29, 'ochozmegamenu.54', 'hoz_menu', 0),
(1139, 30, 'carousel.45', 'footer2', 0),
(1140, 30, 'newslettersubscribe.40', 'newsletter', 0),
(1141, 30, 'occmsblock.46', 'footer1', 0),
(1142, 30, 'ochozmegamenu.54', 'hoz_menu', 0),
(1149, 31, 'account', 'column_left', 0),
(1150, 31, 'carousel.45', 'footer2', 0),
(1151, 31, 'newslettersubscribe.40', 'newsletter', 0),
(1153, 31, 'occmsblock.46', 'footer1', 0),
(1154, 31, 'ochozmegamenu.54', 'hoz_menu', 0),
(1163, 32, 'newslettersubscribe.40', 'newsletter', 0),
(1165, 32, 'occmsblock.46', 'footer1', 0),
(1166, 32, 'ochozmegamenu.54', 'hoz_menu', 0),
(1172, 2, 'ocvermegamenu.34', 'ver_menu', 0),
(1205, 33, 'newslettersubscribe.40', 'newsletter', 0),
(1204, 33, 'ochozmegamenu.54', 'hoz_menu', 0),
(1203, 33, 'carousel.45', 'footer2', 0),
(1202, 33, 'occmsblock.46', 'footer1', 0),
(1184, 34, 'carousel.45', 'footer2', 0),
(1185, 34, 'newslettersubscribe.40', 'newsletter', 0),
(1187, 34, 'occmsblock.46', 'footer1', 0),
(1188, 34, 'ochozmegamenu.54', 'hoz_menu', 0),
(1669, 14, 'octabproductslider.36', 'content_top', 3),
(1667, 14, 'occmsblock.51', 'content_top', 1),
(1721, 15, 'newslettersubscribe.40', 'newsletter', 0),
(1719, 15, 'occmsblock.46', 'footer1', 0),
(1718, 15, 'octabcategoryslider.68', 'content_top', 4),
(1717, 15, 'octabcategoryslider.69', 'content_top', 3),
(1716, 15, 'octabproductslider.57', 'content_top', 2),
(1860, 16, 'octabcategoryslider.68', 'content_top', 2),
(1859, 16, 'octabcategoryslider.69', 'content_top', 1),
(1898, 3, 'mega_filter.1', 'column_left', 0),
(1844, 1, 'octabcategoryslider.69', 'content_top', 2),
(1841, 1, 'ocslideshow.32', 'banner7', 0),
(1665, 14, 'ocslideshow.50', 'banner7', 0),
(1664, 14, 'ocvermegamenu.34', 'ver_menu', 0),
(1663, 14, 'ochozmegamenu.33', 'hoz_menu', 0),
(1715, 15, 'occmsblock.56', 'content_top', 1),
(1713, 15, 'ocslideshow.55', 'content_top', 0),
(1712, 15, 'ochozmegamenu.54', 'hoz_menu', 0),
(1856, 16, 'occmsblock.59', 'banner7_left', 0),
(1726, 15, 'occmsblock.43', 'column_right', 4),
(1855, 16, 'ocslideshow.58', 'banner7', 0),
(1854, 16, 'ochozmegamenu.54', 'hoz_menu', 0),
(1840, 1, 'ocvermegamenu.34', 'ver_menu', 0),
(1839, 1, 'ochozmegamenu.33', 'hoz_menu', 0),
(1838, 1, 'occmsblock.49', 'column_left', 2),
(1837, 1, 'ocnewproductslider.39', 'column_left', 1),
(1836, 1, 'occountdown.62', 'column_left', 0),
(1868, 16, 'octestimonial.42', 'tesmonial', 0),
(1869, 16, 'occountdown.62', 'column_right', 1),
(1870, 16, 'occmsblock.49', 'column_right', 2),
(1871, 16, 'ocnewproductslider.39', 'column_right', 3),
(1893, 3, 'ochozmegamenu.33', 'hoz_menu', 0),
(1892, 3, 'occmsblock.49', 'column_left', 1),
(1884, 25, 'carousel.45', 'footer2', 0),
(1885, 25, 'newslettersubscribe.40', 'newsletter', 0),
(1904, 5, 'newslettersubscribe.40', 'newsletter', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(54, 4, 0, ''),
(470, 13, 0, 'product/special'),
(538, 5, 0, 'product/manufacturer'),
(383, 6, 0, 'account/%'),
(388, 10, 0, 'affiliate/%'),
(455, 2, 0, 'product/product'),
(436, 11, 0, 'information/information'),
(419, 7, 0, 'checkout/%'),
(431, 8, 0, 'information/contact'),
(466, 9, 0, 'information/sitemap'),
(537, 5, 0, 'product/manufacturer%'),
(426, 12, 0, 'product/compare'),
(468, 13, 0, 'product/search'),
(441, 17, 0, 'product/octestimonial'),
(407, 18, 0, 'blog/blog'),
(406, 18, 0, 'blog/article%'),
(484, 20, 0, 'product/ocbestseller'),
(481, 20, 0, 'product/ocnewproduct'),
(536, 3, 0, 'product/category'),
(528, 1, 0, 'common/home'),
(535, 36, 0, 'product/manufacturer/info'),
(534, 35, 0, 'module/mega_filter/results');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'HTC', 'catalog/demo/htc_logo.jpg', 0),
(6, 'Palm', 'catalog/demo/palm_logo.jpg', 0),
(7, 'Hewlett-Packard', 'catalog/demo/hp_logo.jpg', 0),
(8, 'Apple', 'catalog/demo/apple_logo.jpg', 0),
(9, 'Canon', 'catalog/demo/canon_logo.jpg', 0),
(10, 'Sony', 'catalog/demo/sony_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(5, 1),
(5, 2),
(5, 3),
(6, 0),
(6, 1),
(6, 2),
(6, 3),
(7, 0),
(7, 1),
(7, 2),
(7, 3),
(8, 0),
(8, 1),
(8, 2),
(8, 3),
(9, 0),
(9, 1),
(9, 2),
(9, 3),
(10, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_menu`
--

CREATE TABLE `oc_menu` (
  `menu_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `type` varchar(6) NOT NULL,
  `link` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_menu_description`
--

CREATE TABLE `oc_menu_description` (
  `menu_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_menu_module`
--

CREATE TABLE `oc_menu_module` (
  `menu_module_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_mfilter_settings`
--

CREATE TABLE `oc_mfilter_settings` (
  `idx` int(11) UNSIGNED NOT NULL,
  `settings` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `oc_mfilter_settings`
--

INSERT INTO `oc_mfilter_settings` (`idx`, `settings`) VALUES
(1, '{\"attribs\":{\"default\":{\"enabled\":\"0\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"0\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\"}},\"options\":{\"default\":{\"enabled\":\"0\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"0\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\"}},\"base_attribs\":{\"price\":{\"enabled\":\"1\",\"sort_order\":\"-1\",\"collapsed\":\"0\"},\"search\":{\"enabled\":\"1\",\"sort_order\":\"\",\"collapsed\":\"\",\"refresh_delay\":\"1000\",\"button\":\"0\"},\"manufacturers\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"checkbox\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"model\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"sku\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"upc\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"ean\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"jan\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"isbn\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"mpn\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"location\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"text\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"length\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"slider\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"width\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"slider\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"height\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"slider\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"weight\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"slider\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"discounts\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"checkbox\",\"display_list_of_items\":\"\",\"collapsed\":\"0\",\"display_live_filter\":\"\"},\"stock_status\":{\"enabled\":\"0\",\"sort_order\":\"\",\"display_as_type\":\"checkbox\",\"display_list_of_items\":\"\",\"collapsed\":\"0\"},\"rating\":{\"enabled\":\"0\",\"sort_order\":\"\",\"collapsed\":\"0\"}},\"configuration\":{\"show_loader_over_results\":\"1\",\"show_loader_over_filter\":\"0\",\"auto_scroll_to_results\":\"0\",\"add_pixels_from_top\":\"0\",\"ajax_pagination\":\"0\",\"refresh_results\":\"immediately\",\"display_list_of_items\":{\"type\":\"scroll\",\"max_height\":\"155\",\"standard_scroll\":\"0\",\"limit_of_items\":\"4\"},\"display_live_filter\":{\"enabled\":\"0\",\"items\":\"1\"},\"background_color_counter\":\"#428bca\",\"text_color_counter\":\"#ffffff\",\"background_color_search_button\":\"#428bca\",\"background_color_slider\":\"\",\"background_color_header\":\"\",\"text_color_header\":\"\",\"border_bottom_color_header\":\"\",\"image_size_width\":\"20\",\"image_size_height\":\"20\",\"color_of_loader_over_results\":\"\",\"color_of_loader_over_filter\":\"\",\"background_color_widget_button\":\"\",\"css_style\":\"\",\"javascript\":\"MegaFilter.prototype.beforeRequest = function() {\\n\\tvar self = this;\\n};\\n\\nMegaFilter.prototype.beforeRender = function( htmlResponse, htmlContent, json ) {\\n\\tvar self = this;\\n};\\n\\nMegaFilter.prototype.afterRender = function( htmlResponse, htmlContent, json ) {\\n\\tvar self = this;\\n};\\n\",\"content_selector\":\"#mfilter-content-container\",\"type_of_condition\":\"or\",\"calculate_number_of_products\":\"1\",\"show_number_of_products\":\"1\",\"hide_inactive_values\":\"0\",\"show_reset_button\":\"0\",\"show_top_reset_button\":\"0\",\"in_stock_default_selected\":\"0\",\"manual_init\":\"0\",\"change_top_to_column_on_mobile\":\"0\"},\"filters\":{\"based_on_category\":\"1\",\"default\":{\"enabled\":\"0\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"0\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\"},\"3\":{\"enabled\":\"0\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"1\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\",\"sort_order\":\"\"},\"1\":{\"enabled\":\"1\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"1\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\",\"sort_order\":\"\"}},\"name\":\"\",\"title\":{\"1\":\"Mega Filter PRO\"},\"layout_id\":[\"3\"],\"store_id\":[\"0\"],\"position\":\"column_left\",\"inline_horizontal\":\"0\",\"display_always_as_widget\":\"0\",\"widget_with_swipe\":\"1\",\"display_selected_filters\":\"0\",\"status\":\"1\",\"sort_order\":\"\"}');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_mfilter_url_alias`
--

CREATE TABLE `oc_mfilter_url_alias` (
  `mfilter_url_alias_id` int(11) UNSIGNED NOT NULL,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `mfp` text COLLATE utf8_unicode_ci NOT NULL,
  `alias` text COLLATE utf8_unicode_ci NOT NULL,
  `language_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `h1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Blog For Opencart 2.3.x', 'oc_blog', 'Plaza Theme', '1.0', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n    <code>oc_blog</code>\r\n    <name>Blog For Opencart 2.3.x</name>\r\n    <version>1.0</version>\r\n    <author>Plaza Theme</author>\r\n    \r\n    <file path=\"admin/controller/common/column_left.php\">\r\n        <operation>\r\n            <search><![CDATA[\r\n				if ($this->user->hasPermission(\'access\', \'marketing/marketing\')) {\r\n            ]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                $this->load->language(\'extension/module/ocblog\');\r\n\r\n                $blog_menu = array();\r\n\r\n                if ($this->user->hasPermission(\'access\', \'blog/article\')) {\r\n                    $blog_menu[] = array(\r\n                        \'name\' => $this->language->get(\'text_blog_article\'),\r\n                        \'href\' => $this->url->link(\'blog/article\', \'token=\' . $this->session->data[\'token\'], true),\r\n                        \'children\' => array()\r\n                    );\r\n                }\r\n\r\n                if ($this->user->hasPermission(\'access\', \'blog/articlelist\')) {\r\n                    $blog_menu[] = array(\r\n                        \'name\' => $this->language->get(\'text_blog_article_list\'),\r\n                        \'href\' => $this->url->link(\'blog/articlelist\', \'token=\' . $this->session->data[\'token\'], true),\r\n                        \'children\' => array()\r\n                    );\r\n                }\r\n\r\n                if($blog_menu) {\r\n                    $data[\'menus\'][] = array(\r\n                        \'id\'       => \'menu-blog\',\r\n                        \'icon\'     => \'fa-pencil-square-o\', \r\n                        \'name\'     => $this->language->get(\'text_blog\'),\r\n                        \'href\'     => \'\',\r\n                        \'children\' => $blog_menu\r\n                    );\r\n                }\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n	\r\n	<file path=\"catalog/view/theme/default/template/common/footer.tpl\">\r\n        <operation>\r\n            <search trim=\"true\" index=\"0\"><![CDATA[\r\n          <?php foreach ($informations as $information) { ?>\r\n            ]]></search>\r\n            <add position=\"before\" trim=\"false\" offset=\"0\"><![CDATA[\r\n				<li><a href=\"<?php echo $blog[\'href\']; ?>\"><?php echo $blog[\'title\']; ?></a></li>\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n	\r\n	<file path=\"catalog/controller/common/footer.php\">\r\n        <operation>\r\n            <search trim=\"true\" index=\"0\"><![CDATA[\r\n				$data[\'informations\'] = array();\r\n            ]]></search>\r\n            <add position=\"before\" trim=\"false\" offset=\"0\"><![CDATA[\r\n				$data[\'blog\'] = array(\r\n					\'title\' => $this->config->get(\'ocblog_meta_title\'),\r\n					\'href\'  => $this->url->link(\'blog/blog\')\r\n				);\r\n            ]]></add>\r\n        </operation>\r\n    </file>\r\n\r\n    <file path=\"admin/controller/extension/theme/theme_default.php\">\r\n        <operation>\r\n            <search><![CDATA[$this->load->language(\'extension/theme/theme_default\');]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    // Blog\r\n                    $this->load->language(\'extension/module/ocblog\');\r\n                    // End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$data[\'entry_image_related\'] = $this->language->get(\'entry_image_related\');]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    // Blog\r\n                    $data[\'entry_image_article\'] = $this->language->get(\'entry_image_article\');\r\n                    $data[\'entry_image_blog\'] = $this->language->get(\'entry_image_blog\');\r\n                    // End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[if (isset($this->error[\'image_related\'])) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    // Blog\r\n                    if (isset($this->error[\'image_article\'])) {\r\n                        $data[\'error_image_article\'] = $this->error[\'image_article\'];\r\n                    } else {\r\n                        $data[\'error_image_article\'] = \'\';\r\n                    }\r\n\r\n                    if (isset($this->error[\'image_blog\'])) {\r\n                        $data[\'error_image_blog\'] = $this->error[\'image_blog\'];\r\n                    } else {\r\n                        $data[\'error_image_blog\'] = \'\';\r\n                    }\r\n                    // End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[if (isset($this->request->post[\'theme_default_image_related_width\'])) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    // Blog\r\n                    if (isset($this->request->post[\'theme_default_image_article_width\'])) {\r\n                        $data[\'theme_default_image_article_width\'] = $this->request->post[\'theme_default_image_article_width\'];\r\n                    } elseif (isset($setting_info[\'theme_default_image_article_width\'])) {\r\n                        $data[\'theme_default_image_article_width\'] = $this->config->get(\'theme_default_image_article_width\');\r\n                    } else {\r\n                        $data[\'theme_default_image_article_width\'] = 400;\r\n                    }\r\n\r\n                    if (isset($this->request->post[\'theme_default_image_article_height\'])) {\r\n                        $data[\'theme_default_image_article_height\'] = $this->request->post[\'theme_default_image_article_height\'];\r\n                    } elseif (isset($setting_info[\'theme_default_image_article_height\'])) {\r\n                        $data[\'theme_default_image_article_height\'] = $this->config->get(\'theme_default_image_article_height\');\r\n                    } else {\r\n                        $data[\'theme_default_image_article_height\'] = 400;\r\n                    }\r\n\r\n                    if (isset($this->request->post[\'theme_default_image_blog_width\'])) {\r\n                        $data[\'theme_default_image_blog_width\'] = $this->request->post[\'theme_default_image_blog_width\'];\r\n                    } elseif (isset($setting_info[\'theme_default_image_blog_width\'])) {\r\n                        $data[\'theme_default_image_blog_width\'] = $this->config->get(\'theme_default_image_blog_width\');\r\n                    } else {\r\n                        $data[\'theme_default_image_blog_width\'] = 100;\r\n                    }\r\n\r\n                    if (isset($this->request->post[\'theme_default_image_blog_height\'])) {\r\n                        $data[\'theme_default_image_blog_height\'] = $this->request->post[\'theme_default_image_blog_height\'];\r\n                    } elseif (isset($setting_info[\'theme_default_image_blog_height\'])) {\r\n                        $data[\'theme_default_image_blog_height\'] = $this->config->get(\'theme_default_image_blog_height\');\r\n                    } else {\r\n                        $data[\'theme_default_image_blog_height\'] = 100;\r\n                    }\r\n                    // End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[if (!$this->request->post[\'theme_default_image_related_width\'] || !$this->request->post[\'theme_default_image_related_height\']) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    // Blog\r\n                    $this->load->language(\'module/ocblog\');\r\n                    if (!$this->request->post[\'theme_default_image_article_width\'] || !$this->request->post[\'theme_default_image_article_height\']) {\r\n                        $this->error[\'image_article\'] = $this->language->get(\'error_image_article\');\r\n                    }\r\n\r\n                    if (!$this->request->post[\'theme_default_image_blog_width\'] || !$this->request->post[\'theme_default_image_blog_height\']) {\r\n                        $this->error[\'image_blog\'] = $this->language->get(\'error_image_blog\');\r\n                    }\r\n                    // End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n\r\n    <file path=\"admin/view/template/extension/theme/theme_default.tpl\">\r\n        <operation>\r\n            <search><![CDATA[<label class=\"col-sm-2 control-label\" for=\"input-image-related\"><?php echo $entry_image_related; ?></label>]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    <label class=\"col-sm-2 control-label\" for=\"input-image-article\"><?php echo $entry_image_article; ?></label>\r\n                    <div class=\"col-sm-10\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-6\">\r\n                          <input type=\"text\" name=\"theme_default_image_article_width\" value=\"<?php echo $theme_default_image_article_width; ?>\" placeholder=\"<?php echo $entry_width; ?>\" id=\"input-image-article\" class=\"form-control\" />\r\n                        </div>\r\n                        <div class=\"col-sm-6\">\r\n                          <input type=\"text\" name=\"theme_default_image_article_height\" value=\"<?php echo $theme_default_image_article_height; ?>\" placeholder=\"<?php echo $entry_height; ?>\" class=\"form-control\" />\r\n                        </div>\r\n                      </div>\r\n                      <?php if ($error_image_article) { ?>\r\n                      <div class=\"text-danger\"><?php echo $error_image_article; ?></div>\r\n                      <?php } ?>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group required\">\r\n                  <label class=\"col-sm-2 control-label\" for=\"input-image-blog\"><?php echo $entry_image_blog; ?></label>\r\n                    <div class=\"col-sm-10\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-6\">\r\n                          <input type=\"text\" name=\"theme_default_image_blog_width\" value=\"<?php echo $theme_default_image_blog_width; ?>\" placeholder=\"<?php echo $entry_width; ?>\" id=\"input-image-blog\" class=\"form-control\" />\r\n                        </div>\r\n                        <div class=\"col-sm-6\">\r\n                          <input type=\"text\" name=\"theme_default_image_blog_height\" value=\"<?php echo $theme_default_image_blog_height; ?>\" placeholder=\"<?php echo $entry_height; ?>\" class=\"form-control\" />\r\n                        </div>\r\n                      </div>\r\n                      <?php if ($error_image_blog) { ?>\r\n                      <div class=\"text-danger\"><?php echo $error_image_blog; ?></div>\r\n                      <?php } ?>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group required\">\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n\r\n</modification>\r\n', 0, '2016-09-10 10:38:47', '2016-09-10 10:38:47'),
(2, 'Category Thumbnail for v2.3.x by Plaza Themes', 'category_thumbnail', 'Plaza Theme', '1.0', 'http://www.plazathemes.com/', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n    <name>Category Thumbnail for v2.3.x by Plaza Themes</name>\r\n	<version>1.0</version>\r\n	<link>http://www.plazathemes.com/</link>\r\n	<author>Plaza Theme</author>\r\n	<code>category_thumbnail</code>\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'catalog/category\')) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				// Categories\r\n\r\n				$categories = array();\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'catalog/category\')) {]]></search>\r\n			<add position=\"after\">\r\n			<![CDATA[\r\n				$categories[] = array(\r\n					\'name\'	   => $this->language->get(\'text_category\'),\r\n					\'href\'     => $this->url->link(\'catalog/category\', \'token=\' . $this->session->data[\'token\'], true),\r\n					\'children\' => array()\r\n				);\r\n\r\n			}\r\n\r\n			$this->load->language(\'catalog/occategorythumbnail\');\r\n\r\n			if($this->user->hasPermission(\'access\', \'catalog/occategorythumbnail\')) {\r\n				$categories[] = array(\r\n					\'name\'	   => $this->language->get(\'text_thumbnail\'),\r\n					\'href\'     => $this->url->link(\'catalog/occategorythumbnail\', \'token=\' . $this->session->data[\'token\'], true),\r\n					\'children\' => array()\r\n				);\r\n			}\r\n\r\n			/*		\r\n			]]>\r\n			</add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'catalog/product\')) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				*/\r\n\r\n				if($categories) {\r\n					$catalog[] = array(\r\n						\'name\'	   => $this->language->get(\'text_category\'),\r\n						\'href\'     => \'\',\r\n						\'children\' => $categories\r\n					);\r\n				}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n</modification>', 1, '2016-09-10 10:38:50', '2017-11-02 15:02:15'),
(3, 'Layered Navigation Ajax by Plaza Themes', 'layered_navigation', 'Plaza Theme', '1.1', 'http://www.plazathemes.com/', '<modification>\r\n    <name>Layered Navigation Ajax by Plaza Themes</name>\r\n	<version>1.1</version>\r\n	<link>http://www.plazathemes.com/</link>\r\n	<author>Plaza Theme</author>\r\n	<code>layered_navigation</code>\r\n\r\n	<file path=\"catalog/controller/product/category.php\">\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'product/category\', $data));]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n				/* Edit for Layered Navigation Ajax Module by OCMod */\r\n				$module_status = $this->config->get(\'oclayerednavigation_status\');\r\n				if($module_status) {\r\n					$data[\'oclayerednavigation_loader_img\'] = $this->config->get(\'config_url\') . \'image/\' . $this->config->get(\'oclayerednavigation_loader_img\');\r\n                    $this->response->setOutput($this->load->view(\'extension/module/oclayerednavigation/occategory.tpl\', $data));\r\n				} else {\r\n                    $this->response->setOutput($this->load->view(\'product/category\', $data));\r\n                }\r\n			]]></add>\r\n		</operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[\'href\' => $this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'_\' . $result[\'category_id\'] . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[\'href\'  => $this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $result[\'category_id\'] . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=p.sort_order&order=ASC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=p.sort_order&order=ASC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=pd.name&order=ASC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=pd.name&order=ASC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=pd.name&order=DESC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=pd.name&order=DESC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=p.price&order=ASC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=p.price&order=ASC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=p.price&order=DESC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=p.price&order=DESC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=rating&order=DESC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=rating&order=DESC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=rating&order=ASC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=rating&order=ASC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=p.model&order=ASC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=p.model&order=ASC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . \'&sort=p.model&order=DESC\' . $url)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . \'&sort=p.model&order=DESC\' . $url]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . $url . \'&limit=\' . $value)]]></search>\r\n            <add position=\"replace\"><![CDATA[$this->config->get(\'config_url\') . \'index.php?route=product/category&path=\' . $category_id . $url . \'&limit=\' . $value]]></add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[$pagination->url = $this->url->link(\'product/category\', \'path=\' . $this->request->get[\'path\'] . $url . \'&page={page}\');]]></search>\r\n            <add position=\"replace\"><![CDATA[$pagination->url = $this->config->get(\'config_url\') . \'index.php?route=extension/module/oclayerednavigation/category&path=\' . $category_id . $url . \'&page={page}\';]]></add>\r\n        </operation>\r\n	</file>\r\n\r\n    <file path=\"catalog/model/catalog/product.php\">\r\n        <operation>\r\n            <search><![CDATA[if (!empty($data[\'filter_manufacturer_id\'])) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    /* Price range */\r\n                    if  (!empty($data[\'filter_price\'])) {\r\n                        $min_price = $data[\'filter_price\'][\'min_price\'];\r\n                        $max_price = $data[\'filter_price\'][\'max_price\'];\r\n            \r\n                        $sql .=  \" AND p.price >=\'\". $min_price .\"\' AND p.price <=\'\". $max_price .\"\'\" ;\r\n                    }\r\n                    /* End */\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n</modification>', 0, '2016-09-10 10:38:53', '2017-11-02 14:59:43'),
(4, 'Product Rotator Image by Plaza Themes', 'product_rotator', 'Plaza Theme', '1.0', 'http://www.plazathemes.com/', '<modification>\r\n    <name>Product Rotator Image by Plaza Themes</name>\r\n	<version>1.0</version>\r\n	<link>http://www.plazathemes.com/</link>\r\n	<author>Plaza Theme</author>\r\n	<code>product_rotator</code>\r\n\r\n    <!-- Hook into admin product controller -->\r\n	<file path=\"admin/controller/catalog/product.php\">\r\n		<operation>\r\n			<search ><![CDATA[public function index() {]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				/* Product Rotator */\r\n                $this->load->model(\'catalog/ocproductrotator\');\r\n        \r\n                $this->model_catalog_ocproductrotator->installProductRotator();\r\n			]]></add>\r\n		</operation>\r\n        <operation>\r\n            <search ><![CDATA[$this->load->language(\'catalog/product\');]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                /* Product Rotator */\r\n                $this->load->language(\'extension/module/ocproductrotator\');\r\n            ]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search ><![CDATA[$data[\'entry_image\'] = $this->language->get(\'entry_image\');]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n				/* Product Rotator */\r\n                $data[\'entry_is_rotator\'] = $this->language->get(\'entry_is_rotator\');\r\n			]]></add>\r\n        </operation>\r\n        <operation>\r\n            <search ><![CDATA[\'sort_order\' => $product_image[\'sort_order\']]]></search>\r\n            <add position=\"before\"><![CDATA[\r\n                /* Product Rotator */\r\n                \'is_rotator\' => $product_image[\'is_rotator\'],\r\n            ]]></add>\r\n        </operation>\r\n	</file>\r\n\r\n    <!-- Hook into admin product model -->\r\n    <file path=\"admin/model/catalog/product.php\">\r\n        <operation>\r\n            <search><![CDATA[$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"product_image SET product_id = \'\" . (int)$product_id . \"\', image = \'\" . $this->db->escape($product_image[\'image\']) . \"\', sort_order = \'\" . (int)$product_image[\'sort_order\'] . \"\'\");]]></search>\r\n            <add position=\"replace\">\r\n                <![CDATA[\r\n                    /* Product Rotator */\r\n                    $this->db->query(\"INSERT INTO \" . DB_PREFIX . \"product_image SET product_id = \'\" . (int)$product_id . \"\', image = \'\" . $this->db->escape($product_image[\'image\']) . \"\', sort_order = \'\" . (int)$product_image[\'sort_order\'] . \"\', is_rotator = \'\" . (int)$product_image[\'is_rotator\'] . \"\'\");\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n\r\n    <!-- Modify admin product form -->\r\n    <file path=\"admin/view/template/catalog/product_form.tpl\">\r\n        <operation>\r\n            <search><![CDATA[<td class=\"text-right\"><?php echo $entry_sort_order; ?></td>]]></search>\r\n            <add position=\"after\">\r\n                <![CDATA[\r\n                    <!-- Product Rotator -->\r\n                    <td class=\"text-center\"><?php echo $entry_is_rotator; ?></td>\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[<td class=\"text-left\"><button type=\"button\" onclick=\"$(\'#image-row<?php echo $image_row; ?>\').remove();\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    <!-- Product Rotator -->\r\n                    <td class=\"text-center\">\r\n                    <select name=\"product_image[<?php echo $image_row; ?>][is_rotator]\" class=\"form-control rotator-select\">\r\n                      <?php if(isset($product_image[\'is_rotator\']) && (int) $product_image[\'is_rotator\'] == 1): ?>\r\n                        <option value=\"1\" selected=\"selected\">Yes</option>\r\n                        <option value=\"0\">No</option>\r\n                      <?php else: ?>\r\n                        <option value=\"1\">Yes</option>\r\n                        <option value=\"0\" selected=\"selected\">No</option>\r\n                      <?php endif; ?>\r\n                    </select>\r\n                    </td>\r\n                    <!-- End -->\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[function addImage() {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    //Product Rotator\r\n                    $(\'#tab-image tfoot td:first\').attr(\'colspan\', 3);\r\n                    \r\n                    $(\'.rotator-select\').change(function() {\r\n                      var value = $(this).val();\r\n                      if(value == 1) {\r\n                        $(\'.rotator-select\').val(0);\r\n                        $(this).val(1);\r\n                      }\r\n                    });\r\n                    //End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[$(\'#images tbody\').append(html);]]></search>\r\n            <add position=\"after\">\r\n                <![CDATA[\r\n                    //Product Rotator\r\n                    $(\'.rotator-select\').change(function() {\r\n                    var value = $(this).val();\r\n                    if(value == 1) {\r\n                      $(\'.rotator-select\').val(0);\r\n                      $(this).val(1);\r\n                    }\r\n                    });\r\n                    //End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[html += \'  <td class=\"text-left\"><button type=\"button\" onclick=\"$(\\\'#image-row\' + image_row  + \'\\\').remove();\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>\';]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    //Product Rotator\r\n                    html += \' <td class=\"text-center\">\';\r\n                    html += \'   <select name=\"product_image[\' + image_row + \'][is_rotator]\" class=\"form-control rotator-select\">\';\r\n                    html += \'     <option value=\"1\">Yes</option>\';\r\n                    html += \'     <option value=\"0\" selected=\"selected\">No</option>\';\r\n                    html += \'   </select>\';\r\n                    html += \' </td>\';\r\n                    //End\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n\r\n</modification>', 1, '2016-09-10 10:38:57', '2017-11-02 17:43:33'),
(5, 'Search Category by Plaza Themes', 'search_category', 'Plaza Theme', '1.0', 'http://www.plazathemes.com/', '<modification>\r\n    <name>Search Category by Plaza Themes</name>\r\n	<version>1.0</version>\r\n	<link>http://www.plazathemes.com/</link>\r\n	<author>Plaza Theme</author>\r\n	<code>search_category</code>\r\n\r\n	<file path=\"catalog/controller/common/header.php\">\r\n		<operation>\r\n			<search ><![CDATA[$data[\'search\'] = $this->load->controller(\'common/search\');]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n				/* Edit for Search Category Module by OCMod */\r\n				$module_status = $this->config->get(\'ocsearchcategory_status\');\r\n				if($module_status) {\r\n					$data[\'search\'] = $this->load->controller(\'extension/module/ocsearchcategory\');\r\n				} else {\r\n					$data[\'search\'] = $this->load->controller(\'common/search\');\r\n				}\r\n				/* End Code */\r\n			]]></add>\r\n		</operation>\r\n	</file>	\r\n</modification>', 0, '2016-09-10 10:39:01', '2017-11-02 15:01:33'),
(6, 'Testimonial by Plaza Themes', 'testimonial', 'Plaza Theme', '1.0', 'http://www.plazathemes.com/', '<modification>\r\n    <name>Testimonial by Plaza Themes</name>\r\n	<version>1.0</version>\r\n	<link>http://www.plazathemes.com/</link>\r\n	<author>Plaza Theme</author>\r\n	<code>testimonial</code>\r\n\r\n	<file path=\"admin/controller/common/column_left.php\">\r\n		<operation>\r\n			<search><![CDATA[if ($this->user->hasPermission(\'access\', \'catalog/recurring\')) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$this->load->language(\'catalog/octestimonial\');\r\n\r\n				if ($this->user->hasPermission(\'access\', \'catalog/octestimonial\')) {\r\n					$catalog[] = array(\r\n						\'name\'	   => $this->language->get(\'text_testimonial\'),\r\n						\'href\'     => $this->url->link(\'catalog/octestimonial\', \'token=\' . $this->session->data[\'token\'], true),\r\n						\'children\' => array()\r\n					);\r\n				}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/controller/extension/theme/theme_default.php\">\r\n		<operation>\r\n			<search><![CDATA[$this->load->language(\'extension/theme/theme_default\');]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$this->load->language(\'catalog/octestimonial\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[$data[\'text_product\'] = $this->language->get(\'text_product\');]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'text_testimonial\'] = $this->language->get(\'text_testimonial\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[$data[\'entry_product_limit\'] = $this->language->get(\'entry_product_limit\');]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'entry_testimonial_limit\'] = $this->language->get(\'entry_testimonial_limit\');\r\n				$data[\'entry_testimonial_image\'] = $this->language->get(\'entry_testimonial_image\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n            <search><![CDATA[if (isset($this->error[\'image_related\'])) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    if (isset($this->error[\'image_testimonial\'])) {\r\n                        $data[\'error_image_testimonial\'] = $this->error[\'image_testimonial\'];\r\n                    } else {\r\n                        $data[\'error_image_testimonial\'] = \'\';\r\n                    }\r\n\r\n                    if (isset($this->error[\'testimonial_limit\'])) {\r\n						$data[\'error_testimonial_limit\'] = $this->error[\'testimonial_limit\'];\r\n					} else {\r\n						$data[\'error_testimonial_limit\'] = \'\';\r\n					}\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[if (isset($this->request->post[\'theme_default_image_related_width\'])) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n	                if (isset($this->request->post[\'theme_default_testimonial_limit\'])) {\r\n						$data[\'theme_default_testimonial_limit\'] = $this->request->post[\'theme_default_testimonial_limit\'];\r\n					} elseif (isset($setting_info[\'theme_default_testimonial_limit\'])) {\r\n						$data[\'theme_default_testimonial_limit\'] = $setting_info[\'theme_default_testimonial_limit\'];\r\n					} else {\r\n						$data[\'theme_default_testimonial_limit\'] = 15;\r\n					}\r\n\r\n					if (isset($this->request->post[\'theme_default_image_testimonial_width\'])) {\r\n						$data[\'theme_default_image_testimonial_width\'] = $this->request->post[\'theme_default_image_testimonial_width\'];\r\n					} elseif (isset($setting_info[\'theme_default_image_testimonial_width\'])) {\r\n						$data[\'theme_default_image_testimonial_width\'] = $this->config->get(\'theme_default_image_testimonial_width\');\r\n					} else {\r\n						$data[\'theme_default_image_testimonial_width\'] = 100;\r\n					}\r\n\r\n					if (isset($this->request->post[\'theme_default_image_testimonial_height\'])) {\r\n						$data[\'theme_default_image_testimonial_height\'] = $this->request->post[\'theme_default_image_testimonial_height\'];\r\n					} elseif (isset($setting_info[\'theme_default_image_testimonial_height\'])) {\r\n						$data[\'theme_default_image_testimonial_height\'] = $this->config->get(\'theme_default_image_testimonial_height\');\r\n					} else {\r\n						$data[\'theme_default_image_testimonial_height\'] = 100;\r\n					}\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[if (!$this->request->post[\'theme_default_image_related_width\'] || !$this->request->post[\'theme_default_image_related_height\']) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                	if (!$this->request->post[\'theme_default_testimonial_limit\']) {\r\n                        $this->error[\'testimonial_limit\'] = $this->language->get(\'error_testimonial_limit\');\r\n                    }\r\n\r\n                    if (!$this->request->post[\'theme_default_image_testimonial_width\'] || !$this->request->post[\'theme_default_image_testimonial_height\']) {\r\n                        $this->error[\'image_testimonial\'] = $this->language->get(\'error_image_testimonial\');\r\n                    }\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n	</file>\r\n\r\n	<file path=\"admin/view/template/extension/theme/theme_default.tpl\">\r\n		<operation>\r\n            <search><![CDATA[<legend><?php echo $text_product; ?></legend>]]></search>\r\n			<add position=\"before\">\r\n                <![CDATA[\r\n                	<legend><?php echo $text_testimonial; ?></legend>\r\n                	<div class=\"form-group required\">\r\n		              <label class=\"col-sm-2 control-label\" for=\"input-testimonial-limit\"><?php echo $entry_testimonial_limit; ?></label>\r\n		              <div class=\"col-sm-10\">\r\n		                <input type=\"text\" name=\"theme_default_testimonial_limit\" value=\"<?php echo $theme_default_testimonial_limit; ?>\" placeholder=\"<?php echo $entry_testimonial_limit; ?>\" id=\"input-testimonial-limit\" class=\"form-control\" />\r\n		                <?php if ($error_testimonial_limit) { ?>\r\n		                <div class=\"text-danger\"><?php echo $error_testimonial_limit; ?></div>\r\n		                <?php } ?>\r\n		              </div>\r\n		            </div>\r\n	            </fieldset>\r\n          		<fieldset>\r\n                ]]>\r\n            </add>\r\n		</operation>\r\n\r\n        <operation>\r\n            <search><![CDATA[<label class=\"col-sm-2 control-label\" for=\"input-image-related\"><?php echo $entry_image_related; ?></label>]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                    <label class=\"col-sm-2 control-label\" for=\"input-image-testimonial\"><?php echo $entry_testimonial_image; ?></label>\r\n                    <div class=\"col-sm-10\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-sm-6\">\r\n                          <input type=\"text\" name=\"theme_default_image_testimonial_width\" value=\"<?php echo $theme_default_image_testimonial_width; ?>\" placeholder=\"<?php echo $entry_width; ?>\" id=\"input-image-testimonial\" class=\"form-control\" />\r\n                        </div>\r\n                        <div class=\"col-sm-6\">\r\n                          <input type=\"text\" name=\"theme_default_image_testimonial_height\" value=\"<?php echo $theme_default_image_testimonial_height; ?>\" placeholder=\"<?php echo $entry_height; ?>\" class=\"form-control\" />\r\n                        </div>\r\n                      </div>\r\n                      <?php if ($error_image_testimonial) { ?>\r\n                      <div class=\"text-danger\"><?php echo $error_image_testimonial; ?></div>\r\n                      <?php } ?>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group required\">\r\n                ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n</modification>', 1, '2016-09-10 10:39:04', '2017-11-02 15:02:40');
INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(17, 'Themes', 'themes', 'Plaza Theme', '1.0', 'http://www.plazathemes.com/', '<modification>\r\n    <name>Themes</name>\r\n	<version>1.0</version>\r\n	<link>http://www.plazathemes.com/</link>\r\n	<author>Plaza Theme</author>\r\n	<code>themes</code>\r\n	<file path=\"admin/model/localisation/language.php\">\r\n		<operation>\r\n			<search><![CDATA[foreach ($query->rows as $recurring) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			$query = $this->db->query(\"SELECT * FROM \" . DB_PREFIX . \"cmsblock_description WHERE language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\'\");\r\n\r\n			foreach ($query->rows as $cmsblock_description) {\r\n			$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"cmsblock_description SET cmsblock_des_id = \'\" . (int)$cmsblock_description[\'cmsblock_des_id\'] . \"\', language_id = \'\" . (int)$language_id . \"\', cmsblock_id = \'\" . (int)$cmsblock_description[\'cmsblock_id\'] . \"\', title = \'\" . $this->db->escape($cmsblock_description[\'title\']) . \"\', description = \'\" . $this->db->escape($cmsblock_description[\'description\']) . \"\'\");\r\n			}\r\n			$this->cache->delete(\'cmsblock_description\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"option_description WHERE language_id = \'\" . (int)$language_id . \"\'\");]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"cmsblock_description WHERE language_id = \'\" . (int)$language_id . \"\'\");\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[foreach ($query->rows as $recurring) {]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n			$query = $this->db->query(\"SELECT * FROM \" . DB_PREFIX . \"cmsblock_description WHERE language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\'\");\r\n\r\n			foreach ($query->rows as $cmsblock_description) {\r\n			$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"cmsblock_description SET cmsblock_des_id = \'\" . (int)$cmsblock_description[\'cmsblock_des_id\'] . \"\', language_id = \'\" . (int)$language_id . \"\', cmsblock_id = \'\" . (int)$cmsblock_description[\'cmsblock_id\'] . \"\', title = \'\" . $this->db->escape($cmsblock_description[\'title\']) . \"\', description = \'\" . $this->db->escape($cmsblock_description[\'description\']) . \"\'\");\r\n			}\r\n			$this->cache->delete(\'cmsblock_description\');\r\n\r\n			$query = $this->db->query(\"SELECT * FROM \" . DB_PREFIX . \"article_description WHERE language_id = \'\" . (int)$this->config->get(\'config_language_id\') . \"\'\");\r\n\r\n			foreach ($query->rows as $article_description) {\r\n			$this->db->query(\"INSERT INTO \" . DB_PREFIX . \"article_description SET article_id = \'\" . (int)$article_description[\'article_id\'] . \"\', language_id = \'\" . (int)$language_id . \"\', name = \'\" . $this->db->escape($article_description[\'name\']) . \"\', description = \'\" . $this->db->escape($article_description[\'description\']) . \"\', intro_text = \'\" . $this->db->escape($article_description[\'intro_text\']) . \"\', meta_title = \'\" . $this->db->escape($article_description[\'meta_title\']) . \"\', meta_description = \'\" . $this->db->escape($article_description[\'meta_description\']) . \"\', meta_keyword = \'\" . $this->db->escape($article_description[\'meta_keyword\']) . \"\'\");\r\n			}\r\n			$this->cache->delete(\'article_description\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"option_description WHERE language_id = \'\" . (int)$language_id . \"\'\");]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"cmsblock_description WHERE language_id = \'\" . (int)$language_id . \"\'\");\r\n			$this->db->query(\"DELETE FROM \" . DB_PREFIX . \"article_description WHERE language_id = \'\" . (int)$language_id . \"\'\");\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<!--<file path=\"catalog/controller/extension/module/banner.php\">-->\r\n		<!--<operation>-->\r\n			<!--<search><![CDATA[-->\r\n				<!--$this->load->model(\'design/banner\');-->\r\n			<!--]]></search>-->\r\n			<!--<add position=\"after\"><![CDATA[-->\r\n				<!--$this->load->language(\'module/banner\');-->\r\n			<!--]]></add>-->\r\n		<!--</operation>-->\r\n		<!--<operation>-->\r\n			<!--<search><![CDATA[-->\r\n				<!--$data[\'banners\'] = array();-->\r\n			<!--]]></search>-->\r\n			<!--<add position=\"after\"><![CDATA[-->\r\n				<!--$data[\'heading_title\'] = $this->language->get(\'heading_title\');-->\r\n			<!--]]></add>-->\r\n		<!--</operation>-->\r\n	<!--</file>-->\r\n	<file path=\"catalog/controller/extension/captcha/basic_captcha.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'route\'] = $this­>request­>get[\'route\'];]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			if(isset($this­>request­>get[\'route\'])) {\r\n			$data[\'route\'] = $this­>request­>get[\'route\'];\r\n			} else {\r\n			$data[\'route\'] = \"common/home\";\r\n			}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/extension/captcha/google_captcha.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'route\'] = $this­>request­>get[\'route\'];]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			if(isset($this­>request­>get[\'route\'])) {\r\n			$data[\'route\'] = $this­>request­>get[\'route\'];\r\n			} else {\r\n			$data[\'route\'] = \"common/home\";\r\n			}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/controller/design/layout.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'text_column_right\'] = $this->language->get(\'text_column_right\');]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'text_hoz_menu\'] = $this->language->get(\'text_hoz_menu\');\r\n				$data[\'text_ver_menu\'] = $this->language->get(\'text_ver_menu\');\r\n				$data[\'text_banner7\'] = $this->language->get(\'text_banner7\');\r\n				$data[\'text_banner7_left\'] = $this->language->get(\'text_banner7_left\');\r\n				$data[\'text_banner7_right\'] = $this->language->get(\'text_banner7_right\');\r\n				$data[\'text_block_top\'] = $this->language->get(\'text_block_top\');\r\n				$data[\'text_footer1\'] = $this->language->get(\'text_footer1\');\r\n				$data[\'text_footer2\'] = $this->language->get(\'text_footer2\');\r\n				$data[\'text_newsletter\'] = $this->language->get(\'text_newsletter\');\r\n				$data[\'text_block_service\'] = $this->language->get(\'text_block_service\');\r\n				$data[\'text_tesmonial\'] = $this->language->get(\'text_tesmonial\');\r\n				$data[\'text_module_blog\'] = $this->language->get(\'text_module_blog\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$this->response->setOutput($this->load->view(\'design/layout_form\', $data));]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n				$this->response->setOutput($this->load->view(\'design/layout_plaza_form.tpl\', $data));\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/language/en-gb/design/layout.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'text_column_right\']   = \'Column Right\';]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$_[\'text_hoz_menu\']       = \'Block Hoz_menu\';\r\n				$_[\'text_ver_menu\']       = \'Block Ver_menu\';\r\n				$_[\'text_banner7\']        = \'Block Banner7\';\r\n				$_[\'text_banner7_left\']   = \'Block Banner7 Left\';\r\n				$_[\'text_banner7_right\']  = \'Block Banner7 Right\';\r\n				$_[\'text_block_top\']      = \'Block Top\';\r\n				$_[\'text_footer1\']        = \'Block Static Bottom\';\r\n				$_[\'text_footer2\']        = \'Block Brands Logo\';\r\n				$_[\'text_newsletter\']     = \'Block Newsletter\';\r\n				$_[\'text_block_service\']  = \'Block Our Service\';\r\n				$_[\'text_tesmonial\']      = \'Block Testimonial\';\r\n				$_[\'text_module_blog\']    = \'Block Blog\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/common/header.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				$data[\'text_all\'] = $this->language->get(\'text_all\');\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'text_telephone\'] = $this->language->get(\'text_telephone\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\r\n				$data[\'cart\'] = $this->load->controller(\'common/cart\');\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'hoz_menu\'] = $this->load->controller(\'common/hoz_menu\');\r\n				$data[\'ver_menu\'] = $this->load->controller(\'common/ver_menu\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/common/home.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'content_bottom\'] = $this->load->controller(\'common/content_bottom\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'banner7\'] = $this->load->controller(\'common/banner7\');\r\n				$data[\'banner7_left\'] = $this->load->controller(\'common/banner7_left\');\r\n				$data[\'banner7_right\'] = $this->load->controller(\'common/banner7_right\');\r\n				$data[\'block_top\'] = $this->load->controller(\'common/block_top\');\r\n				$data[\'block_service\'] = $this->load->controller(\'common/block_service\');\r\n				$data[\'tesmonial\'] = $this->load->controller(\'common/tesmonial\');\r\n				$data[\'module_blog\'] = $this->load->controller(\'common/module_blog\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/common/footer.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'text_newsletter\'] = $this->language->get(\'text_newsletter\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'text_category_footer\'] = $this->language->get(\'text_category_footer\');\r\n				$data[\'text_category_footer_laptop\'] = $this->language->get(\'text_category_footer_laptop\');\r\n				$data[\'text_category_footer_mobile\'] = $this->language->get(\'text_category_footer_mobile\');\r\n				$data[\'text_category_footer_tablet\'] = $this->language->get(\'text_category_footer_tablet\');\r\n				$data[\'text_category_footer_headphone\'] = $this->language->get(\'text_category_footer_headphone\');\r\n				$data[\'text_category_footer_camera\'] = $this->language->get(\'text_category_footer_camera\');\r\n				$data[\'text_category_footer_accessories\'] = $this->language->get(\'text_category_footer_accessories\');\r\n				$data[\'text_blog\'] = $this->language->get(\'text_blog\');\r\n				$data[\'text_login\'] = $this->language->get(\'text_login\');\r\n				$data[\'text_search\'] = $this->language->get(\'text_search\');\r\n				$data[\'text_viewcart\'] = $this->language->get(\'text_viewcart\');\r\n				$data[\'text_reward\'] = $this->language->get(\'text_reward\');\r\n				$data[\'text_store_information\'] = $this->language->get(\'text_store_information\');\r\n				$data[\'address\'] = $this->config->get(\'config_address\');\r\n				$data[\'telephone\'] = $this->config->get(\'config_telephone\');\r\n			    $data[\'email\'] = $this->config->get(\'config_email\');\r\n			    $data[\'fax\'] = $this->config->get(\'config_fax\');\r\n			    $data[\'title_address\'] = $this->language->get(\'title_address\');\r\n			    $data[\'title_telephone\'] = $this->language->get(\'title_telephone\');\r\n			    $data[\'title_fax\'] = $this->language->get(\'title_fax\');\r\n			    $data[\'title_email\'] = $this->language->get(\'title_email\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$data[\'newsletter\'] = $this->url->link(\'account/newsletter\', \'\', true);]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'footer1\'] = $this->load->controller(\'common/footer1\');\r\n				$data[\'footer2\'] = $this->load->controller(\'common/footer2\');\r\n				$data[\'newsletter\'] = $this->load->controller(\'common/newsletter\');\r\n				$data[\'login\'] = $this->url->link(\'account/login\', \'\', \'SSL\');\r\n				$data[\'checkout\'] = $this->url->link(\'checkout/checkout\', \'\', \'SSL\');\r\n				$data[\'voucher\'] = $this->url->link(\'account/voucher\', \'\', \'SSL\');\r\n				$data[\'blog\'] = $this->url->link(\'blog/blog\', \'\', \'SSL\');\r\n				$data[\'search\'] = $this->url->link(\'product/search\');\r\n				$data[\'viewcart\'] = $this->url->link(\'checkout/cart\', \'\', \'SSL\');\r\n				$data[\'reward\'] = $this->url->link(\'account/reward\', \'\', \'SSL\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/language/en-gb/checkout/cart.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'text_payment_cancel\']      = \'%s every %d %s(s) until canceled\';]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$_[\'button_checkout_cart\']  = \'Procced To Checkout\';\r\n				$_[\'button_update_cart\']    = \'Update Shopping Cart\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/common/header.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'button_checkout\'] = $this->language->get(\'button_checkout\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'button_checkout_cart\'] = $this->language->get(\'button_checkout_cart\');\r\n				$data[\'button_update_cart\'] = $this->language->get(\'button_update_cart\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/product/product.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'description\'] = html_entity_decode($product_info[\'description\'], ENT_QUOTES, \'UTF-8\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'shortdescription\']  = utf8_substr(strip_tags(html_entity_decode($product_info[\'description\'], ENT_QUOTES, \'UTF-8\')), 0, 400) . \'...\';\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$results = $this->model_catalog_product->getProducts($filter_data);]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$product_rotator_status = (int) $this->config->get(\'ocproductrotator_status\');\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$data[\'products\'][] = array(]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				/* Get new product */\r\n                $filter_data = array(\r\n                        \'sort\'  => \'p.date_added\',\r\n                        \'order\' => \'DESC\',\r\n                        \'start\' => 0,\r\n                        \'limit\' => 10\r\n                );\r\n\r\n                $new_results = $this->model_catalog_product->getProducts($filter_data);\r\n                /* End */\r\n                $is_new = false;\r\n                if ($new_results) {\r\n                    foreach($new_results as $new_r) {\r\n                        if($result[\'product_id\'] == $new_r[\'product_id\']) {\r\n                            $is_new = true;\r\n                        }\r\n                    }\r\n                }\r\n				if($product_rotator_status == 1) {\r\n                    $product_id = $result[\'product_id\'];\r\n                    $product_rotator_image = $this->model_catalog_ocproductrotator->getProductRotatorImage($product_id);\r\n\r\n                    if($product_rotator_image) {\r\n                        $rotator_image = $this->model_tool_image->resize($product_rotator_image, $this->config->get($this->config->get(\'config_theme\') . \'_image_product_width\'), $this->config->get($this->config->get(\'config_theme\') . \'_image_product_height\'));\r\n                    } else {\r\n                        $rotator_image = false;\r\n                    }\r\n                } else {\r\n                    $rotator_image = false;\r\n                }\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[\'product_id\'  => $result[\'product_id\'],]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				\'rotator_image\' => $rotator_image,\r\n				\'is_new\'      => $is_new,\r\n			]]></add>\r\n		</operation>\r\n		<operation>\r\n			<search><![CDATA[$data[\'text_limit\'] = $this->language->get(\'text_limit\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'text_sale\'] = $this->language->get(\'text_sale\');\r\n            	$data[\'text_new\'] = $this->language->get(\'text_new\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/language/en-gb/checkout/cart.php\">\r\n		<operation>\r\n			<search><![CDATA[$_[\'text_payment_cancel\']      = \'%s every %d %s(s) until canceled\';]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$_[\'button_checkout_cart\']  = \'Procced To Checkout\';\r\n				$_[\'button_update_cart\']    = \'Update Shopping Cart\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/controller/checkout/cart.php\">\r\n		<operation>\r\n			<search><![CDATA[$data[\'button_checkout\'] = $this->language->get(\'button_checkout\');]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$data[\'button_checkout_cart\'] = $this->language->get(\'button_checkout_cart\');\r\n				$data[\'button_update_cart\'] = $this->language->get(\'button_update_cart\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/common/header.php\">\r\n		<operation>\r\n			<search><![CDATA[ ]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n</modification>\r\n', 1, '2016-09-30 00:12:00', '2017-11-02 19:19:23'),
(18, 'Modification Manager', 'modification_manager', 'OpenCart-Templates', '2.8.1', 'http://www.opencart-templates.co.uk/modification-manager', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n  <name>Modification Manager</name>\n  <code>modification_manager</code>\n  <version>2.8.1</version>\n  <author>OpenCart-Templates</author>\n  <link>http://www.opencart-templates.co.uk/modification-manager</link>\n\n  <file path=\"admin/language/en-gb/extension/modification.php\">\n	<operation>\n      <search><![CDATA[<?php]]></search> \n      <add position=\"after\"><![CDATA[\n$_[\'tab_error\'] = \'Error\';\n$_[\'tab_files\'] = \'Files\';\n\n$_[\'text_add\'] = \'Add Modification\';\n$_[\'text_edit\'] = \'Edit Modification: %s\';\n\n$_[\'text_enabled\'] = \'Enabled\';\n$_[\'text_disabled\'] = \'Disabled\';\n\n$_[\'entry_author\'] = \'Author\';\n$_[\'entry_name\'] = \'Name\';\n$_[\'entry_xml\'] = \'XML\';\n\n$_[\'button_filter\'] = \'Filter\';\n$_[\'button_reset\'] = \'Reset\';\n\n$_[\'column_date_modified\'] = \'Last Modified\';\n\n$_[\'error_warning\'] = \'There has been an error. Please check your data and try again\';\n$_[\'error_required\'] = \'This field is required\';\n$_[\'error_name\'] = \'Missing name tag\';\n$_[\'error_code\'] = \'Missing code tag\';\n$_[\'error_exists\'] = \'Modification \\\'%s\\\' is already using the same code: %s!\';]]></add>\n    </operation>\n  </file>\n\n  <file path=\"admin/controller/extension/installer.php\">\n	<operation>\n      <search index=\"0\"><![CDATA[$json[\'error\'] = sprintf($this->language->get(\'error_exists\'), $modification_info[\'name\']);]]></search>\n      <add position=\"replace\"><![CDATA[$this->model_extension_modification->deleteModification($modification_info[\'modification_id\']);]]></add>\n    </operation>\n  </file>\n</modification>', 1, '2017-11-02 14:52:41', '2017-11-02 14:52:41'),
(19, 'Brainy Filter', 'brainyfilter', 'Giant Leap Lab', 'Ultimate 5.0.5', '', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>  \r\n   <id>Brainy Filter</id>\r\n   <name>Brainy Filter</name>\r\n   <code>brainyfilter</code>\r\n   <version>Ultimate 5.0.5</version>\r\n   <author>Giant Leap Lab</author>\r\n   \r\n   <file path=\"catalog/controller/product/*.php\">\r\n       <operation>\r\n           <search><![CDATA[$this->model_catalog_product->getProducts($filter_data)]]></search>\r\n           <add position=\"before\"><![CDATA[            $filter_data[\'filter_bfilter\'] = true;]]></add>\r\n       </operation>\r\n       <operation>\r\n           <search><![CDATA[= $this->model_catalog_product->getTotalProducts($filter_data)]]></search>\r\n           <add position=\"before\"><![CDATA[            $filter_data[\'filter_bfilter\'] = true;]]></add>\r\n       </operation>\r\n   </file>\r\n   \r\n   <file path=\"catalog/controller/product/category.php\">\r\n	  <operation>\r\n			<search><![CDATA[$category_info = $this->model_catalog_category->getCategory($category_id);]]></search>\r\n            <add position=\"after\"><![CDATA[\r\n                /* Brainy Filter (brainyfilter.xml) - Start ->*/\r\n                if (!$category_info) {\r\n                    $this->load->language(\'module/brainyfilter\');\r\n                    $category_info = array(\r\n                        \'name\' => $this->language->get(\'text_bf_page_title\'),\r\n                        \'description\' => \'\',\r\n                        \'meta_description\' => \'\',\r\n                        \'meta_keyword\' => \'\',\r\n                        \'meta_title\' => $this->language->get(\'text_bf_page_title\'),\r\n                        \'image\' => \'\',\r\n                    );\r\n                    $this->request->get[\'path\'] = 0;\r\n                    $showCategories = false;\r\n                    $route = \'module/brainyfilter/filter\';\r\n                    $path  = \'\';\r\n                } else {\r\n                    $route = \'product/category\';\r\n                    $path  = \'path=\' . $this->request->get[\'path\'];\r\n                    $showCategories = true;\r\n                }\r\n                /* Brainy Filter (brainyfilter.xml) - End ->*/\r\n                ]]>\r\n			</add>\r\n	  </operation>\r\n	  <operation>\r\n			<search><![CDATA[$results = $this->model_catalog_category->getCategories($category_id);]]></search>\r\n            <add position=\"replace\"><![CDATA[\r\n                /* Brainy Filter (brainyfilter.xml) - Start ->*/\r\n                if ($showCategories) {\r\n                $results = $this->model_catalog_category->getCategories($category_id);\r\n                } else {\r\n                    $results = array();\r\n                }\r\n                /* Brainy Filter (brainyfilter.xml) - End ->*/\r\n            ]]>\r\n			</add>\r\n	  </operation>\r\n	  <operation>\r\n			<search regex=\"true\"><![CDATA[/\\\'product\\/category\\\', \\\'path\\=\\\' \\. \\$this\\-\\>request\\-\\>get\\[\\\'path\\\'\\]/]]></search>\r\n            <add position=\"replace\"><![CDATA[$route, $path]]>\r\n			</add>\r\n	  </operation>\r\n        <operation>\r\n            <search><![CDATA[if ($page == 1) {]]></search>\r\n            <add position=\"before\">\r\n                <![CDATA[\r\n                /* Brainy Filter (brainyfilter.xml) - Start ->*/\r\n                if (isset($category_info[\'category_id\'])) {\r\n                /* Brainy Filter (brainyfilter.xml) - End ->*/]]>\r\n            </add>\r\n        </operation>\r\n        <operation>\r\n            <search><![CDATA[if ($limit && ceil($product_total / $limit) > $page) {]]></search>\r\n            <add position=\"after\" offset=\"2\">\r\n                <![CDATA[\r\n                /* Brainy Filter (brainyfilter.xml) - Start ->*/\r\n                }\r\n                /* Brainy Filter (brainyfilter.xml) - End ->*/]]>\r\n            </add>\r\n        </operation>\r\n   </file>\r\n   \r\n   <file path=\"catalog/controller/product/search.php\">\r\n       <operation>\r\n           <search><![CDATA[if (isset($this->request->get[\'search\']) || isset($this->request->get[\'tag\'])) {]]></search>\r\n           <add position=\"replace\"><![CDATA[\r\n                    /* Brainy Filter (brainyfilter.xml) - Start ->*/\r\n                    /* Show product list in any case */\r\n					if (true) {\r\n                    /* Brainy Filter (brainyfilter.xml) - End ->*/\r\n                ]]>\r\n           </add>\r\n       </operation>\r\n   </file>\r\n   \r\n    <file path=\"catalog/model/catalog/product.php\">\r\n        <operation>\r\n            <search><![CDATA[public function getProducts($data = array()) {]]></search>\r\n            <add>\r\n                <![CDATA[\r\n    /**\r\n     * Custom getProducts() method added by Brainy Filter extension\r\n     */\r\n    public function getProducts($data = array()) \r\n    {\r\n        if (!isset($data[\'filter_bfilter\'])) {\r\n            return $this->getProductsOriginal($data);\r\n        }\r\n        $settings = $this->config->get(\'brainyfilter_layout_basic\');\r\n        if (isset($settings[\'global\'][\'subcategories_fix\']) && $settings[\'global\'][\'subcategories_fix\']) {\r\n            $data[\'filter_sub_category\'] = true;\r\n        }\r\n        $this->load->model(\'module/brainyfilter\');\r\n        $model = new ModelModuleBrainyFilter($this->registry);\r\n        $model->setData($data);\r\n        $sql = $model->prepareQueryForCategory();\r\n        $query = $this->db->query($sql);\r\n\r\n        $product_data = array();\r\n		foreach ($query->rows as $result) {\r\n			$product_data[$result[\'product_id\']] = $this->getProduct($result[\'product_id\']);\r\n		}\r\n\r\n		return $product_data;\r\n    }\r\n    \r\n    /** \r\n     * It is the original getProducts() method, which was renamed by Brainy Filter modification.\r\n     * A custom getProduct() method was added instead. \r\n     * Disable the Brainy Filter OCMOD modification in order to reset the method.\r\n     * Note: disabling of Brainy Filter modification will break the work of Brainy Filter extension.\r\n     */\r\n     public function getProductsOriginal($data = array()) { ]]>\r\n            </add>\r\n        </operation>\r\n        \r\n        <operation>\r\n            <search><![CDATA[public function getTotalProducts($data = array()) {]]></search>\r\n            <add>\r\n                <![CDATA[\r\n    /**\r\n     * Custom getTotalProducts() method added by Brainy Filter extension\r\n     */\r\n    public function getTotalProducts($data = array())\r\n    {\r\n        if (!isset($data[\'filter_bfilter\'])) {\r\n            return $this->getTotalProductsOriginal($data);\r\n        }\r\n        $settings = $this->config->get(\'brainyfilter_layout_basic\');\r\n        if (isset($settings[\'global\'][\'subcategories_fix\']) && $settings[\'global\'][\'subcategories_fix\']) {\r\n            $data[\'filter_sub_category\'] = true;\r\n        }\r\n        $this->load->model(\'module/brainyfilter\');\r\n        $model = new ModelModuleBrainyFilter($this->registry);\r\n        $model->setData($data);\r\n        $sql = $model->prepareQueryForTotal();\r\n		$query = $this->db->query($sql);\r\n\r\n		return $query->row[\'total\'];\r\n    }\r\n    \r\n    /** \r\n     * It is the original getTotalProducts() method, which was renamed by Brainy Filter modification.\r\n     * A custom getTotalProducts() method was added instead. \r\n     * Disable the Brainy Filter OCMOD modification in order to reset the method.\r\n     * Note: disabling of Brainy Filter modification will break the work of Brainy Filter extension.\r\n     */\r\n     public function getTotalProductsOriginal($data = array()) { ]]>\r\n            </add>\r\n        </operation>\r\n    </file>\r\n</modification>', 0, '2017-11-02 15:59:03', '2017-11-02 16:24:38'),
(20, 'One page checkout', 'One_page_checkout', 'Andrey Derevjanko, mod by Eetu Salpaharju', '1.5-Toka_mod', 'http://web-andryshka.ru/', '<modification>\r\n	<name>One page checkout</name>\r\n	<version>1.5-Toka_mod</version>\r\n	<link>http://web-andryshka.ru/</link>\r\n	<author>Andrey Derevjanko, mod by Eetu Salpaharju</author>\r\n	<code>One_page_checkout</code>\r\n\r\n	<file path=\"catalog/controller/common/cart.php\">\r\n		<operation>\r\n			<search index=\"0\"><![CDATA[	$data[\'checkout\'] = $this->url->link(\'checkout/checkout\', \'\', true); ]]></search>\r\n			<add position=\"replace\" ><![CDATA[ $data[\'checkout\'] = $this->url->link(\'checkout/onepagecheckout\', \'\', true);	]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/controller/common/header.php\">\r\n		<operation>\r\n			<search index=\"0\"><![CDATA[	$data[\'checkout\'] = $this->url->link(\'checkout/checkout\', \'\', true); ]]></search>\r\n			<add position=\"replace\" ><![CDATA[ $data[\'checkout\'] = $this->url->link(\'checkout/onepagecheckout\', \'\', true);	]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/controller/checkout/cart.php\">\r\n		<operation>\r\n			<search index=\"0\"><![CDATA[	$data[\'checkout\'] = $this->url->link(\'checkout/checkout\', \'\', true); ]]></search>\r\n			<add position=\"replace\" ><![CDATA[ $data[\'checkout\'] = $this->url->link(\'checkout/onepagecheckout\', \'\', true);	]]></add>\r\n		</operation>\r\n	</file>\r\n</modification>', 1, '2017-11-02 19:18:23', '2017-11-02 19:18:23');
INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `date_modified`) VALUES
(21, 'Новая Почта c калькулятором + отделения', 'novaposhta-2.0', 'opencart2x', '2.0', 'http://opencart2x.ru', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n  <name>Новая Почта c калькулятором + отделения</name>\r\n  <code>novaposhta-2.0</code>\r\n  <version>2.0</version>\r\n  <author>opencart2x</author>\r\n  <link>http://opencart2x.ru</link>\r\n	\r\n	<file path=\"catalog/model/tool/simpleapimain.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n			\'id\'   => $result[\'city_id\'],\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			\'id\'   => $result[\'name\'],\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/controller/common/menu.php\">\r\n	  <operation>\r\n		<search><![CDATA[\r\n		$data[\'text_country\'] = $this->language->get(\'text_country\');\r\n		]]></search>\r\n		<add position=\"after\"><![CDATA[        $data[\'text_city\'] = $this->language->get(\'text_city\');]]></add>\r\n	  </operation>\r\n	  <operation>\r\n		<search><![CDATA[\r\n		$data[\'country\'] = $this->url->link(\'localisation/country\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\r\n		]]></search>\r\n		<add position=\"after\"><![CDATA[		$data[\'city\'] = $this->url->link(\'localisation/city\', \'token=\' . $this->session->data[\'token\'], \'SSL\');]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	<file path=\"admin/view/template/common/menu.tpl\">\r\n	  <operation>\r\n		<search><![CDATA[\r\n		  <li><a href=\"<?php echo $zone; ?>\"><?php echo $text_zone; ?></a></li>\r\n		]]></search>\r\n		<add position=\"after\"><![CDATA[          <li><a href=\"<?php echo $city; ?>\"><?php echo $text_city; ?></a></li>]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	<file path=\"admin/controller/sale/customer.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n	public function country() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/view/template/sale/customer_form.tpl\">\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n						<label class=\"col-sm-2 control-label\" for=\"input-city<?php echo $address_row; ?>\"><?php echo $entry_city; ?></label>\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"9\"><![CDATA[\r\n						<label class=\"col-sm-2 control-label\" for=\"input-postcode<?php echo $address_row; ?>\"><?php echo $entry_postcode; ?></label>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n						  <select name=\"address[<?php echo $address_row; ?>][zone_id]\" id=\"input-zone<?php echo $address_row; ?>\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n						  <select name=\"address[<?php echo $address_row; ?>][zone_id]\" id=\"input-zone<?php echo $address_row; ?>\" class=\"form-control\" onchange=\"zone(this, \'<?php echo $address_row; ?>\', \'<?php echo $address[\'city\']; ?>\');\">\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				<div class=\"text-danger\"><?php echo $error_address[$address_row][\'zone\']; ?></div>\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n					  <div class=\"form-group required\">\r\n						<label class=\"col-sm-2 control-label\" for=\"input-city<?php echo $address_row; ?>\"><?php echo $entry_city; ?></label>\r\n						<div class=\"col-sm-10\">\r\n						  <select name=\"address[<?php echo $address_row; ?>][city]\" id=\"input-city<?php echo $address_row; ?>\" class=\"form-control\"></select>\r\n						  <?php if (isset($error_address_city[$address_row])) { ?>\r\n						  <span class=\"error\"><?php echo $error_address_city[$address_row]; ?></span>\r\n						  <?php } ?>\r\n						</div>\r\n					  </div>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				$(\'select[name=\\\'address[\' + index + \'][zone_id]\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$(\'select[name=\\\'address[\' + index + \'][city]\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n				$(\'select[name=\\\'address[\' + index + \'][zone_id]\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n$(\'select[name$=\\\'[country_id]\\\']\').trigger(\'change\');\r\n			]]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n<script type=\"text/javascript\"><!--\r\nfunction zone(element, index, city) {\r\n  if (element.value != \'\') {\r\n		$.ajax({\r\n			url: \'index.php?route=sale/customer/zone&token=<?php echo $token; ?>&zone_id=\' + element.value,\r\n			dataType: \'json\',\r\n			beforeSend: function() {\r\n				$(\'select[name=\\\'address[\' + index + \'][zone_id]\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n			},\r\n			complete: function() {\r\n				$(\'.fa-spin\').remove();\r\n			},\r\n			success: function(json) {\r\n				html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n				if (json[\'city\'] != \'\') {\r\n					for (i = 0; i < json[\'city\'].length; i++) {\r\n						html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n						if (json[\'city\'][i][\'name\'] == city) {\r\n							html += \' selected=\"selected\"\';\r\n						}\r\n\r\n						html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n					}\r\n				} else {\r\n					html += \'<option value=\"0\"><?php echo $text_none; ?></option>\';\r\n				}\r\n\r\n				$(\'select[name=\\\'address[\' + index + \'][city]\\\']\').html(html);\r\n			},\r\n			error: function(xhr, ajaxOptions, thrownError) {\r\n				alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n			}\r\n		});\r\n	}\r\n}\r\n\r\n$(\'select[name$=\\\'[zone_id]\\\']\').trigger(\'change\');\r\n//--></script>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	html += \'    <label class=\"col-sm-2 control-label\" for=\"input-city\' + address_row + \'\"><?php echo $entry_city; ?></label>\';\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"5\"><![CDATA[\r\n	html += \'    <label class=\"col-sm-2 control-label\" for=\"input-postcode\' + address_row + \'\"><?php echo $entry_postcode; ?></label>\';\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	html += \'    <div class=\"col-sm-10\"><select name=\"address[\' + address_row + \'][zone_id]\" id=\"input-zone\' + address_row + \'\" class=\"form-control\"><option value=\"\"><?php echo $text_none; ?></option></select></div>\';\r\n			]]></search>\r\n			<add position=\"after\" offset=\"2\"><![CDATA[\r\n	html += \'  <div class=\"form-group required\">\';\r\n	html += \'    <label class=\"col-sm-2 control-label\" for=\"input-city\' + address_row + \'\"><?php echo $entry_city; ?></label>\';\r\n	html += \'    <div class=\"col-sm-10\"><select name=\"address[\' + address_row + \'][city]\" id=\"input-city\' + address_row + \'\" class=\"form-control\"><option value=\"\"><?php echo $text_none; ?></option></select></div>\';\r\n	html += \'  </div>\';\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	html += \'    <div class=\"col-sm-10\"><select name=\"address[\' + address_row + \'][zone_id]\" id=\"input-zone\' + address_row + \'\" class=\"form-control\"><option value=\"\"><?php echo $text_none; ?></option></select></div>\';\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n	html += \'    <div class=\"col-sm-10\"><select name=\"address[\' + address_row + \'][zone_id]\" id=\"input-zone\' + address_row + \'\" class=\"form-control\" onchange=\"zone(this, \\\'\' + address_row + \'\\\', \\\'0\\\');\"><option value=\"\"><?php echo $text_none; ?></option></select></div>\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"admin/controller/marketing/affiliate.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n	public function transaction() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/view/template/marketing/affiliate_form.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				  <input type=\"text\" name=\"address_2\" value=\"<?php echo $address_2; ?>\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-address-2\" class=\"form-control\" />\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"9\"><![CDATA[\r\n				  <input type=\"text\" name=\"address_2\" value=\"<?php echo $address_2; ?>\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-address-2\" class=\"form-control\" />\r\n			</tr>\r\n			  </tr>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				  <div class=\"text-danger\"><?php echo $error_zone; ?></div>\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n			  <div class=\"form-group required\">\r\n				<label class=\"col-sm-2 control-label\" for=\"input-city\"><?php echo $entry_city; ?></label>\r\n				<div class=\"col-sm-10\">\r\n				  <select name=\"city\" id=\"input-city\" class=\"form-control\">\r\n				  </select>\r\n				  <?php if ($error_city) { ?>\r\n				  <div class=\"text-danger\"><?php echo $error_city; ?></div>\r\n				  <?php } ?>\r\n				</div>\r\n			  </div>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			$(\'select[name=\\\'zone_id\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			$(\'select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n			$(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n$(\'select[name=\\\'country_id\\\']\').trigger(\'change\');\r\n			]]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n<script type=\"text/javascript\"><!--\r\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	$.ajax({\r\n		url: \'index.php?route=sale/affiliate/zone&token=<?php echo $token; ?>&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n		   $(\'.fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json != \'\' && json[\'city\'] != \'\') {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == \'<?php echo $city; ?>\') {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			} else {\r\n				html += \'<option value=\"0\" selected=\"selected\"><?php echo $text_none; ?></option>\';\r\n			}\r\n\r\n			$(\'select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n//--></script>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"admin/controller/sale/order.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n	public function history() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"admin/view/template/sale/order_form.tpl\">\r\n		<operation>\r\n			<search>\r\n				<![CDATA[<label class=\"col-sm-2 control-label\" for=\"input-payment-city\"><?php echo $entry_city; ?></label>]]>\r\n			</search>\r\n			<add position=\"replace\" offset=\"5\"><![CDATA[]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search>\r\n			<![CDATA[<label class=\"col-sm-2 control-label\" for=\"input-shipping-city\"><?php echo $entry_city; ?></label>]]>\r\n			</search>\r\n			<add position=\"replace\" offset=\"5\"><![CDATA[]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				  <select name=\"zone_id\" id=\"input-payment-zone\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n			  <div class=\"form-group required\">\r\n				<label class=\"col-sm-2 control-label\" for=\"input-payment-city\"><?php echo $entry_city; ?></label>\r\n				<div class=\"col-sm-10\">\r\n				  <select name=\"city\" id=\"input-payment-city\" class=\"form-control\">\r\n				  </select>\r\n				</div>\r\n			  </div>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				  <select name=\"zone_id\" id=\"input-shipping-zone\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n			  <div class=\"form-group required\">\r\n				<label class=\"col-sm-2 control-label\" for=\"input-shipping-city\"><?php echo $entry_city; ?></label>\r\n				<div class=\"col-sm-10\">\r\n				  <select name=\"city\" id=\"input-shipping-city\" class=\"form-control\">\r\n				  </select>\r\n				</div>\r\n			  </div>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			$(\'#tab-payment select[name=\\\'zone_id\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			$(\'#tab-payment select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n			$(\'#tab-payment select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\nvar payment_zone_id = \'<?php echo $payment_zone_id; ?>\';\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\nvar payment_city = \'<?php echo $payment_city; ?>\';\r\n\r\n$(\'#tab-payment select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	$.ajax({\r\n		url: \'index.php?route=sale/order/zone&token=<?php echo $token; ?>&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'#tab-payment select[name=\\\'zone_id\\\']\').after(\'<i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n			$(\'#tab-payment .fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json[\'city\']) {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == payment_city) {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			} else {\r\n				html += \'<option value=\"0\" selected=\"selected\"><?php echo $text_none; ?></option>\';\r\n			}\r\n\r\n			$(\'#tab-payment select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'#tab-payment select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				$(\'#tab-payment input[name=\\\'city\\\']\').val(json[\'city\']);\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			payment_zone_id = json[\'zone_id\'];\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			payment_city = json[\'city\'];\r\n			$(\'#tab-payment select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			$(\'#tab-shipping select[name=\\\'zone_id\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n			$(\'#tab-shipping select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n			$(\'#tab-shipping select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\nvar shipping_zone_id = \'<?php echo $shipping_zone_id; ?>\';\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\nvar shipping_city = \'<?php echo $shipping_city; ?>\';\r\n\r\n$(\'#tab-shipping select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	$.ajax({\r\n		url: \'index.php?route=sale/order/zone&token=<?php echo $token; ?>&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'select[name=\\\'shipping_address\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n			$(\'#tab-shipping .fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json != \'\' && json[\'city\'] != \'\') {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == shipping_city) {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			} else {\r\n				html += \'<option value=\"0\" selected=\"selected\"><?php echo $text_none; ?></option>\';\r\n			}\r\n\r\n			$(\'#tab-shipping select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'#tab-shipping select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				$(\'#tab-shipping input[name=\\\'city\\\']\').val(json[\'city\']);\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			shipping_zone_id = json[\'zone_id\'];\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				shipping_city = json[\'city\'];\r\n				$(\'#tab-shipping select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/account/address.php,catalog/controller/account/register.php,catalog/controller/affiliate/edit.php,catalog/controller/affiliate/register.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		if (isset($this->request->post[\'city\'])) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n		$this->load->model(\'localisation/city\');\r\n		$data[\'cities\'] = $this->model_localisation_city->getCities();\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/account/address.php\">\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	protected function validateDelete() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/account/register.php\">\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	public function customfield() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/affiliate/edit.php,catalog/controller/affiliate/register.php\">\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	public function country() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/account/address_form.tpl,catalog/view/theme/*/template/account/register.tpl,catalog/view/theme/*/template/affiliate/edit.tpl,catalog/view/theme/*/template/affiliate/register.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n			  <label class=\"col-sm-2 control-label\" for=\"input-city\"><?php echo $entry_city; ?></label>\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"9\"><![CDATA[\r\n			  <label class=\"col-sm-2 control-label\" for=\"input-postcode\"><?php echo $entry_postcode; ?></label>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			  <div class=\"text-danger\"><?php echo $error_zone; ?></div>\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n		  <div class=\"form-group required\">\r\n			<label class=\"col-sm-2 control-label\" for=\"input-city\"><?php echo $entry_city; ?></label>\r\n			<div class=\"col-sm-10\">\r\n			  <select name=\"city\" id=\"input-city\" class=\"form-control\">\r\n			  </select>\r\n			  <?php if ($error_city) { ?>\r\n			  <div class=\"text-danger\"><?php echo $error_city; ?></div>\r\n			  <?php } ?>\r\n			</div>\r\n		  </div>\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n				$(\'select[name=\\\'zone_id\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$(\'select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n				$(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n<?php echo $footer; ?>\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n<script type=\"text/javascript\"><!--\r\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	if (this.value == \'\') return;\r\n	$.ajax({\r\n		url: \'index.php?route=account/register/zone&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n			$(\'.fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			$(\'.fa-spin\').remove();\r\n\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json[\'city\']) {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == \'<?php echo $city; ?>\') {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			} else {\r\n				html += \'<option value=\"0\" selected=\"selected\"><?php echo $text_none; ?></option>\';\r\n			}\r\n\r\n			$(\'select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n//--></script>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"catalog/controller/checkout/shipping.php\">\r\n	  <operation>\r\n		<search><![CDATA[\r\n			$data[\'entry_postcode\'] = $this->language->get(\'entry_postcode\');\r\n		]]></search>\r\n		<add position=\"replace\"><![CDATA[\r\n			$data[\'entry_city\'] = $this->language->get(\'entry_city\');]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n			if (isset($this->session->data[\'shipping_address\'][\'postcode\'])) {\r\n		]]></search>\r\n		<add position=\"replace\" offset=\"5\"><![CDATA[\r\n			if (isset($this->session->data[\'shipping_address\'][\'city\'])) {\r\n				$data[\'city\'] = $this->session->data[\'shipping_address\'][\'city\'];\r\n			} else {\r\n				$data[\'city\'] = \'\';\r\n			}]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n		if ($country_info && $country_info[\'postcode_required\'] && (utf8_strlen(trim($this->request->post[\'postcode\'])) < 2 || utf8_strlen(trim($this->request->post[\'postcode\'])) > 10)) {\r\n		]]></search>\r\n		<add position=\"replace\" offset=\"2\"><![CDATA[\r\n		if (!isset($this->request->post[\'city\']) || $this->request->post[\'city\'] == \'\') {\r\n			$json[\'error\'][\'city\'] = $this->language->get(\'error_city\');\r\n		}]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n				\'postcode\'       => $this->request->post[\'postcode\'],\r\n		]]></search>\r\n		<add position=\"replace\" offset=\"1\"><![CDATA[\'postcode\'       => \'\',\r\n				\'city\'           => $this->request->post[\'city\'],]]></add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n	public function country() {\r\n		]]></search>\r\n		<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n	  ]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/shipping.tpl\">\r\n	  <operation>\r\n		<search><![CDATA[\r\n		  <label class=\"col-sm-2 control-label\" for=\"input-postcode\"><?php echo $entry_postcode; ?></label>\r\n		]]></search>\r\n		<add position=\"replace\"><![CDATA[\r\n		  <label class=\"col-sm-2 control-label\" for=\"input-city\"><?php echo $entry_city; ?></label>]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n			<input type=\"text\" name=\"postcode\" value=\"<?php echo $postcode; ?>\" placeholder=\"<?php echo $entry_postcode; ?>\" id=\"input-postcode\" class=\"form-control\" />\r\n		]]></search>\r\n		<add position=\"replace\"><![CDATA[\r\n			<select name=\"city\" id=\"input-city\" class=\"form-control\">\r\n			</select>]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n		data: \'country_id=\' + $(\'select[name=\\\'country_id\\\']\').val() + \'&zone_id=\' + $(\'select[name=\\\'zone_id\\\']\').val() + \'&postcode=\' + encodeURIComponent($(\'input[name=\\\'postcode\\\']\').val()),\r\n		]]></search>\r\n		<add position=\"replace\"><![CDATA[		data: \'country_id=\' + $(\'select[name=\\\'country_id\\\']\').val() + \'&zone_id=\' + $(\'select[name=\\\'zone_id\\\']\').val() + \'&city=\' + $(\'select[name=\\\'city\\\']\').val(),]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n			if (json[\'postcode_required\'] == \'1\') {\r\n		]]></search>\r\n		<add position=\"replace\" offset=\"5\"><![CDATA[]]></add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n				if (json[\'error\'][\'postcode\']) {\r\n		]]></search>\r\n		<add position=\"replace\" offset=\"2\"><![CDATA[if (json[\'error\'][\'city\']) {\r\n					$(\'select[name=\\\'city\\\']\').after(\'<div class=\"text-danger\">\' + json[\'error\'][\'city\'] + \'</div>\');\r\n				}]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n				$(\'input[name=\\\'shipping_method\\\']\').on(\'change\', function() {\r\n		]]></search>\r\n		<add position=\"replace\" offset=\"2\"><![CDATA[$(\'input[name=\\\'shipping_method\\\']\').iCheck({ checkboxClass: \'icheckbox_flat\', radioClass: \'iradio_flat\', cursor: true });\r\n				$(\'div.radio label\').click(function() {\r\n					$(\'#button-shipping\').prop(\'disabled\', false);\r\n				});]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n		<search><![CDATA[\r\n			$(\'select[name=\\\'zone_id\\\']\').html(html);\r\n		]]></search>\r\n		<add position=\"after\"><![CDATA[\r\n			$(\'select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n			$(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');]]>\r\n		</add>\r\n	  </operation>\r\n\r\n	  <operation>\r\n			<search><![CDATA[\r\n$(\'select[name=\\\'country_id\\\']\').trigger(\'change\');\r\n			]]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n<script type=\"text/javascript\"><!--\r\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	if (this.value == \'\') return;\r\n	$.ajax({\r\n		url: \'index.php?route=checkout/shipping/zone&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n			$(\'.fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			$(\'.fa-spin\').remove();\r\n\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json[\'city\'] != \'\') {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == \'<?php echo $city; ?>\') {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			}\r\n\r\n			$(\'select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n//--></script>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	\r\n	<file path=\"catalog/controller/checkout/guest.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		if (isset($this->session->data[\'payment_address\'][\'city\'])) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n		$this->load->model(\'localisation/city\');\r\n		$data[\'cities\'] = $this->model_localisation_city->getCities();\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/checkout/guest_shipping.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		if (isset($this->session->data[\'shipping_address\'][\'city\'])) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n		$this->load->model(\'localisation/city\');\r\n		$data[\'cities\'] = $this->model_localisation_city->getCities();\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/checkout/payment_address.php,catalog/controller/checkout/register.php,catalog/controller/checkout/shipping_address.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		$this->load->model(\'localisation/country\');\r\n			]]></search>\r\n			<add position=\"before\" index=\"1\"><![CDATA[\r\n		$this->load->model(\'localisation/city\');\r\n\r\n		$data[\'cities\'] = $this->model_localisation_city->getCities();\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/checkout/payment_address.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		if (isset($this->session->data[\'payment_address\'][\'zone_id\'])) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n		if (isset($this->session->data[\'payment_address\'][\'city\'])) {\r\n			$data[\'city\'] = $this->session->data[\'payment_address\'][\'city\'];\r\n		} else {\r\n			$data[\'city\'] = \'\';\r\n		}\r\n		]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			if ((utf8_strlen($this->request->post[\'city\']) < 2) || (utf8_strlen($this->request->post[\'city\']) > 32)) {\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			if ($this->request->post[\'city\'] == \'\') {\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n		$data[\'cities\'] = $this->model_localisation_city->getCities();\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n		if (isset($this->session->data[\'payment_address\'][\'city\'])) {\r\n			$data[\'city\'] = $this->session->data[\'payment_address\'][\'city\'];\r\n		} else {\r\n			$data[\'city\'] = \'\';\r\n		}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/checkout/register.php,catalog/controller/checkout/shipping_address.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		if (isset($this->session->data[\'shipping_address\'][\'zone_id\'])) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n		if (isset($this->session->data[\'shipping_address\'][\'city\'])) {\r\n			$data[\'city\'] = $this->session->data[\'shipping_address\'][\'city\'];\r\n		} else {\r\n			$data[\'city\'] = \'\';\r\n		}\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n			if ((utf8_strlen(trim($this->request->post[\'city\'])) < 2) || (utf8_strlen(trim($this->request->post[\'city\'])) > 128)) {\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n			if ($this->request->post[\'city\'] == \'\') {\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/checkout/checkout.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n			public function customfield() {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n	public function zone() {\r\n		$json = array();\r\n\r\n		$this->load->model(\'localisation/zone\');\r\n\r\n		$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\r\n\r\n		if ($zone_info) {\r\n			$this->load->model(\'localisation/city\');\r\n\r\n			$json = array(\r\n				\'zone_id\'   => $zone_info[\'zone_id\'],\r\n				\'name\'      => $zone_info[\'name\'],\r\n				\'city\'      => $this->model_localisation_city->getCitiesByZoneId($this->request->get[\'zone_id\']),\r\n				\'status\'    => $zone_info[\'status\']\r\n			);\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/guest.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		<input type=\"text\" name=\"address_2\" value=\"<?php echo $address_2; ?>\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-payment-address-2\" class=\"form-control\" />\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"4\"><![CDATA[\r\n		<input type=\"text\" name=\"address_2\" value=\"<?php echo $address_2; ?>\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-payment-address-2\" class=\"form-control\" />\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n		<select name=\"zone_id\" id=\"input-payment-zone\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"after\" offset=\"2\"><![CDATA[\r\n	  <div class=\"form-group required\">\r\n		<label class=\"control-label\" for=\"input-payment-city\"><?php echo $entry_city; ?></label>\r\n		<select name=\"city\" id=\"input-payment-city\" class=\"form-control\">\r\n		</select>\r\n	  </div>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/guest.tpl,catalog/view/theme/*/template/checkout/payment_address.tpl,catalog/view/theme/*/template/checkout/register.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$(\'#collapse-payment-address select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n				$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n$(\'#collapse-payment-address select[name=\\\'country_id\\\']\').trigger(\'change\');\r\n			]]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n<script type=\"text/javascript\"><!--\r\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	if (this.value == \'\') return;\r\n	$.ajax({\r\n		url: \'index.php?route=checkout/checkout/zone&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n			$(\'.fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			$(\'.fa-spin\').remove();\r\n\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json[\'city\'] != \'\') {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == \'<?php echo $city; ?>\') {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			} else {\r\n				html += \'<option value=\"0\" selected=\"selected\"><?php echo $text_none; ?></option>\';\r\n			}\r\n\r\n			$(\'#collapse-payment-address select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n//--></script>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/guest_shipping.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n	  <input type=\"text\" name=\"address_2\" value=\"<?php echo $address_2; ?>\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-shipping-address-2\" class=\"form-control\" />\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"6\"><![CDATA[\r\n	  <input type=\"text\" name=\"address_2\" value=\"<?php echo $address_2; ?>\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-shipping-address-2\" class=\"form-control\" />\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n	  <select name=\"zone_id\" id=\"input-shipping-zone\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n  <div class=\"form-group required\">\r\n	<label class=\"col-sm-2 control-label\" for=\"input-shipping-city\"><?php echo $entry_city; ?></label>\r\n	<div class=\"col-sm-10\">\r\n	  <select name=\"city\" id=\"input-shipping-city\" class=\"form-control\">\r\n	  </select>\r\n	</div>\r\n  </div>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/guest_shipping.tpl,catalog/view/theme/*/template/checkout/shipping_address.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').html(html);\r\n			]]></search>\r\n			<add position=\"after\"><![CDATA[\r\n				$(\'#collapse-shipping-address select[name=\\\'city\\\']\').html(\'<option value=\"\"><?php echo $text_select; ?></option>\');\r\n				$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n$(\'#collapse-shipping-address select[name=\\\'country_id\\\']\').trigger(\'change\');\r\n			]]></search>\r\n			<add position=\"after\" offset=\"1\"><![CDATA[\r\n<script type=\"text/javascript\"><!--\r\n$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\r\n	if (this.value == \'\') return;\r\n	$.ajax({\r\n		url: \'index.php?route=checkout/checkout/zone&zone_id=\' + this.value,\r\n		dataType: \'json\',\r\n		beforeSend: function() {\r\n			$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').after(\' <i class=\"fa fa-circle-o-notch fa-spin\"></i>\');\r\n		},\r\n		complete: function() {\r\n			$(\'.fa-spin\').remove();\r\n		},\r\n		success: function(json) {\r\n			$(\'.fa-spin\').remove();\r\n\r\n			html = \'<option value=\"\"><?php echo $text_select; ?></option>\';\r\n\r\n			if (json[\'city\'] != \'\') {\r\n				for (i = 0; i < json[\'city\'].length; i++) {\r\n					html += \'<option value=\"\' + json[\'city\'][i][\'name\'] + \'\"\';\r\n\r\n					if (json[\'city\'][i][\'name\'] == \'<?php echo $city; ?>\') {\r\n						html += \' selected=\"selected\"\';\r\n					}\r\n\r\n					html += \'>\' + json[\'city\'][i][\'name\'] + \'</option>\';\r\n				}\r\n			} else {\r\n				html += \'<option value=\"0\" selected=\"selected\"><?php echo $text_none; ?></option>\';\r\n			}\r\n\r\n			$(\'#collapse-shipping-address select[name=\\\'city\\\']\').html(html);\r\n		},\r\n		error: function(xhr, ajaxOptions, thrownError) {\r\n			alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);\r\n		}\r\n	});\r\n});\r\n\r\n$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').trigger(\'change\');\r\n//--></script>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/payment_address.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		<input type=\"text\" name=\"address_2\" value=\"\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-payment-address-2\" class=\"form-control\" />\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"6\"><![CDATA[\r\n		<input type=\"text\" name=\"address_2\" value=\"\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-payment-address-2\" class=\"form-control\" />\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n		<select name=\"zone_id\" id=\"input-payment-zone\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"after\" offset=\"3\"><![CDATA[\r\n	<div class=\"form-group required\">\r\n	  <label class=\"col-sm-2 control-label\" for=\"input-payment-city\"><?php echo $entry_city; ?></label>\r\n	  <div class=\"col-sm-10\">\r\n		<select name=\"city\" id=\"input-payment-city\" class=\"form-control\">\r\n		</select>\r\n	  </div>\r\n	</div>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/view/theme/*/template/checkout/register.tpl\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		<input type=\"text\" name=\"address_2\" value=\"\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-payment-address-2\" class=\"form-control\" />\r\n			]]></search>\r\n			<add position=\"replace\" offset=\"4\"><![CDATA[\r\n		<input type=\"text\" name=\"address_2\" value=\"\" placeholder=\"<?php echo $entry_address_2; ?>\" id=\"input-payment-address-2\" class=\"form-control\" />\r\n			]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[\r\n		<select name=\"zone_id\" id=\"input-payment-zone\" class=\"form-control\">\r\n			]]></search>\r\n			<add position=\"after\" offset=\"2\"><![CDATA[\r\n	  <div class=\"form-group required\">\r\n		<label class=\"control-label\" for=\"input-payment-city\"><?php echo $entry_city; ?></label>\r\n		<select name=\"city\" id=\"input-payment-city\" class=\"form-control\">\r\n		</select>\r\n	  </div>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/controller/api/payment.php,catalog/controller/api/shipping.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n		if ((utf8_strlen($this->request->post[\'city\']) < 2) || (utf8_strlen($this->request->post[\'city\']) > 32)) {\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n		if ((utf8_strlen($this->request->post[\'city\']) < 2) || (utf8_strlen($this->request->post[\'city\']) > 128)) {\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/language/russian/checkout/shipping.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_postcode\']       = \'Почтовый индекс\';]]></search>\r\n		<add position=\"after\"><![CDATA[$_[\'entry_city\']       = \'Отделение Новой почты\';]]></add>\r\n	  </operation>\r\n	\r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_postcode\']       = \'Почтовый индекс должен быть от 2 до 10 символов!\';]]></search>\r\n		<add position=\"after\"><![CDATA[$_[\'error_city\']       = \'Пожалуйста, выберите отделение!\';]]></add>\r\n	  </operation>\r\n	\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']           = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']           = \'Город\';]]></add>\r\n	  </operation>\r\n	\r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']           = \'Пожалуйста, выберите регион / область!\';]]></search>\r\n		<add position=\"after\"><![CDATA[$_[\'error_zone\']           = \'Пожалуйста, выберите город!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/language/russian/account/address.php,catalog/language/russian/account/register.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_city\']           = \'Город\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_city\']           = \'Отделение Новой почты\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_city\']           = \'Город должен быть от 2 до 128 символов!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_city\']           = \'Отделение должно быть от 2 до 128 символов!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']           = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']           = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']           = \'Пожалуйста, выберите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']           = \'Пожалуйста, выберите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']        = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']        = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']        = \'Пожалуйста, выберите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']        = \'Пожалуйста, выберите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/language/russian/affiliate/edit.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_city\']        = \'Город\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_city\']           = \'Отделение Новой почты\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_city\']        = \'Город должен быть от 2 до 128 символов!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_city\']           = \'Отделение должно быть от 2 до 128 символов!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']        = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']           = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']        = \'Пожалуйста, выберите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']           = \'Пожалуйста, выберите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']     = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']     = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']     = \'Пожалуйста, выберите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']     = \'Пожалуйста, выберите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/language/russian/affiliate/register.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_city\']                = \'Город\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_city\']           = \'Отделение Новой почты\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_city\']                = \'Название города должно содержать от 2 до 128 символов!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_city\']           = \'Отделение должно быть от 2 до 128 символов!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']                = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']                = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']                = \'Пожалуйста, выберите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']                = \'Пожалуйста, выберите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']             = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']             = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']             = \'Пожалуйста, выберите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']             = \'Пожалуйста, выберите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	<file path=\"catalog/language/russian/checkout/checkout.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_city\']                     = \'Город\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_city\']           = \'Отделение Новой почты\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_city\']                     = \'Город 1 должен быть от 2 до 128 символов!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_city\']           = \'Отделение должно быть от 2 до 128 символов!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']                     = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']                     = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']                     = \'Пожалуйста, укажите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']                     = \'Пожалуйста, укажите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']                  = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']                  = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']                  = \'Пожалуйста, укажите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']                  = \'Пожалуйста, укажите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	<file path=\"admin/language/russian/customer/customer.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_city\']            = \'Город\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_city\']            = \'Отделение Новой почты\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_city\']            = \'Город должен быть 2 до 128 символов!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_city\']            = \'Отделение должно быть 2 до 128 символов!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']            = \'Регион / Область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']            = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']            = \'Укажите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']            = \'Укажите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']         = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']         = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']         = \'Укажите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']         = \'Укажите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	<file path=\"admin/language/russian/localisation/country.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'heading_title\']           = \'Страны\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'heading_title\']           = \'Области\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_success\']            = \'Страны были изменены!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_success\']            = \'Области были изменены!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_list\']               = \'Список стран\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_list\']               = \'Список областей\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_add\']                = \'Добавить страну\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_add\']                = \'Добавить область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_edit\']               = \'Изменить страну\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_edit\']               = \'Изменить область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_permission\']        = \'У вас нет прав на изменение страны!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_permission\']        = \'У вас нет прав на изменение области!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	\r\n	<file path=\"admin/language/russian/localisation/geo_zone.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']      = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']      = \'Область\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	\r\n	<file path=\"admin/language/russian/localisation/zone.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'column_country\']         = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'column_country\']         = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']          = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']          = \'Область\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n\r\n	<file path=\"admin/language/russian/marketing/affiliate.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_city\']                = \'Город\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_city\']                = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_city\']                = \'Город должен содержать от 2-х до 128-и знаков!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_city\']                = \'Отделение должно содержать от 2-х до 128-и знаков!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']                = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']                = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']                = \'Пожалуйста, выберите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']                = \'Пожалуйста, выберите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']             = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']             = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']             = \'Пожалуйста, выберите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']             = \'Пожалуйста, выберите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	\r\n	<file path=\"admin/language/russian/sale/customer.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']            = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']            = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_zone\']            = \'Пожалуйста, выберите регион / область!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_zone\']            = \'Пожалуйста, выберите город!\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']         = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']         = \'Область\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'error_country\']         = \'Пожалуйста, выберите страну!\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'error_country\']         = \'Пожалуйста, выберите область!\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	\r\n	<file path=\"admin/language/russian/setting/setting.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']                       = \'Регион / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']                       = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']                    = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']                    = \'Область\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	\r\n	<file path=\"admin/language/russian/setting/store.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_zone\']                       = \'Район / область\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_zone\']                       = \'Город\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']                    = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']                    = \'Область\';]]></add>\r\n	  </operation>\r\n	</file>\r\n	\r\n	\r\n	<file path=\"admin/language/russian/sale/order.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_city\']                               = \'Город:\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_city\']                               = \'Отделение Новой почты:\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_zone\']                               = \'Регион / область:\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_zone\']                               = \'Город:\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_country\']                            = \'Страна:\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_country\']                            = \'Область:\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_country_match\']                      = \'Совпадение страны:\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_country_match\']                      = \'Совпадение области:\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_country_code\']                       = \'Код страны:\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_country_code\']                       = \'Код области:\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'entry_country\']                           = \'Страна\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'entry_country\']                           = \'Область\';]]></add>\r\n	  </operation>\r\n	</file>\r\n\r\n	\r\n	\r\n	<file path=\"admin/language/russian/common/menu.php\">\r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_country\']                     = \'Страны\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_country\']                     = \'Области\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_zone\']                        = \'Зоны\';]]></search>\r\n		<add position=\"replace\"><![CDATA[$_[\'text_zone\']                        = \'Города\';]]></add>\r\n	  </operation>\r\n	  \r\n	  <operation>\r\n		<search><![CDATA[$_[\'text_coupon\']                      = \'Купоны\';]]></search>\r\n		<add position=\"before\"><![CDATA[$_[\'text_city\']                        = \'Отделения новой почты\';]]></add>\r\n	  </operation>\r\n	</file>\r\n</modification>', 1, '2017-11-02 19:55:02', '2017-11-02 19:55:02');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{\"name\":\"Category\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(29, 'Home Page', 'carousel', '{\"name\":\"Home Page\",\"banner_id\":\"8\",\"width\":\"130\",\"height\":\"100\",\"status\":\"1\"}'),
(28, 'Home Page', 'featured', '{\"name\":\"Home Page\",\"product\":[\"43\",\"40\",\"42\",\"30\"],\"limit\":\"4\",\"width\":\"200\",\"height\":\"200\",\"status\":\"1\"}'),
(27, 'Home Page', 'slideshow', '{\"name\":\"Home Page\",\"banner_id\":\"7\",\"width\":\"1140\",\"height\":\"380\",\"status\":\"1\"}'),
(31, 'Banner 1', 'banner', '{\"name\":\"Banner 1\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(32, 'OC Slide show 1', 'ocslideshow', '{\"name\":\"OC Slide show 1\",\"status\":\"1\",\"banner\":\"18\",\"width\":\"1920\",\"height\":\"555\"}'),
(33, 'OC Horizontal Mega Menu 1', 'ochozmegamenu', '{\"name\":\"OC Horizontal Mega Menu 1\",\"hactive\":\"LINK27,LINK28\",\"status\":\"1\",\"hhome\":\"1\",\"hdepth\":\"4\",\"hlevel\":\"4\"}'),
(34, 'OC Vertical Megamenu 1', 'ocvermegamenu', '{\"name\":\"OC Vertical Megamenu 1\",\"h_active_cate\":\"CAT20,CAT18,CAT33,CAT57,CAT17,CAT25,CAT34\",\"status\":\"1\",\"h_show_homepage\":\"0\",\"h_item_number\":\"6\",\"h_menu_depth\":\"2\",\"h_menu_level\":\"4\"}'),
(35, 'Block content top 1', 'occmsblock', '{\"name\":\"Block content top 1\",\"status\":\"1\",\"cmsblock\":\"19\"}'),
(36, 'OC Product Tabs Slider 1', 'octabproductslider', '{\"name\":\"OC Product Tabs Slider 1\",\"title\":{\"ru-ru\":{\"title\":\"\"}},\"status\":\"1\",\"types\":[\"bestseller\",\"mostviewed\",\"random\",\"special\",\"latest\"],\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"7\",\"item\":\"5\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"1\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"1\",\"shownextback\":\"1\",\"shownav\":\"0\"}'),
(39, 'OC New Products Slider 1', 'ocnewproductslider', '{\"name\":\"OC New Products Slider 1\",\"title\":{\"en-gb\":{\"title\":\"NEW PRODUCTS\"}},\"status\":\"1\",\"rotator\":\"0\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"8\",\"item\":\"1\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"3\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"0\",\"shownextback\":\"0\",\"shownav\":\"0\"}'),
(40, 'Oc Newsletter 1', 'newslettersubscribe', '{\"name\":\"Oc Newsletter 1\",\"popup\":\"0\",\"status\":\"1\",\"newslettersubscribe_unsubscribe\":\"1\",\"newslettersubscribe_mail_status\":\"1\",\"newslettersubscribe_thickbox\":\"1\",\"newslettersubscribe_registered\":\"1\",\"store_id\":\"0\",\"to\":\"sendall\",\"customer_group_id\":\"1\",\"customers\":\"\",\"affiliates\":\"\",\"products\":\"\",\"subject\":\"\",\"message\":\"conten email here.....\"}'),
(71, 'OC Category Tabs Slider Drill 2', 'octabcategoryslider', '{\"name\":\"OC Category Tabs Slider Drill 2\",\"title\":{\"en-gb\":{\"title\":\"Drill machine\"}},\"status\":\"1\",\"category\":\"\",\"product_category\":[\"20\"],\"thumbnail\":\"1\",\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"6\",\"item\":\"5\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"1\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"1\",\"shownextback\":\"1\",\"shownav\":\"0\",\"fimage\":\"catalog\\/demo\\/image-block\\/10.jpg\",\"simage\":\"catalog\\/demo\\/image-block\\/14.jpg\"}'),
(42, 'OC Testimonials Home1', 'octestimonial', '{\"name\":\"OC Testimonials Home1\",\"title\":{\"en-gb\":{\"title\":\"Testimonials\"}},\"status\":\"1\",\"width\":\"71\",\"height\":\"71\",\"auto\":\"0\",\"items\":\"1\",\"limit\":\"8\",\"speed\":\"3000\",\"rows\":\"2\",\"navigation\":\"0\",\"pagination\":\"1\"}'),
(43, 'Block OurService', 'occmsblock', '{\"name\":\"Block OurService\",\"status\":\"1\",\"cmsblock\":\"22\"}'),
(45, 'Brands logo 1', 'carousel', '{\"name\":\"Brands logo 1\",\"banner_id\":\"8\",\"width\":\"184\",\"height\":\"141\",\"status\":\"1\"}'),
(46, 'Block content bottom', 'occmsblock', '{\"name\":\"Block content bottom\",\"status\":\"1\",\"cmsblock\":\"23\"}'),
(48, 'Block banner category', 'occmsblock', '{\"name\":\"Block banner category\",\"status\":\"1\",\"cmsblock\":\"24\"}'),
(49, 'Block content left 1', 'occmsblock', '{\"name\":\"Block content left 1\",\"status\":\"1\",\"cmsblock\":\"25\"}'),
(50, 'OC Slide show 2', 'ocslideshow', '{\"name\":\"OC Slide show 2\",\"status\":\"1\",\"banner\":\"19\",\"width\":\"1170\",\"height\":\"526\"}'),
(51, 'Block content top 2', 'occmsblock', '{\"name\":\"Block content top 2\",\"status\":\"1\",\"cmsblock\":\"29\"}'),
(54, 'OC Horizontal Mega Menu 2', 'ochozmegamenu', '{\"name\":\"OC Horizontal Mega Menu 2\",\"hactive\":\"CAT20,CAT33,CAT57,CAT17\",\"status\":\"1\",\"hhome\":\"1\",\"hdepth\":\"4\",\"hlevel\":\"4\"}'),
(55, 'OC Slide show 3', 'ocslideshow', '{\"name\":\"OC Slide show 3\",\"status\":\"1\",\"banner\":\"20\",\"width\":\"870\",\"height\":\"511\"}'),
(56, 'Block content top 3', 'occmsblock', '{\"name\":\"Block content top 3\",\"status\":\"1\",\"cmsblock\":\"30\"}'),
(57, 'OC Product Tabs Slider 3', 'octabproductslider', '{\"name\":\"OC Product Tabs Slider 3\",\"title\":{\"en-gb\":{\"title\":\"Our Products\"}},\"status\":\"1\",\"types\":[\"bestseller\",\"mostviewed\",\"special\"],\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"7\",\"item\":\"4\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"1\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"1\",\"shownextback\":\"1\",\"shownav\":\"0\"}'),
(58, 'OC Slide show 4', 'ocslideshow', '{\"name\":\"OC Slide show 4\",\"status\":\"1\",\"banner\":\"21\",\"width\":\"771\",\"height\":\"548\"}'),
(59, 'Banner left home 4', 'occmsblock', '{\"name\":\"Banner left home 4\",\"status\":\"1\",\"cmsblock\":\"31\"}'),
(60, 'Banner right home 4', 'occmsblock', '{\"name\":\"Banner right home 4\",\"status\":\"1\",\"cmsblock\":\"32\"}'),
(61, 'Block content top 4', 'occmsblock', '{\"name\":\"Block content top 4\",\"status\":\"1\",\"cmsblock\":\"33\"}'),
(62, 'OC Price Countdown', 'occountdown', '{\"name\":\"OC Price Countdown\",\"title\":{\"en-gb\":{\"title\":\"Countdown\"}},\"status\":\"1\",\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"showlabel\":\"1\",\"limit\":\"3\",\"item\":\"1\",\"speed\":\"5000\",\"autoplay\":\"0\",\"rows\":\"1\",\"shownextback\":\"0\",\"shownav\":\"0\"}'),
(70, 'OC Category Tabs Slider Saw machine 2', 'octabcategoryslider', '{\"name\":\"OC Category Tabs Slider Saw machine 2\",\"title\":{\"en-gb\":{\"title\":\"Saw machine\"}},\"status\":\"1\",\"category\":\"\",\"product_category\":[\"33\"],\"thumbnail\":\"1\",\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"6\",\"item\":\"5\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"1\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"1\",\"shownextback\":\"1\",\"shownav\":\"0\",\"fimage\":\"catalog\\/demo\\/image-block\\/4.jpg\",\"simage\":\"catalog\\/demo\\/image-block\\/15.jpg\"}'),
(68, 'OC Category Tabs Slider Saw machine', 'octabcategoryslider', '{\"name\":\"OC Category Tabs Slider Saw machine\",\"title\":{\"ru-ru\":{\"title\":\"\"}},\"status\":\"1\",\"category\":\"\",\"product_category\":[\"33\"],\"thumbnail\":\"1\",\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"6\",\"item\":\"4\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"1\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"1\",\"shownextback\":\"1\",\"shownav\":\"0\",\"fimage\":\"catalog\\/banners\\/555x170 Saw.png\",\"simage\":\"catalog\\/banners\\/263x170 Saw.png\"}'),
(69, 'OC Category Tabs Slider Drill', 'octabcategoryslider', '{\"name\":\"OC Category Tabs Slider Drill\",\"title\":{\"ru-ru\":{\"title\":\"\"}},\"status\":\"1\",\"category\":\"\",\"product_category\":[\"20\"],\"thumbnail\":\"1\",\"rotator\":\"1\",\"width\":\"400\",\"height\":\"400\",\"limit\":\"6\",\"item\":\"4\",\"speed\":\"3000\",\"autoplay\":\"0\",\"rows\":\"1\",\"showlabel\":\"1\",\"showprice\":\"1\",\"showdes\":\"0\",\"showaddtocart\":\"1\",\"shownextback\":\"1\",\"shownav\":\"0\",\"fimage\":\"catalog\\/banners\\/555x186 TabsSliderDrill.png\",\"simage\":\"catalog\\/banners\\/Apple-Logo-263x170.png\"}');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_ocslideshow`
--

CREATE TABLE `oc_ocslideshow` (
  `ocslideshow_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `auto` tinyint(1) DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `hover` tinyint(1) DEFAULT NULL,
  `nextback` tinyint(1) DEFAULT NULL,
  `contrl` tinyint(1) DEFAULT NULL,
  `effect` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_ocslideshow`
--

INSERT INTO `oc_ocslideshow` (`ocslideshow_id`, `name`, `status`, `auto`, `delay`, `hover`, `nextback`, `contrl`, `effect`) VALUES
(18, 'OC Slide show 1', 1, 1, 5000, 1, 1, 1, 'random'),
(19, 'OC Slide show 2', 1, 1, 5000, 1, 1, 0, 'random'),
(20, 'OC Slide show 3', 1, 1, 5000, 1, 1, 0, 'random'),
(21, 'OC Slide show 4', 1, 1, 5000, 1, 1, 0, 'random');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_ocslideshow_image`
--

CREATE TABLE `oc_ocslideshow_image` (
  `ocslideshow_image_id` int(11) NOT NULL,
  `ocslideshow_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `banner_store` varchar(255) DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `small_image` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_ocslideshow_image`
--

INSERT INTO `oc_ocslideshow_image` (`ocslideshow_image_id`, `ocslideshow_id`, `link`, `type`, `banner_store`, `image`, `small_image`) VALUES
(278, 18, '#', 2, '0', 'catalog/banners/main/1111.jpg', 'no_image.jpg'),
(276, 19, '#', 2, '0', 'catalog/demo/banners/banner7-2-1.jpg', 'no_image.jpg'),
(260, 20, '#', 1, '0', 'catalog/demo/banners/banner7-4-1.jpg', 'no_image.jpg'),
(262, 21, '#', 2, '0', 'catalog/demo/banners/banner7-3-2.jpg', 'no_image.jpg'),
(275, 19, '#', 2, '0', 'catalog/demo/banners/banner7-2-2.jpg', 'no_image.jpg'),
(261, 21, '#', 2, '0', 'catalog/demo/banners/banner7-3-1.jpg', 'no_image.jpg'),
(259, 20, '#', 2, '0', 'catalog/demo/banners/banner7-4-2.jpg', 'no_image.jpg'),
(277, 18, '#', 2, '0', 'catalog/banners/main/2222.jpg', 'no_image.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_ocslideshow_image_description`
--

CREATE TABLE `oc_ocslideshow_image_description` (
  `ocslideshow_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `ocslideshow_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `sub_title` varchar(64) DEFAULT NULL,
  `description` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_ocslideshow_image_description`
--

INSERT INTO `oc_ocslideshow_image_description` (`ocslideshow_image_id`, `language_id`, `ocslideshow_id`, `title`, `sub_title`, `description`) VALUES
(278, 1, 18, 'Samsung Samsung Samsung', 'Samsung Samsung 70%', 'Lorem ipsum dolor sit amet, sumo sensibus rationibus has in, ei sea simul tantas animal. Ea has recteque postulant, aperiam commune pericula mei ei, no sit audire persequeris referrentur.'),
(277, 1, 18, 'Apple Apple Apple ', 'Apple Apple 70%', 'Lorem ipsum dolor sit amet, sumo sensibus rationibus has in, ei sea simul tantas animal. Ea has recteque postulant, aperiam commune pericula mei ei, no sit audire persequeris referrentur.'),
(275, 1, 19, 'Samsung', 'Samsung', 'Lorem ipsum dolor sit amet, sumo sensibus rationibus has in, ei sea simul tantas animal. Ea has recteque postulant, aperiam commune pericula mei ei, no sit audire persequeris referrentur.'),
(276, 1, 19, 'Apple Apple Apple ', 'Apple Apple Apple ', 'Lorem ipsum dolor sit amet, sumo sensibus rationibus has in, ei sea simul tantas animal. Ea has recteque postulant, aperiam commune pericula mei ei, no sit audire persequeris referrentur.'),
(262, 1, 21, 'Handheld Power Tools.', 'Sale up to 70% off ', 'Mauris eu ornare odio, commodo dignissim elit. In eros diam, porta quis fringilla ullamcorper, maximus in turpis. Curabitur eget iaculis augue, ut commodo lectus...'),
(261, 1, 21, 'Circular Saw GKS 9881', 'incredible collection', 'Mauris eu ornare odio, commodo dignissim elit. In eros diam, porta quis fringilla ullamcorper, maximus in turpis. Curabitur eget iaculis augue, ut commodo lectus...'),
(259, 1, 20, 'Handheld Power Tools.', 'incredible collection', 'Mauris eu ornare odio, commodo dignissim elit. In eros diam, porta quis fringilla ullamcorper, maximus in turpis. Curabitur eget iaculis augue, ut commodo lectus...'),
(260, 1, 20, 'Circular Saw GKS 9881', 'Sale up to 70% off ', 'Mauris eu ornare odio, commodo dignissim elit. In eros diam, porta quis fringilla ullamcorper, maximus in turpis. Curabitur eget iaculis augue, ut commodo lectus...');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `mf_tooltip` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`, `mf_tooltip`) VALUES
(1, 1, 'Radio', NULL),
(2, 1, 'Checkbox', NULL),
(4, 1, 'Text', NULL),
(6, 1, 'Textarea', NULL),
(8, 1, 'Date', NULL),
(7, 1, 'File', NULL),
(5, 1, 'Select', NULL),
(9, 1, 'Time', NULL),
(10, 1, 'Date &amp; Time', NULL),
(12, 1, 'Delivery Date', NULL),
(11, 1, 'Size', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2013-00', 0, 'Default', 'http://tt_sonic1.com/', 1, 1, 'mr', 'mid', 'demo@towerthemes.com', '0974221186', '', '', 'mr', 'mid', '', '15-12 t05 timecity', '', 'hai ba trung', '0123456', 'Tuvalu', 218, 'Niulakita', 3416, '', '[]', 'Cash On Delivery', 'cod', 'mr', 'mid', '', '15-12 t05 timecity', '', 'hai ba trung', '0123456', 'Tuvalu', 218, 'Niulakita', 3416, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', '556.0000', 1, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2016-10-05 15:30:25', '2016-10-05 15:30:26');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_custom_field`
--

CREATE TABLE `oc_order_custom_field` (
  `order_custom_field_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `location` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(1, 1, 1, 0, '', '2016-10-05 15:30:26');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_order_option`
--

INSERT INTO `oc_order_option` (`order_option_id`, `order_id`, `order_product_id`, `product_option_id`, `product_option_value_id`, `name`, `value`, `type`) VALUES
(1, 1, 2, 225, 0, 'Delivery Date', '2011-04-22', 'date');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(1, 1, 48, 'Sander Polisher S445VA', 'product 20', 1, '100.0000', '100.0000', '0.0000', 0),
(2, 1, 47, 'Drill Machine GBM 9', 'Product 21', 1, '100.0000', '100.0000', '0.0000', 300),
(3, 1, 32, 'Circular Saw GKS 735', 'Product 5', 1, '100.0000', '100.0000', '0.0000', 0),
(4, 1, 28, 'Drill Machine GBM 23-2', 'Product 1', 1, '100.0000', '100.0000', '0.0000', 400),
(5, 1, 34, 'Circular Saw GKS 235', 'Product 7', 1, '50.0000', '50.0000', '0.0000', 0),
(6, 1, 40, 'Sander Polisher SP98VA', 'product 11', 1, '101.0000', '101.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Sub-Total', '551.0000', 1),
(2, 1, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(3, 1, 'total', 'Total', '556.0000', 9);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(28, 'Product 1', '', '', '', '', '', '', '', 938, 7, 'catalog/products/id28/Sony-PlayStation-4-Pro-1TB-Black-4-600x600.jpg', 5, 1, '13000.0000', 200, 0, '2009-02-03', '146.40000000', 2, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 9, '2009-02-03 16:06:50', '2017-11-19 16:00:03'),
(29, 'Product 2', '', '', '', '', '', '', '', 999, 6, 'catalog/products/id29/Apple-iPhone-7-Plus-256GB-1-600x600.jpg', 6, 1, '27000.0000', 0, 0, '2009-02-03', '133.00000000', 2, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, 0, '2009-02-03 16:42:17', '2017-11-19 15:57:51'),
(30, 'Product 3', '', '', '', '', '', '', '', 999, 6, 'catalog/products/id30/HP-250-G5-(Z2Z64ES)-Black-5-600x600.jpg', 9, 1, '15000.0000', 0, 0, '0000-00-00', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 5, '2009-02-03 16:59:00', '2017-11-19 15:59:56'),
(31, 'Product 4', '', '', '', '', '', '', '', 1000, 6, 'catalog/products/id31/Xiaomi-Redmi-4x-3-32GB-Black-2-600x600.jpg', 0, 1, '3600.0000', 0, 0, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 3, 1, 1, 0, 1, 0, '2009-02-03 17:00:10', '2017-11-19 15:59:22'),
(32, 'Product 7', '', '', '', '', '', '', '', 998, 6, 'catalog/products/id32/Apple-MacBook-Pro-Retina-MJLQ2UAA-3-600x600.jpg', 8, 1, '65000.0000', 0, 0, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 16, '2009-02-03 17:07:26', '2017-11-19 15:59:39'),
(33, 'Product 6', '', '', '', '', '', '', '', 1000, 6, 'catalog/products/id33/Apple-Wireless-Magic-Mouse-White-1-600x600.jpg', 0, 1, '100.0000', 0, 0, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 1, '2009-02-03 17:08:31', '2017-11-19 15:59:32'),
(34, 'Product 7', '', '', '', '', '', '', '', 999, 6, 'catalog/products/id34/Apple-iPhone-8-64GB-05-600x600.jpg', 8, 1, '25499.0000', 0, 0, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 30, '2009-02-03 18:07:54', '2017-11-19 15:58:00'),
(35, 'Product 8', '', '', '', '', '', '', '', 1000, 5, 'catalog/products/id35/Lenovo-IdeaPad-110-15IBR-80T7004QRA-Black-1-600x600.jpg', 0, 0, '10000.0000', 0, 0, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 0, '2009-02-03 18:08:31', '2017-11-19 15:58:51'),
(36, 'Product 9', '', '', '', '', '', '', '', 994, 6, 'catalog/products/id36/Samsung-UE55KS7500UXUA-1-600x600.jpg', 8, 0, '60000.0000', 100, 0, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 18:09:19', '2017-11-19 15:59:10'),
(40, 'product 11', '', '', '', '', '', '', '', 969, 5, 'catalog/products/id40/A4Tech-Bloody-B328-Black-2-600x600.jpg', 8, 1, '3500.0000', 0, 0, '2009-02-03', '10.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 27, '2009-02-03 21:07:12', '2017-11-19 15:57:26'),
(41, 'Product 14', '', '', '', '', '', '', '', 977, 5, 'catalog/products/id41/D5300-kit-18-55mm-VR-Black-1-600x600.jpg', 8, 1, '20000.0000', 0, 0, '2009-02-03', '5.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 1, '2009-02-03 21:07:26', '2017-11-19 16:00:16'),
(42, 'GBM 13 RE', '', '', '', '', '', '', '', 990, 5, 'catalog/products/id42/ekshn-kamera-GoPro-HERO-6-01-600x600.jpg', 8, 1, '19000.0000', 400, 0, '2009-02-04', '12.50000000', 1, '1.00000000', '2.00000000', '3.00000000', 1, 1, 2, 0, 1, 6, '2009-02-03 21:07:37', '2017-11-19 16:00:22'),
(43, 'Product 16', '', '', '', '', '', '', '', 929, 5, 'catalog/products/id43/Apple-iPad-A1823-(MPG42RKA)-4G-1-600x600.jpg', 8, 0, '20000.0000', 0, 0, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 8, '2009-02-03 21:07:49', '2017-11-19 15:57:40'),
(44, 'Product 17', '', '', '', '', '', '', '', 1000, 5, 'catalog/products/id44/Nikon-40mm-f2.8G-ED-AF-S-DX-Micro-Nikkor-1-600x600.jpg', 8, 1, '9999.0000', 0, 0, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 1, '2009-02-03 21:08:00', '2017-11-19 15:59:02'),
(45, 'Product 18', '', '', '', '', '', '', '', 998, 5, 'catalog/products/id45/1.jpg', 8, 1, '25000.0000', 0, 0, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 0, '2009-02-03 21:08:17', '2017-11-19 15:58:22'),
(46, 'Product 19', '', '', '', '', '', '', '', 25, 5, 'catalog/products/id46/Canon-EOS-750D-kit-(18-135mm)-IS-STM-Black-1-600x600 (1).jpg', 10, 1, '27000.0000', 0, 0, '2009-02-03', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 1, '2009-02-03 21:08:29', '2017-11-19 16:00:10'),
(47, 'Product 21', '', '', '', '', '', '', '', 1000, 5, 'catalog/products/id47/smart-godinnik-KingWear-KW18-01-600x600.jpg', 7, 1, '9999.0000', 400, 0, '2009-02-03', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 0, 1, 0, 1, 16, '2009-02-03 21:08:40', '2017-11-19 15:58:40'),
(48, 'product 20', 'test 1', '', '', '', '', '', 'test 2', 994, 5, 'catalog/products/id48/Apple-Watch-Sport-38mm-Gold-Aluminum-Case-Antique-White-Sport-Band-1-600x600.jpg', 8, 1, '11111.0000', 0, 0, '2009-02-08', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 2, 1, 1, 0, 1, 52, '2009-02-08 17:21:51', '2017-11-19 15:58:07'),
(49, 'SAM1', '', '', '', '', '', '', '', 0, 8, 'catalog/products/id49/Apple-A1574-iPod-Touch-64GB-Gold-1-600x600.jpg', 8, 1, '12000.0000', 0, 0, '2011-04-25', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 5, '2011-04-26 08:57:34', '2017-11-19 15:57:31');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(43, 2, 1, '1'),
(47, 4, 1, '16GB'),
(43, 4, 1, '8gb'),
(42, 3, 1, '100mhz'),
(47, 2, 1, '4');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(44, 1, 'NIKON 40MM F/2.8G ED AF-S DX MICRO NIKKOR ', '&lt;div&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/div&gt;\r\n', '', 'NIKON 40MM F/2.8G ED AF-S DX MICRO NIKKOR ', '', ''),
(45, 1, 'Dell Inspiron 7567 (I757810NDL-60B) Black ', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'Dell Inspiron 7567 (I757810NDL-60B) Black ', '', ''),
(29, 1, 'APPLE IPHONE 7 PLUS 256GB JET BLACK ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;ul&gt;\r\n&lt;/ul&gt;\r\n', '', 'APPLE IPHONE 7 PLUS 256GB JET BLACK ', '', ''),
(36, 1, 'SAMSUNG UE55KS7500UXUA ', '&lt;div&gt;\r\n	&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'SAMSUNG UE55KS7500UXUA ', '', ''),
(47, 1, 'KINGWEAR KW18 BLACK ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n', '', 'KINGWEAR KW18 BLACK ', '', ''),
(41, 1, 'ФОТОАПАРАТ NIKON D5300 KIT ', '&lt;div&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/div&gt;\r\n', '', 'ФОТОАПАРАТ NIKON D5300 KIT ', '', ''),
(43, 1, 'APPLE IPAD A1823 (MP1J2RK/A) SPACE GREY 32 GB / 4G ', '&lt;div&gt;\r\n	&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'APPLE IPAD A1823 (MP1J2RK/A) SPACE GREY 32 GB / 4G ', '', ''),
(31, 1, 'XIAOMI REDMI 4X 3/32GB BLACK ', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'XIAOMI REDMI 4X 3/32GB BLACK ', '', ''),
(49, 1, 'APPLE A1574 IPOD TOUCH 64GB GOLD ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n', '', 'APPLE A1574 IPOD TOUCH 64GB GOLD ', '', ''),
(42, 1, 'ЭКШН-КАМЕРА GOPRO HERO 6 BLACK ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;ul&gt;\r\n&lt;/ul&gt;\r\n', '', 'ЭКШН-КАМЕРА GOPRO HERO 6 BLACK ', '', ''),
(40, 1, 'A4TECH BLOODY B328 BLACK ', '&lt;p class=&quot;intro&quot;&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n', '', 'A4TECH BLOODY B328 BLACK ', '', ''),
(48, 1, 'APPLE WATCH SPORT 38MM GOLD ALUMINUM CASE ANTIQUE WHITE SPORT BAND', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'APPLE WATCH SPORT 38MM GOLD ALUMINUM CASE ANTIQUE WHITE SPORT BAND', '', ''),
(34, 1, 'APPLE IPHONE 8 64GB SILVER ', '&lt;div&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&lt;/span&gt;&lt;br&gt;\r\n\r\n&lt;/div&gt;\r\n', 'APPLE IPHONE ', 'APPLE IPHONE 8 64GB SILVER ', '', ''),
(32, 1, 'НОУТБУК 15.4&quot; APPLE MACBOOK PRO RETINA 15&quot; ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;\r\n	&amp;nbsp;&lt;/p&gt;\r\n', '', 'НОУТБУК 15.4&quot; APPLE MACBOOK PRO RETINA 15&quot; ', '', ''),
(33, 1, 'МИШКА БЕЗПРОВІДНА APPLE WIRELESS MAGIC MOUSE WHITE ', '&lt;div&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&lt;/span&gt;&lt;br&gt;&lt;/div&gt;\r\n', '', 'МИШКА БЕЗПРОВІДНА APPLE WIRELESS MAGIC MOUSE WHITE ', '', ''),
(46, 1, 'Фотоапарат Canon EOS 750D kit ', '&lt;div&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/div&gt;\r\n', '', 'Фотоапарат Canon EOS 750D kit ', '', ''),
(30, 1, 'НОУТБУК 15.6&quot; HP 250 G5 (Z2Z64ES) BLACK ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n', '', 'НОУТБУК 15.6&quot; HP 250 G5 (Z2Z64ES) BLACK ', '', ''),
(28, 1, 'ПРИСТАВКА SONY PLAYSTATION 4 PRO 1TB BLACK ', '&lt;p&gt;\r\n	HTC Touch - in High Definition. Watch music videos and streaming content in awe-inspiring high definition clarity for a mobile experience you never thought possible. Seductively sleek, the HTC Touch HD provides the next generation of mobile functionality, all at a simple touch. Fully integrated with Windows Mobile Professional 6.1, ultrafast 3.5G, GPS, 5MP camera, plus lots more - all delivered on a breathtakingly crisp 3.8&amp;quot; WVGA touchscreen - you can take control of your mobile world with the HTC Touch HD.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Processor Qualcomm&amp;reg; MSM 7201A&amp;trade; 528 MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Operating System&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Memory: 512 MB ROM, 288 MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Dimensions: 115 mm x 62.8 mm x 12 mm / 146.4 grams&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.8-inch TFT-LCD flat touch-sensitive screen with 480 x 800 WVGA resolution&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/WCDMA: Europe/Asia: 900/2100 MHz; Up to 2 Mbps up-link and 7.2 Mbps down-link speeds&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM/GPRS/EDGE: Europe/Asia: 850/900/1800/1900 MHz (Band frequency, HSUPA availability, and data speed are operator dependent.)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Device Control via HTC TouchFLO&amp;trade; 3D &amp;amp; Touch-sensitive front panel buttons&lt;/li&gt;\r\n	&lt;li&gt;\r\n		GPS and A-GPS ready&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth&amp;reg; 2.0 with Enhanced Data Rate and A2DP for wireless stereo headsets&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Wi-Fi&amp;reg;: IEEE 802.11 b/g&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HTC ExtUSB&amp;trade; (11-pin mini-USB 2.0)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		5 megapixel color camera with auto focus&lt;/li&gt;\r\n	&lt;li&gt;\r\n		VGA CMOS color camera&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in 3.5 mm audio jack, microphone, speaker, and FM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Ring tone formats: AAC, AAC+, eAAC+, AMR-NB, AMR-WB, QCP, MP3, WMA, WAV&lt;/li&gt;\r\n	&lt;li&gt;\r\n		40 polyphonic and standard MIDI format 0 and 1 (SMF)/SP MIDI&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Rechargeable Lithium-ion or Lithium-ion polymer 1350 mAh battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Expansion Slot: microSD&amp;trade; memory card (SD 2.0 compatible)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		AC Adapter Voltage range/frequency: 100 ~ 240V AC, 50/60 Hz DC output: 5V and 1A&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Special Features: FM Radio, G-Sensor&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', 'ПРИСТАВКА SONY PLAYSTATION 4 PRO 1TB BLACK ', '', ''),
(35, 1, 'LENOVO IDEAPAD 110-15IBR (80T7004QRA) BLACK ', '&lt;p&gt;&lt;span style=&quot;font-family: &amp;quot;Open Sans&amp;quot;, Arial, sans-serif; font-size: 14px; text-align: justify;&quot;&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;\r\n', '', 'LENOVO IDEAPAD 110-15IBR (80T7004QRA) BLACK ', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_discount`
--

INSERT INTO `oc_product_discount` (`product_discount_id`, `product_id`, `customer_group_id`, `quantity`, `priority`, `price`, `date_start`, `date_end`) VALUES
(470, 42, 1, 30, 1, '66.0000', '0000-00-00', '0000-00-00'),
(469, 42, 1, 20, 1, '77.0000', '0000-00-00', '0000-00-00'),
(468, 42, 1, 10, 1, '88.0000', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_filter`
--

INSERT INTO `oc_product_filter` (`product_id`, `filter_id`) VALUES
(28, 1),
(28, 22),
(30, 1),
(30, 21),
(32, 1),
(32, 6),
(32, 21),
(33, 1),
(33, 21),
(34, 21),
(34, 22),
(41, 21),
(41, 22),
(46, 21),
(46, 22);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `is_rotator` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`, `is_rotator`) VALUES
(2842, 30, 'catalog/products/id30/HP-250-G5-(Z2Z64ES)-Black-2-600x600.jpg', 0, 0),
(2818, 45, 'catalog/products/id45/Dell-Inspiron-7567-(I757810NDL-60B)-Black-4-600x600.jpg', 0, 1),
(2832, 31, 'catalog/products/id31/Xiaomi-Redmi-4x-3-32GB-Black-5-600x600.jpg', 0, 0),
(2814, 48, 'catalog/products/id48/Apple-Watch-Sport-38mm-Gold-Aluminum-Case-Antique-White-Sport-Band-2-600x600.jpg', 0, 0),
(2849, 41, 'catalog/products/id41/D5300-kit-18-55mm-VR-Black-9-600x600.jpg', 0, 0),
(2831, 31, 'catalog/products/id31/Xiaomi-Redmi-4x-3-32GB-Black-4-600x600.jpg', 0, 0),
(2847, 46, 'catalog/products/id46/Canon-EOS-750D-kit-(18-135mm)-IS-STM-Black-4-600x600.jpg', 0, 0),
(2807, 49, 'catalog/products/id49/Apple-A1574-iPod-Touch-64GB-Gold-3-600x600.jpg', 0, 0),
(2806, 49, 'catalog/products/id49/Apple-A1574-iPod-Touch-64GB-Gold-5-600x600.jpg', 0, 1),
(2851, 42, 'catalog/products/id42/ekshn-kamera-GoPro-HERO-6-02-600x600.jpg', 0, 0),
(2850, 42, 'catalog/products/id42/ekshn-kamera-GoPro-HERO-6-05-600x600.jpg', 0, 1),
(2822, 47, 'catalog/products/id47/smart-godinnik-KingWear-KW18-03-600x600.jpg', 0, 1),
(2812, 34, 'catalog/products/id34/Apple-iPhone-8-64GB-04-600x600.jpg', 0, 0),
(2811, 34, 'catalog/products/id34/Apple-iPhone-8-64GB-03-600x600.jpg', 0, 0),
(2845, 28, 'catalog/products/id28/Sony-PlayStation-4-Pro-1TB-Black-2-600x600.jpg', 0, 1),
(2810, 34, 'catalog/products/id34/Apple-iPhone-8-64GB-01-600x600.jpg', 0, 1),
(2838, 32, 'catalog/products/id32/Apple-MacBook-Pro-Retina-MJLQ2UAA-1-600x600.jpg', 0, 1),
(2837, 32, 'catalog/products/id32/Apple-MacBook-Pro-Retina-MJLQ2UAA-5-600x600.jpg', 0, 0),
(2834, 33, 'catalog/products/id33/Apple-Wireless-Magic-Mouse-White-4-600x600.jpg', 0, 1),
(2833, 33, 'catalog/products/id33/Apple-Wireless-Magic-Mouse-White-2-600x600.jpg', 0, 0),
(2846, 46, 'catalog/products/id46/Canon-EOS-750D-kit-(18-135mm)-IS-STM-Black-3-600x600.jpg', 0, 1),
(2841, 30, 'catalog/products/id30/HP-250-G5-(Z2Z64ES)-Black-4-600x600.jpg', 0, 1),
(2844, 28, 'catalog/products/id28/Sony-PlayStation-4-Pro-1TB-Black-7-600x600.jpg', 0, 0),
(2843, 28, 'catalog/products/id28/Sony-PlayStation-4-Pro-1TB-Black-5-600x600.jpg', 0, 0),
(2848, 41, 'catalog/products/id41/D5300-kit-18-55mm-VR-Black-2-600x600.jpg', 0, 1),
(2823, 35, 'catalog/products/id35/1.jpg', 0, 0),
(2824, 35, 'catalog/products/id35/Lenovo-IdeaPad-110-15IBR-80T7004QRA-Black-7-600x600.jpg', 0, 1),
(2805, 40, 'catalog/products/id40/A4Tech-Bloody-B328-Black-5-600x600.jpg', 0, 0),
(2821, 47, 'catalog/products/id47/smart-godinnik-KingWear-KW18-02-600x600.JPG', 0, 0),
(2804, 40, 'catalog/products/id40/A4Tech-Bloody-B328-Black-4-600x600.jpg', 0, 1),
(2813, 48, 'catalog/products/id48/Apple-Watch-Sport-38mm-Gold-Aluminum-Case-Antique-White-Sport-Band-4-600x600.jpg', 0, 1),
(2817, 45, 'catalog/products/id45/Dell-Inspiron-7567-(I757810NDL-60B)-Black-2-600x600.jpg', 0, 0),
(2828, 36, 'catalog/products/id36/Samsung-UE55KS7500UXUA-3-600x600.jpg', 0, 0),
(2827, 36, 'catalog/products/id36/Samsung-UE55KS7500UXUA-2-600x600.jpg', 0, 0),
(2808, 43, 'catalog/products/id43/Apple-iPad-A1823-(MPG42RKA)-4G-2-600x600.jpg', 0, 1),
(2826, 44, 'catalog/products/id44/Nikon-40mm-f2.8G-ED-AF-S-DX-Micro-Nikkor-4-600x600.jpg', 0, 1),
(2825, 44, 'catalog/products/id44/Nikon-40mm-f2.8G-ED-AF-S-DX-Micro-Nikkor-2-600x600.jpg', 0, 0),
(2809, 29, 'catalog/products/id29/Apple-iPhone-7-Plus-256GB-2-600x600.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_related`
--

INSERT INTO `oc_product_related` (`product_id`, `related_id`) VALUES
(28, 28),
(28, 33),
(28, 34),
(28, 41),
(28, 42),
(28, 47),
(30, 30),
(30, 32),
(30, 33),
(30, 34),
(30, 41),
(32, 30),
(32, 32),
(32, 34),
(32, 42),
(32, 46),
(32, 47),
(33, 28),
(33, 30),
(33, 33),
(33, 34),
(33, 46),
(34, 28),
(34, 30),
(34, 32),
(34, 33),
(34, 34),
(34, 46),
(40, 42),
(41, 28),
(41, 30),
(41, 41),
(41, 42),
(42, 28),
(42, 32),
(42, 40),
(42, 41),
(42, 48),
(43, 47),
(44, 47),
(46, 32),
(46, 33),
(46, 34),
(46, 46),
(47, 28),
(47, 32),
(47, 43),
(47, 44),
(47, 48),
(48, 42),
(48, 47);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_reward`
--

INSERT INTO `oc_product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(628, 42, 1, 100),
(623, 47, 1, 300),
(627, 28, 1, 400),
(619, 43, 1, 600),
(626, 30, 1, 200),
(624, 44, 1, 700),
(621, 45, 1, 800),
(618, 49, 1, 1000);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_special`
--

INSERT INTO `oc_product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(476, 42, 1, 1, '90.0000', '2016-09-30', '2022-09-30'),
(475, 30, 1, 1, '80.0000', '2016-09-30', '2019-09-30'),
(473, 34, 1, 0, '50.0000', '2016-09-30', '2023-09-30');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(28, 20),
(28, 24),
(28, 33),
(29, 20),
(29, 24),
(29, 57),
(30, 20),
(30, 33),
(30, 34),
(31, 20),
(31, 33),
(31, 57),
(32, 18),
(32, 20),
(32, 33),
(33, 20),
(33, 33),
(33, 57),
(34, 20),
(34, 33),
(34, 57),
(35, 18),
(35, 20),
(35, 33),
(36, 20),
(36, 33),
(36, 34),
(40, 20),
(40, 24),
(40, 33),
(40, 34),
(41, 20),
(41, 33),
(42, 20),
(42, 33),
(43, 20),
(43, 33),
(43, 34),
(43, 57),
(44, 18),
(44, 20),
(44, 33),
(45, 18),
(45, 20),
(45, 33),
(46, 18),
(46, 20),
(46, 33),
(47, 18),
(47, 20),
(47, 33),
(48, 20),
(48, 33),
(48, 34),
(49, 18),
(49, 20),
(49, 33),
(49, 57);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES
(42, 0, 0),
(30, 0, 0),
(47, 0, 0),
(28, 0, 0),
(41, 0, 0),
(40, 0, 0),
(48, 0, 0),
(36, 0, 0),
(34, 0, 0),
(32, 0, 0),
(43, 0, 0),
(44, 0, 0),
(45, 0, 0),
(31, 0, 0),
(29, 0, 0),
(35, 0, 0),
(49, 0, 0),
(33, 0, 0),
(46, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` longtext NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(12561, 0, 'shipping', 'shipping_sort_order', '3', 0),
(2, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(3, 0, 'sub_total', 'sub_total_status', '1', 0),
(4, 0, 'tax', 'tax_status', '1', 0),
(5, 0, 'total', 'total_sort_order', '9', 0),
(6, 0, 'total', 'total_status', '1', 0),
(7, 0, 'tax', 'tax_sort_order', '5', 0),
(8, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0),
(9, 0, 'cod', 'cod_sort_order', '5', 0),
(10, 0, 'cod', 'cod_total', '0.01', 0),
(11, 0, 'cod', 'cod_order_status_id', '1', 0),
(12, 0, 'cod', 'cod_geo_zone_id', '0', 0),
(13, 0, 'cod', 'cod_status', '1', 0),
(12560, 0, 'shipping', 'shipping_status', '1', 0),
(12559, 0, 'shipping', 'shipping_estimator', '1', 0),
(12550, 0, 'coupon', 'coupon_sort_order', '4', 0),
(12549, 0, 'coupon', 'coupon_status', '0', 0),
(34, 0, 'flat', 'flat_sort_order', '1', 0),
(35, 0, 'flat', 'flat_status', '1', 0),
(36, 0, 'flat', 'flat_geo_zone_id', '0', 0),
(37, 0, 'flat', 'flat_tax_class_id', '9', 0),
(41, 0, 'flat', 'flat_cost', '5.00', 0),
(42, 0, 'credit', 'credit_sort_order', '7', 0),
(43, 0, 'credit', 'credit_status', '1', 0),
(53, 0, 'reward', 'reward_sort_order', '2', 0),
(54, 0, 'reward', 'reward_status', '1', 0),
(146, 0, 'category', 'category_status', '1', 0),
(158, 0, 'account', 'account_status', '1', 0),
(159, 0, 'affiliate', 'affiliate_status', '1', 0),
(8409, 0, 'ocquickview', 'ocquickview_children_element', 'add-to-links', 0),
(11166, 1, 'theme_default', 'theme_default_image_location_width', '268', 0),
(12552, 0, 'voucher', 'voucher_sort_order', '8', 0),
(12551, 0, 'voucher', 'voucher_status', '0', 0),
(103, 0, 'free_checkout', 'free_checkout_status', '1', 0),
(104, 0, 'free_checkout', 'free_checkout_order_status_id', '1', 0),
(12641, 0, 'config', 'config_compression', '6', 0),
(12642, 0, 'config', 'config_secure', '0', 0),
(12643, 0, 'config', 'config_password', '1', 0),
(12644, 0, 'config', 'config_shared', '0', 0),
(12645, 0, 'config', 'config_encryption', 'VwZRrACt2Z8g5UMCbzkRpWsnqNnd6nXazyL9094rUWhWM4xqzulHuy6yShxBuv98z9WR5dyW2axUmDsufoSgKOLhTIY3gKXvhiAIQ4uB0UA9Af1ZCQPsX8ekp3brzdZjXYGt43ut3esEhg0DiR79cvOzcmP0mzLcTg8xAUehvUdsAD7nZO8a9Ii0JMITe7qF84YXW6vgwbkCEYOEImmVlQ2AA9iJR15buqNYa5vsnvq7rwVY5xQ7V6G3DLxd7Q5IDHgBW7yH518eawoV3q13p1VESXqDGaxGEuqHl2PTXRhId62I3t5rV8Xi77drDm7qawGTHSz0qBszSiT0EnKBV9Xzw5SkiY9MJQSl29u2LyY7v9ID6IPG2Tf1Os7CYIPZ8yDkJa2SPX1Yh247d1XSXqSc1J9JOoOE4heiP2B69kcGvtVkijL0dJQJ8hk7NHsTtJQSE0e1VKp88EJKxijHQxkhzar6R0BkzHtYIv0cbmjaTCR5GV4Ri8XM3iuOYaP7jFAa1um0k2FXoksICRRstQ6x2UIbfnfq7LdbB2W41pnG8COaImOrkQuQx3gzABoKWNx7FntKkum3fm0KPAOou113N5ZF3KV0986p4lGpDiyJGwuzJwwhwnOCVJsYXVs0x93xZr82cyVinAj5UZcx3OvlEqj2wXxcuBRbwfXEdynaesDzxc8gLOYSreRAzcUUFJhUGX4Qd6Hbw6c0LDlDAryv3X3GcErKpIa3sd6DxGY6X7rzk5DPnR7Q01ftZx6qbiGDlyF0qlGGxB2cSNuLgs3jCru3fN302QbLr5jBYCeZvrce1bttwbwhWvcrloPss8wNB39BzwyrvWUBBTJtRL5Rs1WeJl5OfADSkJrWbSw4ZVA6AW9DgYhDI0l7l4sxGFMqqzXDgZ9XsIJ2yvPl1NrN8JfnD44I6IcIouFTPeqbwPFZbwdDXwHoxqM4klZiS7WBYH8JOhY4GXrostV1mEXwNIK8qMRj6ekRMolXm1UIbY7KfnxhBtT7utLW2blR', 0),
(12639, 0, 'config', 'config_seo_url', '0', 0),
(12640, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(12166, 0, 'theme_default', 'theme_default_image_testimonial_height', '100', 0),
(12638, 0, 'config', 'config_maintenance', '0', 0),
(12637, 0, 'config', 'config_mail_alert_email', '', 0),
(12636, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(12618, 0, 'config', 'config_return_id', '0', 0),
(12619, 0, 'config', 'config_return_status_id', '2', 0),
(12620, 0, 'config', 'config_captcha', '', 0),
(12621, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(12622, 0, 'config', 'config_logo', 'catalog/logo_03.png', 0),
(12623, 0, 'config', 'config_icon', 'catalog/cart.png', 0),
(12624, 0, 'config', 'config_ftp_hostname', 'localhost', 0),
(12625, 0, 'config', 'config_ftp_port', '21', 0),
(12626, 0, 'config', 'config_ftp_username', 'rad', 0),
(12627, 0, 'config', 'config_ftp_password', 'rad', 0),
(12628, 0, 'config', 'config_ftp_root', '/', 0),
(12629, 0, 'config', 'config_ftp_status', '1', 0),
(12630, 0, 'config', 'config_mail_protocol', 'mail', 0),
(12631, 0, 'config', 'config_mail_parameter', '', 0),
(12632, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(12633, 0, 'config', 'config_mail_smtp_username', '', 0),
(12634, 0, 'config', 'config_mail_smtp_password', '', 0),
(12635, 0, 'config', 'config_mail_smtp_port', '25', 0),
(12168, 0, 'theme_default', 'theme_default_image_related_height', '400', 0),
(12169, 0, 'theme_default', 'theme_default_image_compare_width', '400', 0),
(12170, 0, 'theme_default', 'theme_default_image_compare_height', '400', 0),
(12171, 0, 'theme_default', 'theme_default_image_wishlist_width', '400', 0),
(12172, 0, 'theme_default', 'theme_default_image_wishlist_height', '400', 0),
(12173, 0, 'theme_default', 'theme_default_image_cart_width', '400', 0),
(12174, 0, 'theme_default', 'theme_default_image_cart_height', '400', 0),
(12175, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(12176, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(518, 0, 'installtemp', 'installtemp', '1', 0),
(519, 0, 'ocproductrotator', 'ocproductrotator_status', '1', 0),
(520, 0, 'ocproductrotator', 'ocproductrotator_width', '300', 0),
(521, 0, 'ocproductrotator', 'ocproductrotator_height', '300', 0),
(12167, 0, 'theme_default', 'theme_default_image_related_width', '400', 0),
(12658, 0, 'ocblog', 'ocblog_meta_description', 'Blog Description', 0),
(12657, 0, 'ocblog', 'ocblog_meta_title', 'Blog', 0),
(12656, 0, 'ocblog', 'ocblog_article_limit', '10', 0),
(874, 0, 'oclayerednavigation', 'oclayerednavigation_status', '1', 0),
(875, 0, 'oclayerednavigation', 'oclayerednavigation_loader_img', 'catalog/AjaxLoader.gif', 0),
(8408, 0, 'ocquickview', 'ocquickview_parent_element', 'product-thumb', 0),
(8407, 0, 'ocquickview', 'ocquickview_container', '#tab_bestseller_product;.product-grid;.view-related;', 0),
(8406, 0, 'ocquickview', 'ocquickview_loader_img', 'catalog/AjaxLoader.gif', 0),
(12164, 0, 'theme_default', 'theme_default_image_additional_height', '400', 0),
(934, 0, 'ocsearchcategory', 'ocsearchcategory_status', '1', 0),
(935, 0, 'ocsearchcategory', 'ocsearchcategory_ajax_enabled', '1', 0),
(936, 0, 'ocsearchcategory', 'ocsearchcategory_product_img', '1', 0),
(937, 0, 'ocsearchcategory', 'ocsearchcategory_product_price', '1', 0),
(938, 0, 'ocsearchcategory', 'ocsearchcategory_loader_img', 'catalog/AjaxLoader.gif', 0),
(12617, 0, 'config', 'config_affiliate_id', '4', 0),
(12616, 0, 'config', 'config_affiliate_commission', '5', 0),
(12615, 0, 'config', 'config_affiliate_auto', '1', 0),
(12614, 0, 'config', 'config_affiliate_approval', '0', 0),
(12613, 0, 'config', 'config_stock_checkout', '1', 0),
(12612, 0, 'config', 'config_stock_warning', '1', 0),
(11015, 4, 'theme_default', 'theme_default_image_location_height', '50', 0),
(11165, 1, 'theme_default', 'theme_default_image_cart_height', '400', 0),
(11164, 1, 'theme_default', 'theme_default_image_cart_width', '400', 0),
(11014, 4, 'theme_default', 'theme_default_image_location_width', '268', 0),
(11013, 4, 'theme_default', 'theme_default_image_cart_height', '400', 0),
(11012, 4, 'theme_default', 'theme_default_image_cart_width', '400', 0),
(11011, 4, 'theme_default', 'theme_default_image_wishlist_height', '400', 0),
(11010, 4, 'theme_default', 'theme_default_image_wishlist_width', '400', 0),
(11009, 4, 'theme_default', 'theme_default_image_compare_height', '400', 0),
(11008, 4, 'theme_default', 'theme_default_image_compare_width', '400', 0),
(11007, 4, 'theme_default', 'theme_default_image_related_height', '400', 0),
(11006, 4, 'theme_default', 'theme_default_image_related_width', '400', 0),
(11005, 4, 'theme_default', 'theme_default_image_testimonial_height', '100', 0),
(11004, 4, 'theme_default', 'theme_default_image_testimonial_width', '100', 0),
(11003, 4, 'theme_default', 'theme_default_image_blog_height', '585', 0),
(11002, 4, 'theme_default', 'theme_default_image_blog_width', '1140', 0),
(11001, 4, 'theme_default', 'theme_default_image_article_height', '585', 0),
(11000, 4, 'theme_default', 'theme_default_image_article_width', '1140', 0),
(10999, 4, 'theme_default', 'theme_default_image_additional_height', '400', 0),
(10998, 4, 'theme_default', 'theme_default_image_additional_width', '400', 0),
(10997, 4, 'theme_default', 'theme_default_image_product_height', '400', 0),
(10996, 4, 'theme_default', 'theme_default_image_product_width', '400', 0),
(10995, 4, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(10994, 4, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(10993, 4, 'theme_default', 'theme_default_image_thumb_height', '400', 0),
(10992, 4, 'theme_default', 'theme_default_image_thumb_width', '400', 0),
(10991, 4, 'theme_default', 'theme_default_image_category_height', '253', 0),
(8405, 0, 'ocquickview', 'ocquickview_status', '1', 0),
(10316, 0, 'ocnewproductpage', 'ocnewproductpage_height', '600', 0),
(10314, 0, 'ocnewproductpage', 'ocnewproductpage_limit', '8', 0),
(12655, 0, 'ocbestsellerpage', 'ocbestsellerpage_height', '600', 0),
(12654, 0, 'ocbestsellerpage', 'ocbestsellerpage_width', '600', 0),
(12653, 0, 'ocbestsellerpage', 'ocbestsellerpage_limit', '8', 0),
(12652, 0, 'ocbestsellerpage', 'ocbestsellerpage_status', '1', 0),
(12165, 0, 'theme_default', 'theme_default_image_testimonial_width', '100', 0),
(12611, 0, 'config', 'config_stock_display', '1', 0),
(12610, 0, 'config', 'config_api_id', '2', 0),
(12609, 0, 'config', 'config_fraud_status_id', '7', 0),
(10990, 4, 'theme_default', 'theme_default_image_category_width', '870', 0),
(10989, 4, 'theme_default', 'theme_default_product_description_length', '100', 0),
(10988, 4, 'theme_default', 'theme_default_product_limit', '8', 0),
(10987, 4, 'theme_default', 'theme_default_testimonial_limit', '8', 0),
(10986, 4, 'theme_default', 'theme_default_status', '1', 0),
(10985, 4, 'theme_default', 'theme_default_directory', 'tt_sonic2', 0),
(12608, 0, 'config', 'config_complete_status', '[\"5\",\"3\"]', 1),
(12607, 0, 'config', 'config_processing_status', '[\"5\",\"1\",\"2\",\"12\",\"3\"]', 1),
(12606, 0, 'config', 'config_order_status_id', '1', 0),
(12605, 0, 'config', 'config_checkout_id', '5', 0),
(12604, 0, 'config', 'config_checkout_guest', '1', 0),
(12603, 0, 'config', 'config_cart_weight', '1', 0),
(11163, 1, 'theme_default', 'theme_default_image_wishlist_height', '400', 0),
(11162, 1, 'theme_default', 'theme_default_image_wishlist_width', '400', 0),
(11161, 1, 'theme_default', 'theme_default_image_compare_height', '400', 0),
(11160, 1, 'theme_default', 'theme_default_image_compare_width', '400', 0),
(11159, 1, 'theme_default', 'theme_default_image_related_height', '400', 0),
(11158, 1, 'theme_default', 'theme_default_image_related_width', '400', 0),
(11157, 1, 'theme_default', 'theme_default_image_testimonial_height', '100', 0),
(11155, 1, 'theme_default', 'theme_default_image_blog_height', '585', 0),
(11156, 1, 'theme_default', 'theme_default_image_testimonial_width', '100', 0),
(11154, 1, 'theme_default', 'theme_default_image_blog_width', '1140', 0),
(11153, 1, 'theme_default', 'theme_default_image_article_height', '585', 0),
(11151, 1, 'theme_default', 'theme_default_image_additional_height', '400', 0),
(11152, 1, 'theme_default', 'theme_default_image_article_width', '1140', 0),
(11150, 1, 'theme_default', 'theme_default_image_additional_width', '400', 0),
(11149, 1, 'theme_default', 'theme_default_image_product_height', '400', 0),
(11148, 1, 'theme_default', 'theme_default_image_product_width', '400', 0),
(11147, 1, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(11146, 1, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(10684, 2, 'theme_default', 'theme_default_image_wishlist_height', '400', 0),
(10683, 2, 'theme_default', 'theme_default_image_wishlist_width', '400', 0),
(10682, 2, 'theme_default', 'theme_default_image_compare_height', '400', 0),
(10681, 2, 'theme_default', 'theme_default_image_compare_width', '400', 0),
(10680, 2, 'theme_default', 'theme_default_image_related_height', '400', 0),
(10679, 2, 'theme_default', 'theme_default_image_related_width', '400', 0),
(10678, 2, 'theme_default', 'theme_default_image_testimonial_height', '100', 0),
(10677, 2, 'theme_default', 'theme_default_image_testimonial_width', '100', 0),
(10676, 2, 'theme_default', 'theme_default_image_blog_height', '585', 0),
(10675, 2, 'theme_default', 'theme_default_image_blog_width', '1140', 0),
(10674, 2, 'theme_default', 'theme_default_image_article_height', '585', 0),
(10673, 2, 'theme_default', 'theme_default_image_article_width', '1140', 0),
(10672, 2, 'theme_default', 'theme_default_image_additional_height', '400', 0),
(10671, 2, 'theme_default', 'theme_default_image_additional_width', '400', 0),
(10670, 2, 'theme_default', 'theme_default_image_product_height', '400', 0),
(10669, 2, 'theme_default', 'theme_default_image_product_width', '400', 0),
(10668, 2, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(10667, 2, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(10666, 2, 'theme_default', 'theme_default_image_thumb_height', '400', 0),
(10665, 2, 'theme_default', 'theme_default_image_thumb_width', '400', 0),
(10664, 2, 'theme_default', 'theme_default_image_category_height', '253', 0),
(10663, 2, 'theme_default', 'theme_default_image_category_width', '870', 0),
(10662, 2, 'theme_default', 'theme_default_product_description_length', '100', 0),
(10713, 3, 'theme_default', 'theme_default_image_compare_height', '400', 0),
(10712, 3, 'theme_default', 'theme_default_image_compare_width', '400', 0),
(10711, 3, 'theme_default', 'theme_default_image_related_height', '400', 0),
(10710, 3, 'theme_default', 'theme_default_image_related_width', '400', 0),
(10709, 3, 'theme_default', 'theme_default_image_testimonial_height', '100', 0),
(10708, 3, 'theme_default', 'theme_default_image_testimonial_width', '100', 0),
(10707, 3, 'theme_default', 'theme_default_image_blog_height', '585', 0),
(10706, 3, 'theme_default', 'theme_default_image_blog_width', '1140', 0),
(10705, 3, 'theme_default', 'theme_default_image_article_height', '585', 0),
(10704, 3, 'theme_default', 'theme_default_image_article_width', '1140', 0),
(10703, 3, 'theme_default', 'theme_default_image_additional_height', '400', 0),
(10702, 3, 'theme_default', 'theme_default_image_additional_width', '400', 0),
(10701, 3, 'theme_default', 'theme_default_image_product_height', '400', 0),
(10700, 3, 'theme_default', 'theme_default_image_product_width', '400', 0),
(10699, 3, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(10698, 3, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(10697, 3, 'theme_default', 'theme_default_image_thumb_height', '400', 0),
(10696, 3, 'theme_default', 'theme_default_image_thumb_width', '400', 0),
(10695, 3, 'theme_default', 'theme_default_image_category_height', '253', 0),
(11167, 1, 'theme_default', 'theme_default_image_location_height', '50', 0),
(12163, 0, 'theme_default', 'theme_default_image_additional_width', '400', 0),
(12162, 0, 'theme_default', 'theme_default_image_product_height', '400', 0),
(12602, 0, 'config', 'config_invoice_prefix', 'INV-2017-00', 0),
(12601, 0, 'config', 'config_account_id', '3', 0),
(12600, 0, 'config', 'config_login_attempts', '5', 0),
(12599, 0, 'config', 'config_customer_price', '0', 0),
(12161, 0, 'theme_default', 'theme_default_image_product_width', '400', 0),
(12160, 0, 'theme_default', 'theme_default_image_popup_height', '1000', 0),
(12598, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(12159, 0, 'theme_default', 'theme_default_image_popup_width', '1000', 0),
(12597, 0, 'config', 'config_customer_group_id', '1', 0),
(10315, 0, 'ocnewproductpage', 'ocnewproductpage_width', '600', 0),
(10313, 0, 'ocnewproductpage', 'ocnewproductpage_status', '1', 0),
(12158, 0, 'theme_default', 'theme_default_image_thumb_height', '400', 0),
(12157, 0, 'theme_default', 'theme_default_image_thumb_width', '400', 0),
(11145, 1, 'theme_default', 'theme_default_image_thumb_height', '400', 0),
(11144, 1, 'theme_default', 'theme_default_image_thumb_width', '400', 0),
(11143, 1, 'theme_default', 'theme_default_image_category_height', '253', 0),
(11142, 1, 'theme_default', 'theme_default_image_category_width', '870', 0),
(10661, 2, 'theme_default', 'theme_default_product_limit', '8', 0),
(10660, 2, 'theme_default', 'theme_default_testimonial_limit', '8', 0),
(10659, 2, 'theme_default', 'theme_default_status', '1', 0),
(10658, 2, 'theme_default', 'theme_default_directory', 'tt_sonic3', 0),
(10694, 3, 'theme_default', 'theme_default_image_category_width', '870', 0),
(10693, 3, 'theme_default', 'theme_default_product_description_length', '100', 0),
(10692, 3, 'theme_default', 'theme_default_product_limit', '8', 0),
(10691, 3, 'theme_default', 'theme_default_testimonial_limit', '8', 0),
(10690, 3, 'theme_default', 'theme_default_status', '1', 0),
(10689, 3, 'theme_default', 'theme_default_directory', 'tt_sonic4', 0),
(12156, 0, 'theme_default', 'theme_default_image_category_height', '253', 0),
(12155, 0, 'theme_default', 'theme_default_image_category_width', '870', 0),
(12154, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(12152, 0, 'theme_default', 'theme_default_testimonial_limit', '8', 0),
(12153, 0, 'theme_default', 'theme_default_product_limit', '8', 0),
(11141, 1, 'theme_default', 'theme_default_product_description_length', '100', 0),
(11140, 1, 'theme_default', 'theme_default_product_limit', '8', 0),
(11139, 1, 'theme_default', 'theme_default_testimonial_limit', '8', 0),
(11138, 1, 'theme_default', 'theme_default_status', '1', 0),
(11137, 1, 'theme_default', 'theme_default_directory', 'tt_sonic2', 0),
(10685, 2, 'theme_default', 'theme_default_image_cart_width', '400', 0),
(10686, 2, 'theme_default', 'theme_default_image_cart_height', '400', 0),
(10687, 2, 'theme_default', 'theme_default_image_location_width', '268', 0),
(10688, 2, 'theme_default', 'theme_default_image_location_height', '50', 0),
(10714, 3, 'theme_default', 'theme_default_image_wishlist_width', '400', 0),
(10715, 3, 'theme_default', 'theme_default_image_wishlist_height', '400', 0),
(10716, 3, 'theme_default', 'theme_default_image_cart_width', '400', 0),
(10717, 3, 'theme_default', 'theme_default_image_cart_height', '400', 0),
(10718, 3, 'theme_default', 'theme_default_image_location_width', '268', 0),
(10719, 3, 'theme_default', 'theme_default_image_location_height', '50', 0),
(12596, 0, 'config', 'config_customer_search', '0', 0),
(12595, 0, 'config', 'config_customer_activity', '0', 0),
(12594, 0, 'config', 'config_customer_online', '0', 0),
(12593, 0, 'config', 'config_tax_customer', '', 0),
(12592, 0, 'config', 'config_tax_default', '', 0),
(12591, 0, 'config', 'config_tax', '0', 0),
(12590, 0, 'config', 'config_voucher_max', '1000', 0),
(12589, 0, 'config', 'config_voucher_min', '1', 0),
(12588, 0, 'config', 'config_review_guest', '1', 0),
(12587, 0, 'config', 'config_review_status', '1', 0),
(12586, 0, 'config', 'config_limit_admin', '100', 0),
(12585, 0, 'config', 'config_product_count', '0', 0),
(12584, 0, 'config', 'config_weight_class_id', '1', 0),
(12583, 0, 'config', 'config_length_class_id', '1', 0),
(12582, 0, 'config', 'config_currency_auto', '1', 0),
(12150, 0, 'theme_default', 'theme_default_directory', 'xemlab', 0),
(12151, 0, 'theme_default', 'theme_default_status', '1', 0),
(12581, 0, 'config', 'config_currency', 'UAN', 0),
(12580, 0, 'config', 'config_admin_language', 'ru-ru', 0),
(12579, 0, 'config', 'config_language', 'ru-ru', 0),
(12578, 0, 'config', 'config_zone_id', '3481', 0),
(12577, 0, 'config', 'config_country_id', '220', 0),
(12576, 0, 'config', 'config_comment', '', 0),
(12575, 0, 'config', 'config_open', '', 0),
(12574, 0, 'config', 'config_image', '', 0),
(12573, 0, 'config', 'config_fax', '(0123) 456 789', 0),
(12025, 0, 'mfilter_version', 'mfilter_version', '2.0.5.2', 0),
(12026, 0, 'mega_filter_module', 'mega_filter_module', '[]', 1),
(12030, 0, 'mfilter_license', 'mfilter_license', '{\"order_id\":\"0c0b3da4ac402bd86191d959be081114\",\"email\":\"286d481a297463c2cb41fe8fa139f5e5\",\"time\":1509633018}', 1),
(12027, 0, 'mega_filter_settings', 'mega_filter_settings', '{\"show_number_of_products\":\"1\",\"calculate_number_of_products\":\"1\",\"show_loader_over_results\":\"1\",\"css_style\":\"\",\"content_selector\":\"#mfilter-content-container\",\"refresh_results\":\"immediately\",\"attribs\":{\"price\":{\"enabled\":\"1\",\"sort_order\":\"-1\"}},\"layout_c\":\"3\",\"display_live_filter\":{\"items\":\"1\"}}', 1),
(12121, 0, 'mega_filter_seo', 'mega_filter_seo', '{\"enabled\":\"1\"}', 1),
(12029, 0, 'mega_filter_status', 'mega_filter_status', '1', 0),
(12572, 0, 'config', 'config_telephone', '(0123) 456789', 0),
(12571, 0, 'config', 'config_email', 'addresok@gmail.com', 0),
(12570, 0, 'config', 'config_geocode', '', 0),
(12569, 0, 'config', 'config_address', 'г. Черновцы. ул. &quot; &quot;', 0),
(12568, 0, 'config', 'config_owner', 'радио рынок', 0),
(12567, 0, 'config', 'config_name', 'Default', 0),
(12566, 0, 'config', 'config_layout_id', '1', 0),
(12565, 0, 'config', 'config_theme', 'theme_default', 0),
(12564, 0, 'config', 'config_meta_keyword', '', 0),
(12563, 0, 'config', 'config_meta_description', 'радио рынок', 0),
(12562, 0, 'config', 'config_meta_title', 'радио рынок', 0),
(12122, 0, 'mega_filter_filters', 'mega_filter_filters', '{\"2\":{\"enabled\":\"1\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"0\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\",\"sort_order\":\"\"},\"1\":{\"enabled\":\"1\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"0\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\",\"sort_order\":\"\"},\"default\":{\"enabled\":\"0\",\"type\":\"checkbox\",\"display_live_filter\":\"\",\"collapsed\":\"0\",\"display_list_of_items\":\"\",\"sort_order_values\":\"\"}}', 1),
(12646, 0, 'config', 'config_file_max_size', '300000', 0),
(12647, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(12648, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(12649, 0, 'config', 'config_error_display', '1', 0),
(12650, 0, 'config', 'config_error_log', '1', 0),
(12651, 0, 'config', 'config_error_filename', 'error.log', 0),
(12659, 0, 'ocblog', 'ocblog_meta_keyword', 'Blog Keyword', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_subscribe`
--

CREATE TABLE `oc_subscribe` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_id` varchar(225) NOT NULL,
  `name` varchar(225) NOT NULL,
  `date` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_testimonial`
--

CREATE TABLE `oc_testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_testimonial`
--

INSERT INTO `oc_testimonial` (`testimonial_id`, `status`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_testimonial_description`
--

CREATE TABLE `oc_testimonial_description` (
  `testimonial_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_testimonial_description`
--

INSERT INTO `oc_testimonial_description` (`testimonial_id`, `language_id`, `image`, `customer_name`, `content`) VALUES
(1, 0, 'catalog/testimonial/111.jpg', 'Rebecka Filson', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(2, 0, 'catalog/testimonial/111.jpg', 'Nathanael Jaworski', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(3, 0, 'catalog/testimonial/111.jpg', 'Magdalena Valencia', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(4, 0, 'catalog/testimonial/111.jpg', 'Alva Ono', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(5, 0, 'catalog/testimonial/111.jpg', 'Dewey Tetzlaff', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(6, 0, 'catalog/testimonial/111.jpg', 'Lavina Wilderman', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(7, 0, 'catalog/testimonial/111.jpg', 'Amber Laha', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.'),
(8, 0, 'catalog/testimonial/111.jpg', 'Lindsy Neloms', 'Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis tortor. Nunc scelerisque, nisi a blandit varius, nunc purus venenatis ligula, sed venenatis orci augue nec sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin mattis, enim blandit molestie molestie, nisl quam bibendum nisi, sed luctus felis justo ut nisl. In hac habitasse platea dictumst. Duis quis aliquam lectus, ac dapibus turpis. Nulla convallis vel felis eget porttitor. Morbi nisl metus, bibendum vitae luctus accumsan, consequat id quam.');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_url_alias`
--

CREATE TABLE `oc_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_url_alias`
--

INSERT INTO `oc_url_alias` (`url_alias_id`, `query`, `keyword`) VALUES
(1032, 'category_id=20', 'drill'),
(923, 'manufacturer_id=8', 'apple'),
(962, 'information_id=4', 'About Us'),
(1030, 'category_id=34', 'Safety'),
(1028, 'category_id=18', 'Skil Nail Gun'),
(1048, 'category_id=57', 'Electric Planner'),
(1027, 'category_id=24', 'smartphone'),
(1087, 'product_id=47', 'KINGWEAR KW18 BLACK '),
(1080, 'product_id=43', 'APPLE IPAD A1823 (MP1J2RK/A) SPACE GREY 32 GB / 4G '),
(1089, 'product_id=44', 'NIKON 40MM F/2.8G ED AF-S DX MICRO NIKKOR '),
(1092, 'product_id=31', 'XIAOMI REDMI 4X 3/32GB BLACK '),
(1079, 'product_id=49', 'APPLE A1574 IPOD TOUCH 64GB GOLD '),
(1093, 'product_id=33', 'applemouse'),
(1100, 'product_id=41', 'imac'),
(1090, 'product_id=36', 'SAMSUNG UE55KS7500UXUA '),
(1095, 'product_id=32', 'MacBook Ren'),
(924, 'manufacturer_id=9', 'canon'),
(926, 'manufacturer_id=5', 'htc'),
(925, 'manufacturer_id=7', 'hewlett-packard'),
(927, 'manufacturer_id=6', 'palm'),
(832, 'manufacturer_id=10', 'sony'),
(959, 'information_id=6', 'delivery'),
(960, 'information_id=3', 'privacy'),
(961, 'information_id=5', 'terms'),
(1082, 'product_id=34', 'APPLE IPHONE 8 '),
(1099, 'product_id=46', 'Canon EOS 750D kit '),
(1097, 'product_id=30', ' HP 250 G5 (Z2Z64ES) BLACK '),
(1098, 'product_id=28', 'ПРИСТАВКА SONY PLAYSTATION 4 PRO 1TB BLACK '),
(1088, 'product_id=35', 'LENOVO IDEAPAD 110-15IBR (80T7004QRA) BLACK '),
(1101, 'product_id=42', 'ЭКШН-КАМЕРА GOPRO HERO 6 BLACK '),
(1078, 'product_id=40', 'A4TECH BLOODY B328 BLACK '),
(1083, 'product_id=48', 'APPLE WATCH SPORT 38MM GOLD ALUMINUM CASE ANTIQUE WHITE SPORT BAND'),
(1085, 'product_id=45', 'Dell Inspiron 7567 (I757810NDL-60B) Black '),
(1081, 'product_id=29', 'APPLE IPHONE 7 PLUS 256GB JET BLACK ');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '4fbab8a853934bf9adda969e8a11450d9e204528', 'b5qwqmRhR', 'John', 'Doe', 'addresok@gmail.com', '', '', '127.0.0.1', 1, '2017-11-01 01:39:49');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{\"access\":[\"blog\\/article\",\"blog\\/articlelist\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/occategorythumbnail\",\"catalog\\/octestimonial\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/filemanager\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_group\",\"d_shopunity\\/account\",\"d_shopunity\\/dependency\",\"d_shopunity\\/developer\",\"d_shopunity\\/extension\",\"d_shopunity\\/filemanager\",\"d_shopunity\\/invoice\",\"d_shopunity\\/market\",\"d_shopunity\\/order\",\"d_shopunity\\/setting\",\"d_shopunity\\/tester\",\"d_shopunity\\/transaction\",\"design\\/banner\",\"design\\/language\",\"design\\/layout\",\"design\\/menu\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/theme\",\"extension\\/analytics\\/google_analytics\",\"extension\\/captcha\\/basic_captcha\",\"extension\\/captcha\\/google_captcha\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/event\",\"extension\\/extension\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/installer\",\"extension\\/modification\",\"extension\\/module\\/account\",\"extension\\/module\\/affiliate\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/installtemp\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/newslettersubscribe\",\"extension\\/module\\/ocajaxlogin\",\"extension\\/module\\/ocbestsellerpage\",\"extension\\/module\\/ocblog\",\"extension\\/module\\/occategorythumbnail\",\"extension\\/module\\/occmsblock\",\"extension\\/module\\/occountdown\",\"extension\\/module\\/ochozmegamenu\",\"extension\\/module\\/oclayerednavigation\",\"extension\\/module\\/ocnewproductpage\",\"extension\\/module\\/ocnewproductslider\",\"extension\\/module\\/ocproductrotator\",\"extension\\/module\\/ocquickview\",\"extension\\/module\\/ocsearchcategory\",\"extension\\/module\\/ocslideshow\",\"extension\\/module\\/octabcategoryslider\",\"extension\\/module\\/octabproductslider\",\"extension\\/module\\/octestimonial\",\"extension\\/module\\/ocvermegamenu\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/openbay\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/worldpay\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/store\",\"extension\\/theme\\/theme_default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"marketing\\/affiliate\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"module\\/brainyfilter\",\"module\\/d_quickcheckout\",\"module\\/d_shopunity\",\"module\\/mega_filter\",\"report\\/affiliate\",\"report\\/affiliate_activity\",\"report\\/affiliate_login\",\"report\\/customer_activity\",\"report\\/customer_credit\",\"report\\/customer_login\",\"report\\/customer_online\",\"report\\/customer_order\",\"report\\/customer_reward\",\"report\\/customer_search\",\"report\\/marketing\",\"report\\/product_purchased\",\"report\\/product_viewed\",\"report\\/sale_coupon\",\"report\\/sale_order\",\"report\\/sale_return\",\"report\\/sale_shipping\",\"report\\/sale_tax\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/compatibility\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/module\\/d_shopunity\",\"extension\\/module\\/d_quickcheckout\",\"extension\\/module\\/ocblog\",\"blog\\/article\",\"blog\\/articlelist\"],\"modify\":[\"blog\\/article\",\"blog\\/articlelist\",\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/occategorythumbnail\",\"catalog\\/octestimonial\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/filemanager\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_group\",\"d_shopunity\\/account\",\"d_shopunity\\/dependency\",\"d_shopunity\\/developer\",\"d_shopunity\\/extension\",\"d_shopunity\\/filemanager\",\"d_shopunity\\/invoice\",\"d_shopunity\\/market\",\"d_shopunity\\/order\",\"d_shopunity\\/setting\",\"d_shopunity\\/tester\",\"d_shopunity\\/transaction\",\"design\\/banner\",\"design\\/language\",\"design\\/layout\",\"design\\/menu\",\"design\\/theme\",\"design\\/translation\",\"event\\/compatibility\",\"event\\/theme\",\"extension\\/analytics\\/google_analytics\",\"extension\\/captcha\\/basic_captcha\",\"extension\\/captcha\\/google_captcha\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/event\",\"extension\\/extension\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/installer\",\"extension\\/modification\",\"extension\\/module\\/account\",\"extension\\/module\\/affiliate\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/installtemp\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/newslettersubscribe\",\"extension\\/module\\/ocajaxlogin\",\"extension\\/module\\/ocbestsellerpage\",\"extension\\/module\\/ocblog\",\"extension\\/module\\/occategorythumbnail\",\"extension\\/module\\/occmsblock\",\"extension\\/module\\/occountdown\",\"extension\\/module\\/ochozmegamenu\",\"extension\\/module\\/oclayerednavigation\",\"extension\\/module\\/ocnewproductpage\",\"extension\\/module\\/ocnewproductslider\",\"extension\\/module\\/ocproductrotator\",\"extension\\/module\\/ocquickview\",\"extension\\/module\\/ocsearchcategory\",\"extension\\/module\\/ocslideshow\",\"extension\\/module\\/octabcategoryslider\",\"extension\\/module\\/octabproductslider\",\"extension\\/module\\/octestimonial\",\"extension\\/module\\/ocvermegamenu\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/openbay\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/worldpay\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/store\",\"extension\\/theme\\/theme_default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"marketing\\/affiliate\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"module\\/brainyfilter\",\"module\\/d_quickcheckout\",\"module\\/d_shopunity\",\"module\\/mega_filter\",\"report\\/affiliate\",\"report\\/affiliate_activity\",\"report\\/affiliate_login\",\"report\\/customer_activity\",\"report\\/customer_credit\",\"report\\/customer_login\",\"report\\/customer_online\",\"report\\/customer_order\",\"report\\/customer_reward\",\"report\\/customer_search\",\"report\\/marketing\",\"report\\/product_purchased\",\"report\\/product_viewed\",\"report\\/sale_coupon\",\"report\\/sale_order\",\"report\\/sale_return\",\"report\\/sale_shipping\",\"report\\/sale_tax\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/compatibility\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\",\"extension\\/module\\/d_shopunity\",\"extension\\/module\\/d_quickcheckout\",\"extension\\/module\\/ocblog\",\"blog\\/article\",\"blog\\/articlelist\"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Структура таблиці `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Індекси таблиці `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  ADD PRIMARY KEY (`affiliate_id`);

--
-- Індекси таблиці `oc_affiliate_activity`
--
ALTER TABLE `oc_affiliate_activity`
  ADD PRIMARY KEY (`affiliate_activity_id`);

--
-- Індекси таблиці `oc_affiliate_login`
--
ALTER TABLE `oc_affiliate_login`
  ADD PRIMARY KEY (`affiliate_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Індекси таблиці `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  ADD PRIMARY KEY (`affiliate_transaction_id`);

--
-- Індекси таблиці `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Індекси таблиці `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Індекси таблиці `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Індекси таблиці `oc_article`
--
ALTER TABLE `oc_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Індекси таблиці `oc_article_description`
--
ALTER TABLE `oc_article_description`
  ADD PRIMARY KEY (`article_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Індекси таблиці `oc_article_list`
--
ALTER TABLE `oc_article_list`
  ADD PRIMARY KEY (`article_list_id`),
  ADD KEY `name` (`name`);

--
-- Індекси таблиці `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Індекси таблиці `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Індекси таблиці `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Індекси таблиці `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Індекси таблиці `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Індекси таблиці `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Індекси таблиці `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Індекси таблиці `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Індекси таблиці `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Індекси таблиці `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Індекси таблиці `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Індекси таблиці `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Індекси таблиці `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Індекси таблиці `oc_cmsblock`
--
ALTER TABLE `oc_cmsblock`
  ADD PRIMARY KEY (`cmsblock_id`);

--
-- Індекси таблиці `oc_cmsblock_description`
--
ALTER TABLE `oc_cmsblock_description`
  ADD PRIMARY KEY (`cmsblock_des_id`,`language_id`);

--
-- Індекси таблиці `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Індекси таблиці `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Індекси таблиці `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Індекси таблиці `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Індекси таблиці `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Індекси таблиці `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Індекси таблиці `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Індекси таблиці `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Індекси таблиці `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Індекси таблиці `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Індекси таблиці `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Індекси таблиці `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Індекси таблиці `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Індекси таблиці `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Індекси таблиці `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Індекси таблиці `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Індекси таблиці `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Індекси таблиці `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Індекси таблиці `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Індекси таблиці `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Індекси таблиці `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Індекси таблиці `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Індекси таблиці `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Індекси таблиці `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Індекси таблиці `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Індекси таблиці `oc_dqc_setting`
--
ALTER TABLE `oc_dqc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Індекси таблиці `oc_dqc_statistic`
--
ALTER TABLE `oc_dqc_statistic`
  ADD PRIMARY KEY (`statistic_id`);

--
-- Індекси таблиці `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Індекси таблиці `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Індекси таблиці `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Індекси таблиці `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Індекси таблиці `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Індекси таблиці `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Індекси таблиці `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Індекси таблиці `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Індекси таблиці `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Індекси таблиці `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Індекси таблиці `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Індекси таблиці `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Індекси таблиці `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Індекси таблиці `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Індекси таблиці `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Індекси таблиці `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Індекси таблиці `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Індекси таблиці `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Індекси таблиці `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Індекси таблиці `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Індекси таблиці `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Індекси таблиці `oc_menu`
--
ALTER TABLE `oc_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Індекси таблиці `oc_menu_description`
--
ALTER TABLE `oc_menu_description`
  ADD PRIMARY KEY (`menu_id`,`language_id`);

--
-- Індекси таблиці `oc_menu_module`
--
ALTER TABLE `oc_menu_module`
  ADD PRIMARY KEY (`menu_module_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Індекси таблиці `oc_mfilter_settings`
--
ALTER TABLE `oc_mfilter_settings`
  ADD UNIQUE KEY `idx` (`idx`);

--
-- Індекси таблиці `oc_mfilter_url_alias`
--
ALTER TABLE `oc_mfilter_url_alias`
  ADD PRIMARY KEY (`mfilter_url_alias_id`);

--
-- Індекси таблиці `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Індекси таблиці `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Індекси таблиці `oc_ocslideshow`
--
ALTER TABLE `oc_ocslideshow`
  ADD PRIMARY KEY (`ocslideshow_id`);

--
-- Індекси таблиці `oc_ocslideshow_image`
--
ALTER TABLE `oc_ocslideshow_image`
  ADD PRIMARY KEY (`ocslideshow_image_id`);

--
-- Індекси таблиці `oc_ocslideshow_image_description`
--
ALTER TABLE `oc_ocslideshow_image_description`
  ADD PRIMARY KEY (`ocslideshow_image_id`,`language_id`);

--
-- Індекси таблиці `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Індекси таблиці `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Індекси таблиці `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Індекси таблиці `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Індекси таблиці `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Індекси таблиці `oc_order_custom_field`
--
ALTER TABLE `oc_order_custom_field`
  ADD PRIMARY KEY (`order_custom_field_id`);

--
-- Індекси таблиці `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Індекси таблиці `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Індекси таблиці `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Індекси таблиці `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Індекси таблиці `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Індекси таблиці `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Індекси таблиці `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Індекси таблиці `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Індекси таблиці `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Індекси таблиці `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Індекси таблиці `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Індекси таблиці `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Індекси таблиці `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Індекси таблиці `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Індекси таблиці `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Індекси таблиці `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Індекси таблиці `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Індекси таблиці `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Індекси таблиці `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Індекси таблиці `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Індекси таблиці `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Індекси таблиці `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Індекси таблиці `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Індекси таблиці `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Індекси таблиці `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Індекси таблиці `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Індекси таблиці `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Індекси таблиці `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Індекси таблиці `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Індекси таблиці `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Індекси таблиці `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Індекси таблиці `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Індекси таблиці `oc_subscribe`
--
ALTER TABLE `oc_subscribe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Index_2` (`email_id`);

--
-- Індекси таблиці `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Індекси таблиці `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Індекси таблиці `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Індекси таблиці `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Індекси таблиці `oc_testimonial`
--
ALTER TABLE `oc_testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Індекси таблиці `oc_testimonial_description`
--
ALTER TABLE `oc_testimonial_description`
  ADD PRIMARY KEY (`testimonial_id`,`language_id`);

--
-- Індекси таблиці `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Індекси таблиці `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Індекси таблиці `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Індекси таблиці `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  ADD PRIMARY KEY (`url_alias_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Індекси таблиці `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Індекси таблиці `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Індекси таблиці `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Індекси таблиці `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Індекси таблиці `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Індекси таблиці `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Індекси таблиці `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Індекси таблиці `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Індекси таблиці `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Індекси таблиці `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  MODIFY `affiliate_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_affiliate_activity`
--
ALTER TABLE `oc_affiliate_activity`
  MODIFY `affiliate_activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_affiliate_login`
--
ALTER TABLE `oc_affiliate_login`
  MODIFY `affiliate_login_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  MODIFY `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `oc_article`
--
ALTER TABLE `oc_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_article_list`
--
ALTER TABLE `oc_article_list`
  MODIFY `article_list_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблиці `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT для таблиці `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблиці `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT для таблиці `oc_cmsblock`
--
ALTER TABLE `oc_cmsblock`
  MODIFY `cmsblock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблиці `oc_cmsblock_description`
--
ALTER TABLE `oc_cmsblock_description`
  MODIFY `cmsblock_des_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;
--
-- AUTO_INCREMENT для таблиці `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT для таблиці `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_dqc_setting`
--
ALTER TABLE `oc_dqc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_dqc_statistic`
--
ALTER TABLE `oc_dqc_statistic`
  MODIFY `statistic_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT для таблиці `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблиці `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблиці `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1905;
--
-- AUTO_INCREMENT для таблиці `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=539;
--
-- AUTO_INCREMENT для таблиці `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_menu`
--
ALTER TABLE `oc_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_mfilter_url_alias`
--
ALTER TABLE `oc_mfilter_url_alias`
  MODIFY `mfilter_url_alias_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблиці `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT для таблиці `oc_ocslideshow`
--
ALTER TABLE `oc_ocslideshow`
  MODIFY `ocslideshow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблиці `oc_ocslideshow_image`
--
ALTER TABLE `oc_ocslideshow_image`
  MODIFY `ocslideshow_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;
--
-- AUTO_INCREMENT для таблиці `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблиці `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблиці `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_order_custom_field`
--
ALTER TABLE `oc_order_custom_field`
  MODIFY `order_custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблиці `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT для таблиці `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=471;
--
-- AUTO_INCREMENT для таблиці `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2852;
--
-- AUTO_INCREMENT для таблиці `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT для таблиці `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблиці `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=629;
--
-- AUTO_INCREMENT для таблиці `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=477;
--
-- AUTO_INCREMENT для таблиці `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12660;
--
-- AUTO_INCREMENT для таблиці `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `oc_subscribe`
--
ALTER TABLE `oc_subscribe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT для таблиці `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT для таблиці `oc_testimonial`
--
ALTER TABLE `oc_testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1102;
--
-- AUTO_INCREMENT для таблиці `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблиці `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4236;
--
-- AUTO_INCREMENT для таблиці `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
